import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";
import {DashboardRoutingModule} from "./dashboard-routing.module";
import {ManageProductModule} from "../manage-product/manage-product.module";

@NgModule({
    imports: [
        CommonModule,
        CustomPrimeNGModule,
        DashboardRoutingModule,
        ManageProductModule
    ],
    declarations: [DashboardComponent]
})
export class DashboardModule {
}
