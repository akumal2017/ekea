import {Component, OnInit} from '@angular/core';
import {SessionStorageService} from "../../lib/services/session-storage.service";
import {LoginService} from "../../ekea-webstore/services/login.service";

@Component({
    selector: 'app-cms-navbar',
    templateUrl: './cms-navbar.component.html',
    styleUrls: ['./cms-navbar.component.css']
})
export class CmsNavbarComponent implements OnInit {
    isLoggedIn: boolean;

    constructor(
        private _sessionStorageService: SessionStorageService,
        private _loginService: LoginService
    ) {
    }

    ngOnInit() {
        this.isLogIn();
    }

    isLogIn() {
        this.isLoggedIn = false;
        let token = this._sessionStorageService.getIsAdmin();
        if (token) {
            this.isLoggedIn = true;
        }
    }

    logOut(event) {
        event.preventDefault();
        this._sessionStorageService.clearSession();
        let baseHref = window.location.origin;
        window.location.href = baseHref + "/login";

    }

}
