import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {EkeaCmsComponent} from "./ekea-cms.component";

const routes: Routes = [
    {
        path: '',
        component: EkeaCmsComponent,
        children: [
            {path: '', redirectTo: 'dashboard'},
            {path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule'},
            {path: 'manage-product', loadChildren: './manage-product/manage-product.module#ManageProductModule'},
            {path: 'manage-order', loadChildren: './manage-order/manage-order.module#ManageOrderModule'},
            {
                path: 'manage-order-detail/:orderId',
                loadChildren: './manage-order-detail/manage-order-detail.module#ManageOrderDetailModule'
            },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EkeaCmsRoutingModule {
}
