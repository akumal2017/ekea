import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EkeaCmsComponent} from './ekea-cms.component';
import {HomeService} from "../ekea-webstore/services/home.service";
import {ProductService} from "../ekea-webstore/services/product.service";
import {CategoryService} from "../ekea-webstore/services/category.service";
import {EventService} from "../ekea-webstore/services/event.service";
import {CartService} from "../ekea-webstore/services/cart.service";
import {MessageService} from "primeng/api";
import {CheckoutService} from "../ekea-webstore/services/checkout.service";
import {UserService} from "../ekea-webstore/services/user.service";
import {LoginService} from "../ekea-webstore/services/login.service";
import {OrderService} from "../ekea-webstore/services/order.service";
import {OrderDetailService} from "../ekea-webstore/services/order-detail.service";
import {CustomPrimeNGModule} from "../lib/custom-primeng.module";
import {StructureModuleModule} from "../ekea-webstore/structure-module/structure-module.module";
import {EkeaCmsRoutingModule} from "./ekea-cms-routing.module";
import {CmsNavbarComponent} from "./cms-navbar/cms-navbar.component";

@NgModule({
    imports: [
        CommonModule,
        CustomPrimeNGModule,
        StructureModuleModule,
        EkeaCmsRoutingModule
    ],
    declarations: [EkeaCmsComponent, CmsNavbarComponent],
    providers: [
        HomeService,
        ProductService,
        CategoryService,
        EventService,
        CartService,
        MessageService,
        CheckoutService,
        UserService,
        LoginService,
        OrderService,
        OrderDetailService
    ]
})
export class EkeaCmsModule {
}
