import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-manage-order-detail',
    templateUrl: './manage-order-detail.component.html',
    styleUrls: ['./manage-order-detail.component.css']
})
export class ManageOrderDetailComponent implements OnInit {
    orderId: string;

    constructor(private _activatedRoute: ActivatedRoute) {
        this._activatedRoute.params.subscribe(params => {
            //console.log("Params: ", params.catego)
            this.orderId = params.orderId;
        });
    }

    ngOnInit() {

    }

}
