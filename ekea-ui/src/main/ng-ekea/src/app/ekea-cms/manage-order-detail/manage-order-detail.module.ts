import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ManageOrderDetailComponent} from './manage-order-detail.component';
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";
import {ManageOrderDetailRoutingModule} from "./manage-order-detail-routing.module";
import {OrderDetailModule} from "../../ekea-webstore/order-detail/order-detail.module";

@NgModule({
    imports: [
        CommonModule,
        CustomPrimeNGModule,
        ManageOrderDetailRoutingModule,
        OrderDetailModule
    ],
    declarations: [ManageOrderDetailComponent]
})
export class ManageOrderDetailModule {
}
