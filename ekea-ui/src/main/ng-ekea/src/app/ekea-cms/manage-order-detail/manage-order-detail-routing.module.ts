import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {ManageOrderDetailComponent} from "./manage-order-detail.component";

const routes: Routes = [
    {
        path: '', component: ManageOrderDetailComponent

    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ManageOrderDetailRoutingModule {
}
