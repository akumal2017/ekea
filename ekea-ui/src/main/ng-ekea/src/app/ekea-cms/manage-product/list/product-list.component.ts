import {Component, OnInit} from "@angular/core";
import {ConfirmationService} from "primeng/components/common/confirmationservice";
import {OverlayPanel} from "primeng/components/overlaypanel/overlaypanel";
import {PrimeNGListBaseComponent} from "../../../lib/components/base-form/primeng-list-base.component";
import {ProductService} from "../../../ekea-webstore/services/product.service";
import {ProductModel} from "../../../ekea-webstore/model/product.model";
import {ApiConstant} from "../../../lib/constants/api.constant";

@Component({
    selector: 'app-product-list',
    templateUrl: 'product-list.component.html',
    styleUrls: ['product-list.component.css']
})
export class ProductListComponent extends PrimeNGListBaseComponent implements OnInit {
    formHeader: string = "Product Form";
    selectedImageName: string;


    constructor(private productService: ProductService, protected confirmationService: ConfirmationService) {
        super(confirmationService);
        // this.listData=[
        //   {id:1,code:'R001',name:'Chicken Momo',price:'100.00',description:'Chicken Momo with soup',checked:false,version:0},
        //   {id:2,code:'R002',name:'Buff Momo',price:'100.00',description:'Buff Momo with soup',checked:false,version:0}
        // ];

        this.cols = [
            {field: 'id', header: 'Code'},
            {field: 'name', header: 'Name'},
            {field: 'price', header: 'Price'},
            {field: 'categoryEntityType', header: 'Category'}
        ];
    }

    getService() {
        return this.productService;
    }

    getNewDataModel() {
        return new ProductModel();
    }

    setSelectedImage(image) {
        if (image) {
            this.selectedImageName = image;
        }
    }

    viewImage(event, selectedProduct, overlayImage: OverlayPanel) {
        if (selectedProduct.image) {
            this.selectedImageName = selectedProduct.image;
            overlayImage.toggle(event);
        }
    }

    getImage() {
        return ApiConstant.IMAGE_DISPLAY + this.selectedImageName;
    }

}



