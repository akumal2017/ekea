import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ManageProductComponent} from './manage-product.component';
import {ProductListComponent} from "./list/product-list.component";
import {ProductFormComponent} from "./form/product-form.component";
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ManageProductRoutingModule} from "./manage-product-routing.module";

@NgModule({
    imports: [
        CommonModule,
        CustomPrimeNGModule,
        FormsModule,
        ReactiveFormsModule,
        ManageProductRoutingModule
    ],
    declarations: [ManageProductComponent, ProductListComponent, ProductFormComponent],
    exports: [ProductListComponent, ProductFormComponent]
})
export class ManageProductModule {
}
