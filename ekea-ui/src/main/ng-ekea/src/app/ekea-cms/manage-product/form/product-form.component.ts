import {Component, Input} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PrimeNGFormComponentBase} from "../../../lib/components/base-form/primng-form-base.component";
import {ProductModel} from "../../../ekea-webstore/model/product.model";
import {ProductService} from "../../../ekea-webstore/services/product.service";
import {ResponseModel} from "../../../lib/model/response.model";
import {FormAction} from "../../../lib/enums/form-action.enum";
import {ApiConstant} from "../../../lib/constants/api.constant";
import {CategoryService} from "../../../ekea-webstore/services/category.service";
import {MessageService} from "primeng/api";


@Component({
    selector: 'app-product-form',
    templateUrl: 'product-form.component.html',
    styleUrls: ['product-form.component.css']
})
export class ProductFormComponent extends PrimeNGFormComponentBase {
    @Input("productModel")
    productModel: ProductModel;
    @Input("buttonType")
    buttonType: string = this.ADD_BUTTON_ROLE;
    productForm: FormGroup;
    imgUrl: string;
    progress = {value: 0};
    imageFile: File;
    ekeaMsgs = [];
    isVisible: boolean = false;
    fileName: string;
    showFileUploadErrMsg: boolean = false;
    categoryList = [];

    constructor(protected _formBuilder: FormBuilder, private _productService: ProductService, private _categoryService: CategoryService, private _messageService: MessageService) {
        super();
        this.categoryList = [];
        // this.cateogryModel = new CategoryModel();
        this.fetchCategoryList();
    }

    fetchCategoryList() {
        this._categoryService.getList().then((res: ResponseModel) => {
            if (res.responseStatus) {
                this.categoryList = res.result;

            } else {
                this.categoryList = [];
            }
        });
    }

    initForm() {
        // this.dataModel = (this.productModel == null)? new ProductModel(): this.productModel;

        this.productForm = new FormGroup({});
        this.productForm = this._formBuilder.group({
            id: [{value: this.productModel.id, disabled: true}],
            name: [this.productModel.name, [Validators.required]],
            price: [this.productModel.price, [Validators.required]],
            img1: this.productModel.img1,
            description: this.productModel.description,
            material: this.productModel.material,
            categoryEntityId: [{id: this.productModel.categoryEntityId}, [Validators.required]],
            version: this.productModel.version
        });
        this.baseForm = this.productForm;
    }

    getService() {
        return this._productService;
    }

    onSelectFile(event) {
        if (event.target.files && event.target.files[0]) {
            this.isVisible = true;
            var reader = new FileReader();

            const fileReader: FileReader = new FileReader();

            reader.readAsDataURL(event.target.files[0]);
            this.imageFile = event.target.files[0];

            reader.onload = (event: any) => {
                this.imgUrl = event.target.result
            };

        } else {
            this.resetFileUpload();
        }
    }

    uploadImage() {
        this.getService().progress$.subscribe(
            (data: number) => {
                this.progress.value = data;
            });
        this.getService().uploadRequest(this.imageFile).subscribe((res: ResponseModel) => {
            if (res.responseStatus) {
                this.fileName = res.result;
                this.showFileUploadErrMsg = false;
            }
        });
    }

    add(dialogStatus) {
        // this.role = this.baseForm.value;
        // if (this.fileName) {
            this.dataModel = this.baseForm.value;
            this.dataModel['img1'] = this.fileName;
            this.getService().add(this.dataModel).then((data: ResponseModel) => {
                if (data.responseStatus) {
                    let formAction = (dialogStatus === true) ? FormAction.SAVE_AND_NEW : FormAction.SAVE;
                    this.emitDialogStatus(dialogStatus, formAction, data);
                    this.baseForm.reset();
                    this.resetFileUpload();
                }
                else {
                    //this.alertModel.alertType = AlertType.ERROR;
                    this._messageService.add({severity: 'error', summary: 'Message', detail: data.message});
                }
            });
        // }//
        //else {
            this.showFileUploadErrMsg = true;
        // }
    }

    update(dialogStatus) {
        this.dataModel = {};
        this.dataModel = this.baseForm.value;
        this.dataModel['id'] = this.productModel.id;
        if (this.fileName) {
            this.dataModel['img1'] = this.fileName;
        }
        this.getService().update(this.dataModel).then((data: ResponseModel) => {
            if (data.responseStatus) {

                this.emitDialogStatus(dialogStatus, FormAction.UPDATE, data);
                // this.getService().alertModel.alertType = AlertType.SUCCESS;
                this.getService().buttonRole = this.ADD_BUTTON_ROLE;
                this.resetFileUpload();
                //this.viewList();
            }
            else {
                console.error("Form update error");
                this._messageService.add({severity: 'error', summary: 'Message', detail: data.message});
                // this.alertModel.showAlert = true;
                // this.alertModel.message = data.message;
                // this.alertModel.alertType = AlertType.ERROR;
            }
        });
    }

    resetFileUpload() {
        this.isVisible = false;
        this.fileName = "";
        this.imageFile = null;
        this.progress.value = 0;
        this.imgUrl = "";
    }

    getImage() {
        return ApiConstant.IMAGE_DISPLAY + this.productModel.img1;
    }

    submitForm(dialogStatus: boolean) {
        if (this.baseForm.valid) {
            let selectedCategoryType = this.baseForm.controls['categoryEntityId'].value;
            if (selectedCategoryType.name || selectedCategoryType.id) {
                this.baseForm.controls['categoryEntityId'].setValue(selectedCategoryType.id);
            }
            if (this.buttonType == this.UPDATE_BUTTON_ROLE) {
                this.update(dialogStatus);
            }
            else {
                // this.getService().alertModel.showAlert = false;
                this.add(dialogStatus);
            }
        }
    }


}

export interface FileReaderEventTarget extends EventTarget {
    result: string
}

export interface FileReaderEvent extends Event {
    target: FileReaderEventTarget;

    getMessage(): string;
}

