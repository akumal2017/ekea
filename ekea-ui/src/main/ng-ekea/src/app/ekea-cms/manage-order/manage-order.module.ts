import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ManageOrderComponent} from './manage-order.component';
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";
import {ManageOrderRoutingModule} from "./manage-order-routing.module";

@NgModule({
    imports: [
        CommonModule,
        CustomPrimeNGModule,
        ManageOrderRoutingModule
    ],
    declarations: [ManageOrderComponent]
})
export class ManageOrderModule {
}
