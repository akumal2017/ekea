import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {OrderComponent} from "./order.component";
import {ManageOrderComponent} from "./manage-order.component";

const routes: Routes = [
    {
        path: '', component: ManageOrderComponent

    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ManageOrderRoutingModule {
}
