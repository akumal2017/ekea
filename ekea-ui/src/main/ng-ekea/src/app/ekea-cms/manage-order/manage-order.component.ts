import {Component, OnInit} from '@angular/core';
import {OrderModel} from "../../ekea-webstore/model/order.model";
import {OrderStatus} from "../../ekea-webstore/enums/order-status.enum";
import {OrderService} from "../../ekea-webstore/services/order.service";
import {Router} from "@angular/router";
import {ResponseModel} from "../../lib/model/response.model";
import {SessionStorageService} from "../../lib/services/session-storage.service";
import {UserLocationModel} from "../../ekea-webstore/model/user-location.model";
import {MessageService} from "primeng/api";

@Component({
    selector: 'app-manage-order',
    templateUrl: './manage-order.component.html',
    styleUrls: ['./manage-order.component.css']
})
export class ManageOrderComponent implements OnInit {
    orderList: OrderModel[];
    cols: any[];
    BEING_PREPARED: string;

    RECEIVED: string;
    CANCLLED: string;
    ON_HOLD: string;
    countryList = [];
    selectedCountry = {label: '', value: ''};
    loading: boolean = true;

    constructor(
        private _orderService: OrderService,
        private _router: Router,
        private _sessionStorageService: SessionStorageService,
        private _messageService: MessageService,
    ) {
        this.BEING_PREPARED = OrderStatus.BEING_PREPARED;
        this.RECEIVED = OrderStatus.RECEIVED;
        this.CANCLLED = OrderStatus.CANCLLED;
        this.ON_HOLD = OrderStatus.ON_HOLD;
    }

    ngOnInit() {
        this.fetchOrderList();
        this.initCols();
        this.initCountryList();
    }

    fetchOrderList() {
        let userLoctionModel: UserLocationModel = this._sessionStorageService.getClientLocation();
        if (userLoctionModel.country) {
            this.selectedCountry = {label: userLoctionModel.country, value: userLoctionModel.country};
            this._orderService.getListByCountry(userLoctionModel.country).then((res: ResponseModel) => {
                this.loading = false;
                if (res.responseStatus) {
                    this.orderList = res.result;
                } else {
                    this.orderList = [];
                }
            });
        }

    }

    initCols() {
        this.cols = [
            {field: 'id', header: 'Order ID #'},

        ];
    }

    initCountryList() {
        this.countryList = [
            {label: 'Select Site', value: ''},
            {label: 'All Site', value: 'All_Country'},
            {label: 'Finland', value: 'Finland'},
            {label: 'Sweden', value: 'Sweden'},
            {label: 'Russia', value: 'Russia'},
        ];
    }

    updateStatus(order: OrderModel, orderStatus: string) {
        if (order) {
            this._orderService.updateStatus(order.id, orderStatus).then((res: ResponseModel) => {
                if (res.responseStatus) {
                    order.orderStatus = orderStatus;
                    this._messageService.add({
                        severity: 'success',
                        summary: 'Order',
                        detail: 'Status Updated Successfully'
                    });
                } else {
                    this._messageService.add({severity: 'error', summary: 'Order', detail: res.message});
                }
            });

        }


    }

    viewDetail(order: OrderModel) {
        this._router.navigateByUrl(`/cms/manage-order-detail/${order.id}`);
    }

    getOrderList(event) {
        console.log(JSON.stringify(event));
        if (event.value !== "All_Country") {
            this.getOrderByCountry(event.value);
        } else {
            this._orderService.getList().then((res: ResponseModel) => {
                if (res.responseStatus) {
                    this.orderList = res.result;
                } else {
                    this.orderList = [];
                }
            });
        }
    }

    getOrderByCountry(country: string) {

        this._orderService.getListByCountry(country).then((res: ResponseModel) => {
            this.loading = false;
            if (res.responseStatus) {
                this.orderList = res.result;
            } else {
                this.orderList = [];
            }
        });
    }
}