import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
    {path: '', redirectTo: 'webstore', pathMatch: 'full'},
    {path: 'webstore', loadChildren: './ekea-webstore/layout.module#LayoutModule'},
    {path: 'cms', loadChildren: './ekea-cms/ekea-cms.module#EkeaCmsModule'},
    {path: 'login', loadChildren: './cms-login/cms-login.module#CmsLoginModule'}
    //{path: '**', redirectTo: 'login'}


];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
//   providers:[
//     {provide: APP_BASE_HREF, useValue: '.'},
//
//     {provide: LocationStrategy, useClass: SorsHashLocationStrategy}
// ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
