/**
 * Created by Anil Kumal on 2/12/2018.
 */
import {UserLocationModel} from "../../ekea-webstore/model/user-location.model";
import {HttpService} from "./http.service";
import {Injectable} from "@angular/core";

@Injectable()
export class SessionStorageService {
    public static TOKEN_KEY: string = 'appToken';
    public static APP_KEY: string = 'appKey';
    public static CART: string = 'cart';
    public static IP: string = 'ip';
    public static COUNTRY: string = 'country';
    public static LAT: string = 'lat';
    public static LON: string = 'lon';

    constructor(private  _httpService: HttpService) {
    }

    // Save data to sessionStorage
    setToken(token) {
        sessionStorage.setItem(SessionStorageService.TOKEN_KEY, token);
    }

    getToken() {
        return sessionStorage.getItem(SessionStorageService.TOKEN_KEY);
    }

    setIsAdmin(token) {
        sessionStorage.setItem(SessionStorageService.APP_KEY, token);
    }

    getIsAdmin() {
        return sessionStorage.getItem(SessionStorageService.APP_KEY);
    }

    removeToken() {
        sessionStorage.removeItem(SessionStorageService.TOKEN_KEY);
    }

    removeAppToken() {
        sessionStorage.removeItem(SessionStorageService.APP_KEY);
    }

    clearSession() {
        sessionStorage.clear();
    }

    setClientLocation(location: UserLocationModel) {
        sessionStorage.setItem(SessionStorageService.IP, location.ip);
        sessionStorage.setItem(SessionStorageService.COUNTRY, location.country);
        sessionStorage.setItem(SessionStorageService.LAT, location.lat);
        sessionStorage.setItem(SessionStorageService.LON, location.lon);
    }

    getClientLocation(): UserLocationModel {
        //
        let userLocation: UserLocationModel = new UserLocationModel();
        userLocation.ip = sessionStorage.getItem(SessionStorageService.IP);
        userLocation.country = sessionStorage.getItem(SessionStorageService.COUNTRY);
        userLocation.lat = sessionStorage.getItem(SessionStorageService.LAT);
        userLocation.lon = sessionStorage.getItem(SessionStorageService.LON);


        return userLocation;
    }

    setCart(cartData: string) {
        sessionStorage.setItem(SessionStorageService.CART, cartData);
    }

    getCart() {
        return sessionStorage.getItem(SessionStorageService.CART);
    }

    clearCart() {
        sessionStorage.removeItem(SessionStorageService.CART);
    }

}


