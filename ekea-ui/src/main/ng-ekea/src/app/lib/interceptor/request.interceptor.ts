import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {SessionStorageService} from "../services/session-storage.service";
import {UserLocationModel} from "../../ekea-webstore/model/user-location.model";
import {HttpService} from "../services/http.service";

/**
 * Created by Anil Kumal on 2/12/2018.
 */
@Injectable()
export class RequestInterceptor implements HttpInterceptor {
    //locationUrl = "https://ipvigilante.com/json";
    constructor(private _sessionStorageService: SessionStorageService
    ) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let authToken = this._sessionStorageService.getToken();
        console.log("Auth Token: ", authToken);
        let userLocation: UserLocationModel = this._sessionStorageService.getClientLocation();
        if (req.url !== HttpService.GEOAPI_URL) {
            req = req.clone({
                setHeaders: {
                    authorization: authToken !== null ? authToken : "",
                    ip: userLocation.ip !== null ? userLocation.ip : "",
                    country: userLocation.country !== null ? userLocation.country : "",
                    lat: userLocation.lat !== null ? userLocation.lat : "",
                    lon: userLocation.lon !== null ? userLocation.lon : "",
                }
            });
        }


        console.log("Request URL", req.url);

        return next.handle(req);
    }


}
