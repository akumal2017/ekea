import {OnInit} from "@angular/core";
import {Message} from "primeng/components/common/message";
import {Button} from "../../../lib/enums/button.enum";
import {FormsBaseComponent} from "./forms-base.component";
import {ResponseModel} from "../../model/response.model";
import {AlertType} from "../../enums/alert-type.enum";
import {FormAction} from "../../enums/form-action.enum";
import {AlertMessage} from "../../model/alert-msg.model";
import {ConfirmationService} from "primeng/components/common/confirmationservice";

/**
 * Created by Anil Kumal on 2/12/2018.
 */

export class PrimeNGListBaseComponent extends FormsBaseComponent implements OnInit {
    dataModel = {};
    msgs: Message[] = [];
    topPosition = 80;
    formHeader: string = "Give Header Name";
    displayDialog: boolean = false;
    listData: any[] = [];
    buttonType = this.ADD_BUTTON_ROLE;
    loadingStatus: boolean;
    currentPage: number = 0;
    pageSize: number = 10;
    totalRecords: number = 0;
    cols: any[];

    constructor(protected confirmationService: ConfirmationService) {
        super();
    }

    getService(): any {
        console.error("Override this method and return the respective service.");
        return null;
    }

    ngOnInit() {
        this.getAll();

    }

    setDisplayDialog(dialogValue: boolean) {
        this.displayDialog = dialogValue;
    }

    getDisplayDialog() {
        return this.displayDialog;
    }

    openForm() {
        this.dataModel = this.getNewDataModel();
        this.buttonType = this.ADD_BUTTON_ROLE;
        this.setDisplayDialog(true);
    }

    hideDialog(event) {
        this.setDisplayDialog(false);
    }

    getAll() {
        this.loadingStatus = true;
        this.getService().getList().then((res: ResponseModel) => {
                this.loadingStatus = false;
                if (res.responseStatus) {
                    this.listData = res.result;
                } else {
                    this.listData = [];
                }
            }
        );
    }

    onButtonClick(event) {
        let buttonName = event.buttonName;
        switch (buttonName) {
            case Button.REFRESH:
                this.resetPagination();
                this.getAll();
                break;
            case Button.ADD:
            default:
                this.openForm();
                break;
        }
    }

    editRecord(recordToEdit) {
        this.dataModel = recordToEdit;
        this.buttonType = this.UPDATE_BUTTON_ROLE;
        this.setDisplayDialog(true);
    }

    deleteRecord(recordToDelete) {
        this.getService().delete(recordToDelete.id).then((res: ResponseModel) => {
            this.alertModel.detail = res.message;
            if (res.responseStatus) {
                this.alertModel.severity = AlertType.SUCCESS;
                this.alertModel.summary = "SUCCESS!";
                this.getAll();
            } else {
                this.alertModel.severity = AlertType.ERROR;
                this.alertModel.summary = "ERROR!";
            }
            this.showMsg(this.alertModel);
        });
    }

    confirmDelete(recordToDelete) {
        this.confirmationService.confirm({
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'fa fa-trash',
            accept: () => {
                this.deleteRecord(recordToDelete);
            }
            // reject: () => {
            //   this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
            // }
        });
    }

    onFormSubmit(event) {
        console.log("ON Form Submit: ", JSON.stringify(event));
        this.setDisplayDialog(event.dialogStatus);
        if (FormAction.SAVE == event.formAction || FormAction.UPDATE == event.formAction) {
            this.alertModel.detail = event.data.message;
            this.alertModel.severity = AlertType.SUCCESS;
            this.showMsg(this.alertModel);
        }
        this.getService().dataModel = this.getNewDataModel();
        if (FormAction.CANCEL !== event.formAction) {
            this.getAll();
        }
    }

    getNewDataModel(): any {
        console.error("Override this method and return the respective service.");
        return null;
    }

    showMsg(alertModel: AlertMessage) {
        this.msgs = [];
        this.msgs.push(alertModel);
    }

    resetPagination() {
        this.currentPage = 0;
        this.pageSize = 10;
    }

}
