import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";

/**
 * Created by Anil Kumal on 2/12/2018.
 */
@Component({
    selector: 'app-page-header',
    templateUrl: 'page-header.component.html',
    styleUrls: ['page-header.component.css'],
})
export class PageHeaderComponent implements OnInit {
    @Input("pageHeaderName")
    pageHeaderName: string = "Please Override header name";

    @Input("showButton")
    showButton: boolean = true;
    @Output()
    buttonClick: EventEmitter<any> = new EventEmitter();

    ngOnInit(): void {

    }

    onButtonClick(buttonName: string) {
        this.buttonClick.emit({buttonName: buttonName});
    }


}
