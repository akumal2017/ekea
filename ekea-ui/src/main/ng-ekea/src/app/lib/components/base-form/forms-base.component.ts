import {AlertMessage} from "../../model/alert-msg.model";
import {FormGroup} from "@angular/forms";
import {DatePipe} from "@angular/common";

/**
 * Created by Anil Kumal on 2/12/2018.
 */
export class FormsBaseComponent {
    ADD_BUTTON_ROLE = 'addButton';
    UPDATE_BUTTON_ROLE = 'editButton';
    DEFAULT_DATE_FORMAT = 'yyyy-MM-dd';
    alertModel: AlertMessage = new AlertMessage();

    constructor() {
    }

    /**
     * compares if from date is greater than to date
     * @param {string} from
     * @param {string} to
     * @returns {(group: FormGroup) => {[p: string]: any}}
     */
    fromDateLessThanToDateValidator(from: string, to: string) {
        return (group: FormGroup): { [key: string]: any } => {
            let fromDate = group.controls[from];
            let toDate = group.controls[to];
            if (fromDate.value > toDate.value) {
                return {
                    dates: "From Date should be less than To Date"
                };
            }
            return {};
        }
    }

    /**
     * Format date in yyyy-MM-dd
     * @param date
     * @returns {string | null}
     *  - formated date
     */
    getFormatedDate(date) {
        let dateFormat = new DatePipe('en');
        return dateFormat.transform(date, this.DEFAULT_DATE_FORMAT);
    }
}
