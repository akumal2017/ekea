/**
 * Created by Anil Kumal on 2/12/2018.
 */
export class AlertType {
    static SUCCESS: string = "success";
    static WARN: string = "warn";
    static INFO: string = "info";
    static ERROR: string = "error";
}
