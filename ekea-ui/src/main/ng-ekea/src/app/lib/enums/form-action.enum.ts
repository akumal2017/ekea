/**
 * Created by Anil Kumal on 2/12/2018.
 */
export class FormAction {
    static SAVE: string = "save";
    static SAVE_AND_NEW = "saveAndNew";
    static UPDATE: string = "update";
    static DELETE: string = "delete";
    static VIEW: string = "view";
    static CANCEL: string = "cancel";
}
