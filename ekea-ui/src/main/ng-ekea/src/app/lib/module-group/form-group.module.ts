import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgModule} from "@angular/core";
import {FtLibModule} from "../ft-lib.module";

/**
 * Created by Anil Kumal on 2/12/2018.
 */
@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        FtLibModule

    ],

    exports: [
        FormsModule,
        ReactiveFormsModule,
        FtLibModule
    ]

})
export class CommonFormGroupModule {
}
