/**
 * Created by Anil Kumal on 2/12/2018.
 */
export class BaseFtModel {
    id: string;
    name: string;
    version: number;
    checked: boolean = false;

}
