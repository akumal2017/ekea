/**
 * Created by Anil Kumal on 2/12/2018.
 */
export class AlertMessage {
    severity: string;
    //title of message
    summary: string;
    detail: string;
}
