/**
 * Created by Anil Kumal on 2/12/2018.
 */
export class ResponseModel {
    result: any;
    message: string;
    responseStatus: boolean;
    page: any;
}
