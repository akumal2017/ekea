import {HttpClientModule} from "@angular/common/http";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {TooltipModule} from "primeng/components/tooltip/tooltip";
import {PageHeaderComponent} from "./components/page-header.component";
import {ButtonModule} from "primeng/components/button/button";

/**
 * Created by Anil Kumal on 2/12/2018.
 */
@NgModule({
    declarations: [
        PageHeaderComponent
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        TooltipModule,
        ButtonModule

    ],
    exports: [PageHeaderComponent],
    providers: []

})
export class FtLibModule {
}
