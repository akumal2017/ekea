import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CmsLoginComponent} from "./cms-login.component";

const routes: Routes = [
    {
        path: '', component: CmsLoginComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CmsLoginRoutingModule {
}
