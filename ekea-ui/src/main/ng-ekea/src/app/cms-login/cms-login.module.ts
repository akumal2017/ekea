import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CmsLoginComponent} from './cms-login.component';
import {CmsLoginRoutingModule} from "./cms-login-routing.module";
import {LoginModule} from "../ekea-webstore/login/login.module";
import {CustomPrimeNGModule} from "../lib/custom-primeng.module";
import {LoginService} from "../ekea-webstore/services/login.service";
import {SessionStorageService} from "../lib/services/session-storage.service";
import {EventService} from "../ekea-webstore/services/event.service";

@NgModule({
    imports: [
        CommonModule,
        CmsLoginRoutingModule,
        LoginModule,
        CustomPrimeNGModule
    ],
    declarations: [CmsLoginComponent],
    providers: [LoginService, SessionStorageService, EventService]
})
export class CmsLoginModule {
}
