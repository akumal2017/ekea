import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {UserLocationModel} from "../ekea-webstore/model/user-location.model";
import {ExternalApiResponseModel} from "../ekea-webstore/model/external-api-response.model";
import {SessionStorageService} from "../lib/services/session-storage.service";
import {LoginService} from "../ekea-webstore/services/login.service";

@Component({
    selector: 'app-cms-login',
    templateUrl: './cms-login.component.html',
    styleUrls: ['./cms-login.component.css']
})
export class CmsLoginComponent implements OnInit {

    infoMsgs = [];

    constructor(private _router: Router, private _sessionStorage: SessionStorageService, private _loginService: LoginService) {
        let userLocation: UserLocationModel = new UserLocationModel();
        userLocation = this._sessionStorage.getClientLocation();
        if (!userLocation.ip) {
            this._loginService.getUserLocation().then((res: ExternalApiResponseModel) => {
                if (res) {
                    userLocation.ip = res.query;
                    userLocation.country = res.country;
                    userLocation.lat = res.lat;
                    userLocation.lon = res.lon;
                    this._sessionStorage.setClientLocation(userLocation);
                }
            });
        }
    }

    ngOnInit() {
    }

    onLoginSuccess(event) {
        if (event.success && event.isAdmin) {
            this._router.navigateByUrl("/cms/dashboard");
        } else {
            this.infoMsgs.push({severity: 'error', summary: 'Login: ', detail: 'Email or password not match.'})
        }

    }

}
