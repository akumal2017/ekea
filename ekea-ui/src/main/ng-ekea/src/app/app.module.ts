import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {AppComponent} from "./app.component";
import {CommonModule} from "@angular/common";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {RequestInterceptor} from "./lib/interceptor/request.interceptor";
import {HttpService} from "./lib/services/http.service";
import {AppRoutingModule} from "./app-routing.module";
import {SessionStorageService} from "./lib/services/session-storage.service";
import {AuthGuard} from "./lib/services/auth-guard.service";


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        ProgressSpinnerModule
    ],
    providers: [{
        provide: HTTP_INTERCEPTORS,
        useClass: RequestInterceptor,
        multi: true
    },
        HttpService,
        SessionStorageService,
        AuthGuard
    ],
    bootstrap: [AppComponent],

})
export class AppModule {
}
