import {Component, OnInit} from '@angular/core';
import "../../../assets/vendor/owl.carousel/owl.carousel.js";
import "../../../assets/vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js";
import {HomeService} from "../services/home.service";
import {ResponseModel} from "../../lib/model/response.model";
import {SessionStorageService} from "../../lib/services/session-storage.service";
import {UserLocationModel} from "../model/user-location.model";
import {ExternalApiResponseModel} from "../model/external-api-response.model";

declare var $: any;

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

    productList = [];

    constructor(private _homeService: HomeService,
                private _sessionStorage: SessionStorageService
    ) {
        let userLocation: UserLocationModel = new UserLocationModel();
        userLocation = this._sessionStorage.getClientLocation();
        if (userLocation.ip) {
            this.getHotProductList();
        } else {
            this._homeService.getUserLocation().then((res: ExternalApiResponseModel) => {
                if (res) {
                    userLocation.ip = res.query;
                    userLocation.country = res.country;
                    userLocation.lat = res.lat;
                    userLocation.lon = res.lon;
                    this._sessionStorage.setClientLocation(userLocation);
                    this.getHotProductList();
                }
            });
        }


    }

    getHotProductList() {
        this._homeService.getHotProductList().then((res: ResponseModel) => {
            if (res.responseStatus) {
                this.productList = res.result;
                this.renderProducts();
            } else {
                this.productList = [];
            }
        });

    }

    renderProducts() {
        var $product_slider = $('#product-slider');
        for (let i = 0; i < this.productList.length; i++) {
            $product_slider
                .append(' <div class="item"><div class="product"><div class="flip-container"><div class="flipper"><div class="front"><a href="detail.html"><img src="../../../assets/img/' + this.productList[i].img1 + '"' + 'alt="" class="img-fluid"></a></div><div class="back"><a href="detail.html"><img src="../../../assets/img/' + this.productList[i].img1 + '"' + 'alt="" class="img-fluid"></a></div></div></div><a href="detail.html" class="invisible"><img src="../../../assets/img/product1.jpg" alt="" class="img-fluid"></a><div class="text"><h3><a href="/webstore/product-detail/' + this.productList[i].id + '">' + this.productList[i].name + '</a></h3><p class="price">' + this.productList[i].price + '€</p></div><!-- /.text--><div class="ribbon sale"><div class="theribbon">SALE</div><div class="ribbon-background"></div></div><!-- /.ribbon--><div class="ribbon new"><div class="theribbon">NEW</div><div class="ribbon-background"></div></div><!-- /.ribbon--><div class="ribbon gift"><div class="theribbon">GIFT</div><div class="ribbon-background"></div></div><!-- /.ribbon--></div><!-- /.product--></div>');
        }
        $('.product-slider').owlCarousel({
            items: 1,
            dots: true,
            nav: false,
            responsive: {
                480: {
                    items: 1
                },
                765: {
                    items: 2
                },
                991: {
                    items: 3
                },
                1200: {
                    items: 5
                }
            }
        });

    }

    clickEvent() {
        alert("Hello");
    }

    ngOnInit() {
        $('.shop-detail-carousel').owlCarousel({
            items: 1,
            thumbs: true,
            nav: false,
            dots: false,
            loop: true,
            autoplay: true,
            thumbsPrerendered: true
        });


        $('#main-slider').owlCarousel({
            items: 1,
            nav: false,
            dots: true,
            autoplay: true,
            autoplayHoverPause: true,
            dotsSpeed: 400,
            loop: true
        });





    }

}
