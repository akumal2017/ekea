import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";
import {HomeRoutingModule} from "./home-routing.module";
import {HomeComponent} from "./home.component";


@NgModule({
    imports: [
        CommonModule,
        CustomPrimeNGModule,
        HomeRoutingModule,

    ],
    declarations: [
        HomeComponent,
    ],
    providers: []
})
export class HomeModule {
}
