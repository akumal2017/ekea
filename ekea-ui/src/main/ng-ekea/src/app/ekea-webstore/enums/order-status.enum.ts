export class OrderStatus {
    static BEING_PREPARED: string = "Being Prepared";
    static RECEIVED: string = "Received";
    static CANCLLED: string = "Cancelled";
    static ON_HOLD: string = "On Hold";

}
