import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";
import {ProductComponent} from "./product.component";
import {ProductRoutingModule} from "./product-routing.module";

@NgModule({
    imports: [
        CommonModule,
        ProductRoutingModule,
        CustomPrimeNGModule
    ],
    declarations: [ProductComponent]
})
export class ProductModule {
}
