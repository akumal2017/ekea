import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ProductService} from "../services/product.service";
import {ResponseModel} from "../../lib/model/response.model";
import {ProductModel} from "../model/product.model";
import {CategoryService} from "../services/category.service";
import {CategoryModel} from "../model/category.model";
import {CartService} from "../services/cart.service";

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
    productList: ProductModel[];
    category: CategoryModel;
    categoryId: string;
    rows: number = 8;
    totalRecords: number = 100;
    currentPage: number = 0;
    loading: boolean;

    constructor(private _activatedRoute: ActivatedRoute,
                private _productService: ProductService,
                private _categoryService: CategoryService,
                private _cartService: CartService,
                private _router: Router
    ) {
        this.productList = [];
        this.category = new CategoryModel();
        this._activatedRoute.params.subscribe(params => {
            this.categoryId = params.categoryId;
            this.getProducts(this.categoryId);
        });
        this.loading = true;
    }

    ngOnInit() {
    }

    getProducts(categoryId: string) {
        this.productList = [];
        if (categoryId) {
            this.getProductsByCategoryId(categoryId);
            this.getCategoryById(categoryId);
        } else {

            this.getAllProducts()
        }

    }

    getProductsByCategoryId(categoryId: string) {
        this.loading = true;
        this._productService.getProductListByCategoryId(categoryId, this.currentPage, this.rows).then((res: ResponseModel) => {

            if (res.responseStatus) {
                this.productList = res.result;
                this.totalRecords = res.page.totalRecords;
            }
            this.loading = false;
        });
    }

    getAllProducts() {
        this.loading = true;
        this.category = new CategoryModel();
        this.category.type = "All Products";
        this.category.description = "Browse a comprehensive list of E-kea products. we offer wide selection of the best products we have found and carefully selected worldwide ";
        this._productService.getListWithPage(this.currentPage, this.rows).then((res: ResponseModel) => {
            if (res.responseStatus) {
                this.productList = res.result;
                this.totalRecords = res.page.totalRecords;

            }
            this.loading = false;
        });
    }

    getCategoryById(categoryId: string) {
        this._categoryService.getByID(categoryId).then((res: ResponseModel) => {
            if (res.responseStatus) {
                this.category = res.result;
            }
        });
    }


    paginate(event) {
        this.currentPage = event.page;
        this.rows = event.rows;
        this.getProducts(this.categoryId);
    }

    viewProdcutDetail(productId: string) {
        this._router.navigateByUrl(`/webstore/product-detail/${productId}`)
    }

    addToCart(product: ProductModel) {
        this._cartService.addToCart(product);
    }


}
