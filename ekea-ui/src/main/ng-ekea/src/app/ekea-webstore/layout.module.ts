import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {LayoutRoutingModule} from "./layout-routing.module";
import {LayoutComponent} from "./layout.component";
import {CustomPrimeNGModule} from "../lib/custom-primeng.module";
import {StructureModuleModule} from "./structure-module/structure-module.module";
import {HomeService} from "./services/home.service";
import {ProductService} from "./services/product.service";
import {CategoryService} from "./services/category.service";
import {EventService} from "./services/event.service";
import {CartService} from "./services/cart.service";
import {MessageService} from "primeng/api";
import {CheckoutService} from "./services/checkout.service";
import {UserService} from "./services/user.service";
import {LoginService} from "./services/login.service";
import {OrderService} from "./services/order.service";
import {OrderDetailService} from "./services/order-detail.service";

@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        CustomPrimeNGModule,
        StructureModuleModule
    ],
    declarations: [LayoutComponent],
    providers: [
        HomeService,
        ProductService,
        CategoryService,
        EventService,
        CartService,
        MessageService,
        CheckoutService,
        UserService,
        LoginService,
        OrderService,
        OrderDetailService
    ]
})
export class LayoutModule {
}
