import {Component, OnInit} from '@angular/core';
import {ChangePasswordModel} from "../model/change-password.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginService} from "../services/login.service";
import {ResponseModel} from "../../lib/model/response.model";
import {UserOtherDetailModel} from "../model/user-other-detail.model";

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
    changePasswordModel: ChangePasswordModel;
    userDetail: UserOtherDetailModel;
    changePasswordForm: FormGroup;
    userForm: FormGroup;
    pwdInfoMsgs = [];
    userInfoMsgs = [];
    showDummyPwdSaveBtn: boolean;
    disableUserSaveBtn: boolean;

    constructor(private _formBuilder: FormBuilder, private _loginService: LoginService) {
        this.changePasswordModel = new ChangePasswordModel();

        this.pwdInfoMsgs = [];
        this.showDummyPwdSaveBtn = false;
        this.disableUserSaveBtn = false;
    }

    ngOnInit() {
        //this.initUserForm();
        this.getUserProfileDetail();
        this.initChangePasswordForm();


    }

    getUserProfileDetail() {
        this._loginService.getProfile().then((res: ResponseModel) => {
            this.userDetail = new UserOtherDetailModel();
            if (res.responseStatus) {
                this.userDetail = res.result;
                // this.userForm.setValue({
                //     name:    this.userDetail.name,
                //     address: this.userDetail.address,
                //     city: this.userDetail.city,
                //     zipCode: this.userDetail.zipCode,
                //     country: this.userDetail.country,
                //     mobile: this.userDetail.mobile,
                //     phone: this.userDetail.phone
                // });
            }
            this.initUserForm();

        });
    }

    initChangePasswordForm() {
        this.changePasswordForm = new FormGroup({});
        this.changePasswordForm = this._formBuilder.group({
            oldPassword: [this.changePasswordModel.oldPassword, [Validators.required]],
            newPassword: [this.changePasswordModel.newPassword, [Validators.required]],
            confirmPassword: [this.changePasswordModel.confirmPassword, [Validators.required]],
            version: this.changePasswordModel.version
        });
    }

    initUserForm() {
        this.userForm = new FormGroup({});
        this.userForm = this._formBuilder.group({
            name: [this.userDetail.name, [Validators.required]],
            address: [this.userDetail.address, [Validators.required]],
            zipCode: [this.userDetail.zipCode, [Validators.required]],
            city: [this.userDetail.city, [Validators.required]],
            country: [this.userDetail.country, [Validators.required]],
            mobile: this.userDetail.mobile,
            phone: this.userDetail.phone,
            version: this.changePasswordModel.version
        });
    }

    isPasswordMatched() {
        let newPwd = this.changePasswordForm.controls['newPassword'].value;
        let confirmPwd = this.changePasswordForm.controls['confirmPassword'].value;
        return (newPwd === confirmPwd);
    }

    updatePassword() {
        this.hidePwdInfoMsg();
        let pwdInfoMsg = {severity: '', summary: '', detail: ''};
        if (this.changePasswordForm.valid) {
            this.showDummyPwdSaveBtn = true;
            this.changePasswordModel = this.changePasswordForm.value;
            pwdInfoMsg = {
                severity: 'info',
                summary: 'Change Password:',
                detail: 'Please wait, Your password is being updated.'
            };
            this.showPwdInfoMsg(pwdInfoMsg);
            this._loginService.changePassword(this.changePasswordModel).then((res: ResponseModel) => {

                if (res.responseStatus) {
                    pwdInfoMsg = {
                        severity: 'success',
                        summary: 'Success:',
                        detail: 'Password updated successfully.'
                    };
                    this.changePasswordForm.reset();
                    this.hidePwdInfoMsg();

                }
                else {
                    pwdInfoMsg = {severity: 'error', summary: 'Error', detail: res.message};
                    this.hidePwdInfoMsg();
                }
                this.showDummyPwdSaveBtn = false;
                this.showPwdInfoMsg(pwdInfoMsg);
            });
        }
    }

    showPwdInfoMsg(infoMsg) {
        this.pwdInfoMsgs.push(infoMsg);
    }

    hidePwdInfoMsg() {
        this.pwdInfoMsgs = [];
    }


    updateDetails() {
        this.hideUserInfoMsg();
        let userInfoMsg = {severity: '', summary: '', detail: ''};
        if (this.userForm.valid) {
            this.disableUserSaveBtn = true;
            this.userDetail = this.userForm.value;
            userInfoMsg = {
                severity: 'info',
                summary: 'Personal Info:',
                detail: 'Please wait, your address detail is being updated...'
            };
            this.showUserInfoMsg(userInfoMsg);
            this._loginService.update(this.userDetail).then((res: ResponseModel) => {

                if (res.responseStatus) {
                    userInfoMsg = {
                        severity: 'success',
                        summary: 'Success:',
                        detail: 'Personal Info updated successfully.'
                    };
                    this.changePasswordForm.reset();
                    this.hideUserInfoMsg();

                }
                else {
                    userInfoMsg = {severity: 'error', summary: 'Error', detail: res.message};
                    this.hideUserInfoMsg();
                }
                this.disableUserSaveBtn = false;
                this.showUserInfoMsg(userInfoMsg);
            });
        }
    }

    showUserInfoMsg(infoMsg) {
        this.userInfoMsgs.push(infoMsg);
    }

    hideUserInfoMsg() {
        this.userInfoMsgs = [];
    }
}
