import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AccountComponent} from './account.component';
import {AccountRoutingModule} from "./account-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";

@NgModule({
    imports: [
        CommonModule,
        AccountRoutingModule,
        FormsModule,
        CustomPrimeNGModule,
        ReactiveFormsModule
    ],
    declarations: [AccountComponent]
})
export class AccountModule {
}
