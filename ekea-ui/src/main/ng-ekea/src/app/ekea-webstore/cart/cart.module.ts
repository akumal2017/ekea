import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CartComponent} from './cart.component';
import {CartRoutingModule} from "./cart-routing.module";
import {FormsModule} from "@angular/forms";
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";

@NgModule({
    imports: [
        CommonModule,
        CartRoutingModule,
        FormsModule,
        CustomPrimeNGModule
    ],
    declarations: [CartComponent]
})
export class CartModule {
}
