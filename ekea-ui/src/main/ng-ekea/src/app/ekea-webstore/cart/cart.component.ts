import {Component, OnInit} from '@angular/core';
import {CartService} from "../services/cart.service";
import {OrderDetailModel} from "../model/order-detail.model";
import {MessageService} from "primeng/api";
import {Router} from "@angular/router";
import {SessionStorageService} from "../../lib/services/session-storage.service";

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
    totalItems: number;
    localCartArr: OrderDetailModel [];
    totalAmount: number;
    shippingCost: number;
    showInfoMsg: boolean;
    constructor(private _cartService: CartService,
                private _messageService: MessageService,
                private _router: Router,
                private _sessionStorageService: SessionStorageService
    ) {

    }

    ngOnInit() {
        this.totalItems = this._cartService.getCartItems().length;
        this.shippingCost = this._cartService.shippingCost;
        this.initCart();
        this.initShowInfoMsg();
        this.calculateTotal();
    }

    initCart() {
        //debugger;
        this.localCartArr = this._cartService.getCartDataFromSession();

    }

    initShowInfoMsg() {
        let token = this._sessionStorageService.getToken();
        if (token) {
            this.showInfoMsg = false;

        } else {
            this.showInfoMsg = true;
        }
    }

    calculateTotal() {
        this.totalAmount = 0;
        for (let i = 0; i < this.localCartArr.length; i++) {
            let eachCartItem: OrderDetailModel = this.localCartArr[i];
            let eachItemTotal: number = eachCartItem.productEntityPrice * eachCartItem.quantity;
            this.totalAmount += eachItemTotal;
        }
    }

    onActivate(event) {
        console.log(JSON.stringify(event));
        event.target.classList.remove('pi-times');
        event.target.classList.add('pi-check'); // To ADD

    }

    onDelete(cartItem: OrderDetailModel) {
        let index = this.localCartArr.indexOf(cartItem);
        this.localCartArr.splice(index, 1);
        this._cartService.setCartItems(this.localCartArr);
        this.calculateTotal();
        this._cartService.notify();
        this._messageService.add({
            severity: 'success',
            summary: 'Success Message',
            detail: 'Deleted Product, ' + cartItem.productEntityName + 'from the cart.'
        });

    }

    onEdit(cartItem: OrderDetailModel) {
        cartItem.showEditQty = true;
        cartItem.tempQty = cartItem.quantity;
    }

    onSave(cartItem: OrderDetailModel) {
        let index = this.localCartArr.indexOf(cartItem);
        this.localCartArr[index] = cartItem;
        cartItem.showEditQty = false;
        this._cartService.setCartItems(this.localCartArr);
        this.showMessage(cartItem);
        this.calculateTotal();

    }

    onCancel(cartItem: OrderDetailModel) {
        cartItem.quantity = cartItem.tempQty;
        cartItem.showEditQty = false;
    }

    showMessage(cartItem: OrderDetailModel) {
        this._messageService.add({
            severity: 'success',
            summary: 'Success Message',
            detail: 'Quantity updated for Product, ' + cartItem.productEntityName + '.'
        });
    }

    navigateRoute(cartItem: OrderDetailModel) {
        this._router.navigateByUrl(`/webstore/product-detail/${cartItem.productEntityId}`);
    }

    proceedToCheckout() {
        this._router.navigateByUrl("/webstore/checkout");
    }

    backToProduct() {
        this._router.navigateByUrl("/webstore/product");
    }

}
