import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LoginModel} from "../model/login.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginService} from "../services/login.service";
import {ResponseModel} from "../../lib/model/response.model";
import {SessionStorageService} from "../../lib/services/session-storage.service";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    @Output("onLoginSuccess")
    loginSuccess = new EventEmitter();

    @Input("showLogo")
    showLogo: boolean;
    loginModel: LoginModel;
    loginFormGroup: FormGroup;
    disableLoginBtn: boolean;
    infoMsgs = [];

    constructor(
        private _formBuilder: FormBuilder,
        private _loginService: LoginService,
        private _sessionStograge: SessionStorageService
    ) {
        this.loginModel = new LoginModel();
        this.disableLoginBtn = false;
        this.showLogo = false;
    }

    ngOnInit() {
        this.initForm();

    }

    initForm() {
        this.loginFormGroup = new FormGroup({});
        this.loginFormGroup = this._formBuilder.group({
            email: [this.loginModel.email, [Validators.required]],
            password: [this.loginModel.password, [Validators.required]],
        });
    }

    onLogin() {
        this.hide();
        let infoMsg = {severity: '', summary: '', detail: ''};
        if (this.loginFormGroup.valid) {
            this.disableLoginBtn = true;
            //infoMsg = {severity: 'info', summary: 'Login: ', detail: 'Please wait, Login in process...'};
            //this.show(infoMsg);
            this.loginModel = this.loginFormGroup.value;
            this._loginService.authenticate(this.loginModel).then((res: ResponseModel) => {
                if (res.responseStatus) {
                    this.hide();
                    //infoMsg = {severity:'success', summary:'Login Success:', detail:'Login Success'};
                    this.loginFormGroup.reset();
                    this.disableLoginBtn = false;
                    this._sessionStograge.setIsAdmin(res.result.isAdmin);
                    this._sessionStograge.setToken(res.result.token);

                    this._loginService.updateLoginStatus(true);
                    this.loginSuccess.emit({success: true, isAdmin: res.result.isAdmin});
                    this.disableLoginBtn = false;
                } else {
                    this.hide();
                    infoMsg = {severity: 'error', summary: 'Login Error', detail: res.message};
                    this.disableLoginBtn = false;
                    this.show(infoMsg);
                }

            });
        }
        // this.loginSuccess.emit({success:true});
    }

    show(infoMsg) {
        this.infoMsgs.push(infoMsg);
    }

    hide() {
        this.infoMsgs = [];
    }

}
