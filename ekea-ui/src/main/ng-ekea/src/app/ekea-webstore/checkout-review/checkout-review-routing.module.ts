import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CheckoutReviewComponent} from "./checkout-review.component";

const routes: Routes = [
    {
        path: '', component: CheckoutReviewComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CheckoutReviewRoutingModule {
}
