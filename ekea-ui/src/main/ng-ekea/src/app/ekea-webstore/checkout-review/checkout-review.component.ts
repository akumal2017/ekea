import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {CheckoutService} from "../services/checkout.service";
import {CartService} from "../services/cart.service";
import {OrderDetailModel} from "../model/order-detail.model";
import {OrderModel} from "../model/order.model";
import {ResponseModel} from "../../lib/model/response.model";
import {EventService} from "../services/event.service";

@Component({
    selector: 'app-checkout-review',
    templateUrl: './checkout-review.component.html',
    styleUrls: ['./checkout-review.component.css']
})
export class CheckoutReviewComponent implements OnInit {
    localCartArr: OrderDetailModel [];
    totalAmount: number;
    shippingCost: number;
    disableOrderBtn: boolean;
    hideOrderBtn: boolean;
    orderId: string;
    infoMsgs = [];
    constructor(
        private _router: Router,
        private _checkoutService: CheckoutService,
        private _cartService: CartService,
        private _eventService: EventService
    ) {
    }

    ngOnInit() {
        this.shippingCost = this._cartService.shippingCost;
        this.localCartArr = this._cartService.getCartDataFromSession();
        this.totalAmount = this._cartService.getTotalAmount();
        this.disableOrderBtn = false;
        this.hideOrderBtn = false;
    }

    backToDelivery() {
        this._router.navigateByUrl("/webstore/checkout-delivery");
    }

    backToCheckout() {
        this._router.navigateByUrl("/webstore/checkout");
    }

    backToPayment() {
        this._router.navigateByUrl("/webstore/checkout-payment");
    }

    placeOrder() {
        this.disableOrderBtn = false;
        this.hideOrderBtn = false;
        this.hide();
        let finalOrder: OrderModel = new OrderModel();
        finalOrder.shippingCost = this.shippingCost;
        finalOrder.paymentAmount = this.totalAmount;
        finalOrder.orderDetailsList = this.localCartArr;
        let infoMsg = {severity: 'info', summary: 'Order: ', detail: 'Please wait, your order is being placed...'};
        this.show(infoMsg);
        this._checkoutService.makeOrder(finalOrder).then((res: ResponseModel) => {
            this.disableOrderBtn = true;
            this.hide();
            if (res.responseStatus) {
                infoMsg = {severity: 'success', summary: 'Order: ', detail: 'You order was placed successfully.'};
                this.orderId = res.result;
                this.hideOrderBtn = true;
                this._cartService.clearCart();
            } else {
                infoMsg = {severity: 'error', summary: 'Order: ', detail: res.message};
            }
            this.show(infoMsg);
            this.disableOrderBtn = false;
        });

    }

    show(infoMsg) {
        this.infoMsgs.push(infoMsg);
    }

    hide() {
        this.infoMsgs = [];
    }



}
