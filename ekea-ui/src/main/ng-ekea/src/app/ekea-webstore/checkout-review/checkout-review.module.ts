import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CheckoutReviewComponent} from './checkout-review.component';
import {CheckoutReviewRoutingModule} from "./checkout-review-routing.module";
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";

@NgModule({
    imports: [
        CommonModule,
        CheckoutReviewRoutingModule,
        CustomPrimeNGModule
    ],
    declarations: [CheckoutReviewComponent]
})
export class CheckoutReviewModule {
}
