import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {CheckoutService} from "../services/checkout.service";
import {OrderDetailModel} from "../model/order-detail.model";
import {CartService} from "../services/cart.service";

@Component({
    selector: 'app-checkout-payment',
    templateUrl: './checkout-payment.component.html',
    styleUrls: ['./checkout-payment.component.css']
})
export class CheckoutPaymentComponent implements OnInit {
    paymentMethod: string;
    localCartArr: OrderDetailModel [];
    totalAmount: number;
    shippingCost: number;

    constructor(
        private _router: Router,
        private _checkoutService: CheckoutService,
        private _cartService: CartService
    ) {
    }

    ngOnInit() {
        this.paymentMethod = this._checkoutService.paymentMethod;
        this.shippingCost = this._cartService.shippingCost;
        this.localCartArr = this._cartService.getCartDataFromSession();
        this.totalAmount = this._cartService.getTotalAmount();
    }

    gotoReview() {
        this._router.navigateByUrl("/webstore/checkout-review");
    }

    backToDelivery() {
        this._router.navigateByUrl("/webstore/checkout-delivery");
    }

    backToCheckout() {
        this._router.navigateByUrl("/webstore/checkout");
    }

    changePaymentMode(event) {
        this._checkoutService.setPaymentMethod(this.paymentMethod);
    }

}
