import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CheckoutPaymentComponent} from './checkout-payment.component';
import {CheckoutPaymentRoutingModule} from "./checkout-payment-routing.module";
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";
import {FormsModule} from "@angular/forms";

@NgModule({
    imports: [
        CommonModule,
        CheckoutPaymentRoutingModule,
        CustomPrimeNGModule,
        FormsModule
    ],
    declarations: [CheckoutPaymentComponent]
})
export class CheckoutPaymentModule {
}
