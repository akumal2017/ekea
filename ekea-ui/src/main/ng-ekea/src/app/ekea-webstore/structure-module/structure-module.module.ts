import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavBarComponent} from "./nav-bar/nav-bar.component";
import {FooterComponent} from "./footer/footer.component";
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";

@NgModule({
    imports: [
        CommonModule, CustomPrimeNGModule
    ],
    declarations: [NavBarComponent, FooterComponent],
    exports: [
        NavBarComponent, FooterComponent
    ]
})
export class StructureModuleModule {
}
