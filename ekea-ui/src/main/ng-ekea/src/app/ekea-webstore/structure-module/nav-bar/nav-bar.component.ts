import {Component, OnDestroy, OnInit} from '@angular/core';
import {EventService} from "../../services/event.service";
import {CategoryService} from "../../services/category.service";
import {CategoryModel} from "../../model/category.model";
import {ResponseModel} from "../../../lib/model/response.model";
import {Router} from "@angular/router";
import {SessionStorageService} from "../../../lib/services/session-storage.service";
import {LoginService} from "../../services/login.service";

@Component({
    selector: 'app-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit, OnDestroy {
    cartCounter: number = 0;
    subscribeEvent: any;
    menuBarEvent: any;
    categoryArr: CategoryModel[];
    isLoggedIn: boolean;
    constructor(
        private _eventService: EventService,
        private _categoryService: CategoryService,
        private _router: Router,
        private _sessionStorageService: SessionStorageService,
        private _loginService: LoginService
    ) {
        this.categoryArr = [];
        this.isLogIn();
    }

    ngOnInit() {
        this.subscribeEvent = this._eventService.getCartNotification().subscribe((totalOrder: number) => {
            this.cartCounter = totalOrder;
        });

        this.menuBarEvent = this._eventService.getLogInNotification().subscribe((isLoggedIn: boolean) => {
            this.isLoggedIn = isLoggedIn;
        });
        this.getCategories();
        this.initCartCount();
    }

    initCartCount() {
        let cartItems = this._sessionStorageService.getCart();
        if (cartItems) {
            let cartArr = JSON.parse(cartItems);
            this.cartCounter = cartArr.length;
        } else {
            this.cartCounter = 0;
        }
    }

    ngOnDestroy(): void {
        this.subscribeEvent.unsubscribe();
        this.menuBarEvent.unsubscribe();
    }

    getCategories() {
        this._categoryService.getList().then((res: ResponseModel) => {
            if (res.responseStatus) {
                this.categoryArr = res.result;
            }
        });
    }

    isLogIn() {
        this.isLoggedIn = false;
        let token = this._sessionStorageService.getToken();
        if (token) {
            this.isLoggedIn = true;
        }
    }

    logOut(event) {
        event.preventDefault();
        this._loginService.logout();
    }



}
