import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {CheckoutService} from "../services/checkout.service";
import {CartService} from "../services/cart.service";
import {OrderDetailModel} from "../model/order-detail.model";

@Component({
    selector: 'app-checkout-delivery',
    templateUrl: './checkout-delivery.component.html',
    styleUrls: ['./checkout-delivery.component.css']
})
export class CheckoutDeliveryComponent implements OnInit {
    deliveryType: string;
    localCartArr: OrderDetailModel [];
    totalAmount: number;
    shippingCost: number;

    constructor(
        private _router: Router,
        private _checkoutService: CheckoutService,
        private _cartService: CartService
    ) {
    }

    ngOnInit() {
        this.deliveryType = this._checkoutService.deliveryType;
        this.shippingCost = this._cartService.shippingCost;
        this.localCartArr = this._cartService.getCartDataFromSession();
        this.totalAmount = this._cartService.getTotalAmount();
    }

    gotoPayment() {
        this._cartService.setShippingCost(this.shippingCost);
        this._router.navigateByUrl("/webstore/checkout-payment");
    }

    backToCheckout() {
        this._router.navigateByUrl("/webstore/checkout");
    }

    changeDeliveryType(event) {
        if (this.deliveryType === "HOME_DELIVERY") {
            this.shippingCost = 10;
        } else {
            this.shippingCost = 0;
        }
        this._checkoutService.setDeliveryType(this.deliveryType);
    }
}
