import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CheckoutDeliveryComponent} from './checkout-delivery.component';
import {CheckoutDeliveryRoutingModule} from "./checkout-delivery-routing.module";
import {FormsModule} from "@angular/forms";
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";

@NgModule({
    imports: [
        CommonModule,
        CheckoutDeliveryRoutingModule,
        FormsModule,
        CustomPrimeNGModule
    ],
    declarations: [CheckoutDeliveryComponent]
})
export class CheckoutDeliveryModule {
}
