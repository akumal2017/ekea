import {Component, OnInit} from '@angular/core';
import {OrderService} from "../services/order.service";
import {Router} from "@angular/router";
import {ResponseModel} from "../../lib/model/response.model";
import {OrderModel} from "../model/order.model";
import {OrderStatus} from "../enums/order-status.enum";

@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
    orderList: OrderModel[];
    BEING_PREPARED: string;

    RECEIVED: string;
    CANCLLED: string;
    ON_HOLD: string;

    constructor(private _orderService: OrderService,
                private _router: Router
    ) {
        this.BEING_PREPARED = OrderStatus.BEING_PREPARED;
        this.RECEIVED = OrderStatus.RECEIVED;
        this.CANCLLED = OrderStatus.CANCLLED;
        this.ON_HOLD = OrderStatus.ON_HOLD;
    }

    ngOnInit() {
        this.fetchOrderList();
    }

    fetchOrderList() {
        this._orderService.getMyOrders().then((res: ResponseModel) => {
            if (res.responseStatus) {
                this.orderList = res.result;
            } else {
                this.orderList = [];
            }
        });
    }

    onDetailView(order: OrderModel) {
        this._router.navigateByUrl("/webstore/order-detail/" + order.id);
    }

}
