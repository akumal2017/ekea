import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LayoutComponent} from "./layout.component";

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {path: '', redirectTo: 'home'},
            {path: 'home', loadChildren: './home/home.module#HomeModule'},
            {path: 'product/:categoryId', loadChildren: './product/product.module#ProductModule'},
            {path: 'product', loadChildren: './product/product.module#ProductModule'},
            {
                path: 'product-detail/:productId',
                loadChildren: './product-detail/product-detail.module#ProductDetailModule'
            },
            {path: 'cart', loadChildren: './cart/cart.module#CartModule'},
            {path: 'order', loadChildren: './order/order.module#OrderModule'},
            {path: 'order-detail/:orderId', loadChildren: './order-detail/order-detail.module#OrderDetailModule'},
            {path: 'account', loadChildren: './account/account.module#AccountModule'},
            {path: 'checkout', loadChildren: './checkout/checkout.module#CheckoutModule'},
            {
                path: 'checkout-delivery',
                loadChildren: './checkout-delivery/checkout-delivery.module#CheckoutDeliveryModule'
            },
            {
                path: 'checkout-payment',
                loadChildren: './checkout-payment/checkout-payment.module#CheckoutPaymentModule'
            },
            {path: 'checkout-review', loadChildren: './checkout-review/checkout-review.module#CheckoutReviewModule'},
            {path: 'register', loadChildren: './register/register.module#RegisterModule'},
            {path: 'sign-in', loadChildren: './register/register.module#RegisterModule'},

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {
}
