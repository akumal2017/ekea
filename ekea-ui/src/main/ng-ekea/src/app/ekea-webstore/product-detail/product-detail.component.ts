import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ProductService} from "../services/product.service";
import {ProductModel} from "../model/product.model";
import {ResponseModel} from "../../lib/model/response.model";
import {CartService} from "../services/cart.service";

@Component({
    selector: 'app-product-detail',
    templateUrl: './product-detail.component.html',
    styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
    images: any[];
    product: ProductModel;
    categoryId: string;
    categoryName: string;

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _productService: ProductService,
        private _cartService: CartService,
    ) {
        this.product = new ProductModel();
        this._activatedRoute.params.subscribe(params => {
            //console.log("Params: ", params.catego)
            this.getProduct(params.productId);
        });
    }

    getProduct(productId: string) {
        if (productId) {
            this._productService.getByID(productId).then((res: ResponseModel) => {
                if (res.responseStatus) {
                    this.product = res.result;
                    this.categoryId = this.product.categoryEntityId;
                    this.categoryName = this.product.categoryEntityType;
                    this.images = [];
                    this.images.push({
                        source: '/assets/img/' + this.product.img1,
                        alt: 'Description for Image 1',
                        title: 'Title 1'
                    });
                }
            });
        }
    }

    ngOnInit() {


    }

    addToCart(product: ProductModel) {
        this._cartService.addToCart(product);
    }

}
