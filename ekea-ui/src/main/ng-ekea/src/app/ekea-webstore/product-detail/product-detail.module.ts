import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductDetailComponent} from './product-detail.component';
import {ProductDetailRoutingModule} from "./product-detail-routing.module";
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";

@NgModule({
    imports: [
        CommonModule,
        ProductDetailRoutingModule,
        CustomPrimeNGModule
    ],
    declarations: [ProductDetailComponent]
})
export class ProductDetailModule {
}
