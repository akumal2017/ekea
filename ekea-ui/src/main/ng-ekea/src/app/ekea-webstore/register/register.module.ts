import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegisterComponent} from './register.component';
import {CustomPrimeNGModule} from "../../lib/custom-primeng.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RegisterRoutingModule} from "./register-routing.module";
import {LoginModule} from "../login/login.module";

@NgModule({
    imports: [
        CommonModule,
        CustomPrimeNGModule,
        FormsModule,
        RegisterRoutingModule,
        ReactiveFormsModule,
        LoginModule
    ],
    declarations: [RegisterComponent]
})
export class RegisterModule {
}
