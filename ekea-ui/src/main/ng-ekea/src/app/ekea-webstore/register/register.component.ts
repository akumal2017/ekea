import {Component, OnInit} from '@angular/core';
import {UserModel} from "../model/user.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../services/user.service";
import {ResponseModel} from "../../lib/model/response.model";
import {Router} from "@angular/router";
import {SessionStorageService} from "../../lib/services/session-storage.service";

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    userModel: UserModel;
    registerFormGroup: FormGroup;
    disableRegisterBtn: boolean;
    infoMsgs = [];

    constructor(
        private _formBuilder: FormBuilder,
        private _userService: UserService,
        private _router: Router,
        private _sessionStorage: SessionStorageService
    ) {
        this.userModel = new UserModel();
        this.disableRegisterBtn = false;
    }

    ngOnInit() {
        this.initForm();
    }

    initForm() {
        this.registerFormGroup = new FormGroup({});
        this.registerFormGroup = this._formBuilder.group({
            id: this.userModel.id,
            name: [this.userModel.name, [Validators.required]],
            password: [this.userModel.password, [Validators.required]],
            email: [this.userModel.email, [Validators.required]],
            address: [this.userModel.address, [Validators.required]],
            city: [this.userModel.city, [Validators.required]],
            zipCode: [this.userModel.zipCode, [Validators.required]],
            country: [this.userModel.country, [Validators.required]],
            version: this.userModel.version
        });
    }

    onSubmit() {
        this.hide();
        if (this.registerFormGroup.valid) {
            this.disableRegisterBtn = true;
            this.userModel = this.registerFormGroup.value;
            this._userService.add(this.userModel).then((res: ResponseModel) => {
                let infoMsg = {severity: '', summary: '', detail: ''};
                if (res.responseStatus) {
                    infoMsg = {
                        severity: 'success',
                        summary: 'Registration Success:',
                        detail: 'Now, you can log in with your login credentials'
                    };
                    this.registerFormGroup.reset();
                } else {
                    infoMsg = {severity: 'error', summary: 'Registration Error', detail: res.message}
                }
                this.show(infoMsg);
            });
            this.disableRegisterBtn = false;
        }
    }

    show(infoMsg) {
        this.infoMsgs.push(infoMsg);
    }

    hide() {
        this.infoMsgs = [];
    }

    onLoginSuccess(event) {
        if (event.success) {
            this._router.navigateByUrl("/webstore/home");
        }

    }

}
