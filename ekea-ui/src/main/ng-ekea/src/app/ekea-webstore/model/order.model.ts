/**
 * Created by Anil Kumal on 12/7/2018.
 */
import {BaseFtModel} from "../../lib/model/base-ft.model";


export class OrderModel extends BaseFtModel {
    country: string;
    shippingCost: number;
    paymentAmount: number;
    orderDate: Date;
    orderStatus: string;
    orderDetailsList = [];

}
