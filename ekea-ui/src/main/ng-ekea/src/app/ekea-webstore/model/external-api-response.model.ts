/**
 * Created by Anil Kumal on 12/7/2018.
 */


export class ExternalApiResponseModel {
    query: string;
    status: string;
    country: string;
    countryCode: string;
    region: string;
    regionName: string;
    city: string;
    zip: string;
    lat: string;
    lon: string;
    timezone: string;
    isp: string;
    org: string;
    as: string;
    mobile: boolean;
    proxy: boolean;
}
