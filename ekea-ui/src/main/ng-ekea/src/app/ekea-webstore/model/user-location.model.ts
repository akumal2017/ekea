/**
 * Created by Anil Kumal on 12/7/2018.
 */


export class UserLocationModel {
    ip: string;
    country: string;
    lat: string;
    lon: string;
}
