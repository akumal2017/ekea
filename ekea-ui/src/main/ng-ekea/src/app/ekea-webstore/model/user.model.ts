/**
 * Created by Anil Kumal on 12/7/2018.
 */
import {BaseFtModel} from "../../lib/model/base-ft.model";


export class UserModel extends BaseFtModel {
    password: string;
    address: string;
    country: string;
    zipCode: string;
    city: string;
    email: string;
    phone: string;
    mobile: string;
    isAdmin: boolean;
}
