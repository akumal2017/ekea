/**
 * Created by Anil Kumal on 12/7/2018.
 */
import {BaseFtModel} from "../../lib/model/base-ft.model";


export class UserOtherDetailModel extends BaseFtModel {
    address: string;
    country: string;
    zipCode: string;
    city: string;
    phone: string;
    mobile: boolean;
}
