/**
 * Created by Anil Kumal on 12/7/2018.
 */
import {BaseFtModel} from "../../lib/model/base-ft.model";


export class CategoryModel extends BaseFtModel {
    type: string;
    description: string;

}
