/**
 * Created by Anil Kumal on 12/7/2018.
 */
import {BaseFtModel} from "../../lib/model/base-ft.model";


export class OrderDetailModel extends BaseFtModel {

    productEntityId: string;
    productEntityName: string;
    orderEntityId: string;
    productEntityImg1: string;
    productEntityPrice: number;
    quantity: number;
    unitPrice: number;
    showEditQty: boolean = false;
    tempQty: number;

}
