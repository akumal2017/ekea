/**
 * Created by Anil Kumal on 12/7/2018.
 */
import {BaseFtModel} from "../../lib/model/base-ft.model";


export class ProductModel extends BaseFtModel {
    material: string;
    categoryEntityId: string;
    categoryEntityType: string;
    img1: string;
    img2: string;
    img3: string;
    img4: string;
    price: number;
    description: string;
    quantity: number;
}
