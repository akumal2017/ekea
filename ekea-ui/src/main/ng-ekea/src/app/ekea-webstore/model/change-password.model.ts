/**
 * Created by Anil Kumal on 10/12/2018.
 */
import {BaseFtModel} from "../../lib/model/base-ft.model";

export class ChangePasswordModel extends BaseFtModel {
    oldPassword: string;
    newPassword: string;
    confirmPassword: string;
}
