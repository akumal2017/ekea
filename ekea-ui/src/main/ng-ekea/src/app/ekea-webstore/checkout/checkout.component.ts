import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {OrderDetailModel} from "../model/order-detail.model";
import {CartService} from "../services/cart.service";
import {LoginService} from "../services/login.service";
import {UserModel} from "../model/user.model";
import {ResponseModel} from "../../lib/model/response.model";

@Component({
    selector: 'app-checkout',
    templateUrl: './checkout.component.html',
    styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
    localCartArr: OrderDetailModel [];
    totalAmount: number;
    shippingCost: number;
    userModel: UserModel;
    constructor(
        private _router: Router,
        private _cartService: CartService,
        private _loginService: LoginService
    ) {
    }

    ngOnInit() {
        this.fetchUser();
        this.shippingCost = this._cartService.shippingCost;
        this.localCartArr = this._cartService.getCartDataFromSession();
        this.totalAmount = this._cartService.getTotalAmount();
    }

    goToDelivery() {
        this._router.navigateByUrl("/webstore/checkout-delivery");
    }

    backToCart() {
        this._router.navigateByUrl("/webstore/cart");
    }

    fetchUser() {
        this._loginService.getProfile().then((res: ResponseModel) => {
            if (res.responseStatus) {
                this.userModel = res.result;
            } else {
                this.userModel = new UserModel();
            }
        });
    }

}
