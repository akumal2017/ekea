import {Injectable} from "@angular/core";
import {Observable, Subject} from "rxjs";

/**
 * Created by Anil KUmal on 8/12/2018.
 */

@Injectable()
export class EventService {
    private cartNotification: Subject<any> = new Subject<any>();
    private logInNotification: Subject<any> = new Subject<any>();

    setCartNotification(totalOrders: number): void {
        this.cartNotification.next(totalOrders);
    }

    getCartNotification(): Observable<any> {
        return this.cartNotification.asObservable();
    }

    setLogInNotification(isLogIn: boolean): void {
        this.logInNotification.next(isLogIn);
    }

    getLogInNotification(): Observable<any> {
        return this.logInNotification.asObservable();
    }


}
