import {Injectable} from "@angular/core";
import {FTBaseService} from "../../lib/services/ft-base.service";
import {HttpService} from "../../lib/services/http.service";
import {CategoryModel} from "../model/category.model";


@Injectable()
export class CategoryService extends FTBaseService {
    dataModel: CategoryModel = new CategoryModel();

    serviceApi: string = '/ekea/category';


    constructor(httpService: HttpService) {
        super(httpService);

    }


}
