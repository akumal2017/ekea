import {Injectable} from "@angular/core";
import {FTBaseService} from "../../lib/services/ft-base.service";
import {ProductModel} from "../model/product.model";
import {HttpService} from "../../lib/services/http.service";
import {OrderDetailModel} from "../model/order-detail.model";
import {EventService} from "./event.service";
import {MessageService} from "primeng/api";
import {SessionStorageService} from "../../lib/services/session-storage.service";


@Injectable()
export class CartService extends FTBaseService {
    dataModel: ProductModel = new ProductModel();

    serviceApi: string = '/ekea/product';
    cartArr: OrderDetailModel[];
    shippingCost: number;
    totalAmount: number;

    constructor(httpService: HttpService,
                private _eventService: EventService,
                private _messageService: MessageService,
                private _sessionStorage: SessionStorageService,
    ) {
        super(httpService);
        this.cartArr = [];
        this.shippingCost = 10;

    }

    saveCartDataInSessionStorage() {
        this._sessionStorage.setCart(JSON.stringify(this.cartArr));
    }

    getCartDataFromSession(): any[] {
        let cartArr = this._sessionStorage.getCart();
        let cartItems = [];
        if (cartArr) {
            cartItems = JSON.parse(cartArr);
        }
        return cartItems;
    }

    addToCart(product: ProductModel) {
        let orderDetail: OrderDetailModel = this.mapProductToOrder(product);
        if (!this.isRedundantOrder(orderDetail)) {
            this.cartArr.push(orderDetail);
            this.saveCartDataInSessionStorage();
            this.showMessage(orderDetail);
            this._eventService.setCartNotification(this.cartArr.length);
        }
    }

    mapProductToOrder(product: ProductModel) {
        let orderDetail = new OrderDetailModel();
        orderDetail.productEntityId = product.id;
        orderDetail.productEntityName = product.name;
        orderDetail.productEntityImg1 = product.img1;
        orderDetail.productEntityPrice = product.price;
        orderDetail.quantity = 1;
        return orderDetail;
    }

    isRedundantOrder(orderDetail: OrderDetailModel) {
        let isDuplicate = false;
        for (let i = 0; i < this.cartArr.length; i++) {
            let eachOrderDetail: OrderDetailModel = this.cartArr[i];
            if (eachOrderDetail.productEntityId === orderDetail.productEntityId) {
                isDuplicate = true;
                eachOrderDetail.quantity++;
                this.showMessage(orderDetail);
                break;
            }
        }
        return isDuplicate;
    }

    removeOrderFromList(orderDetail: OrderDetailModel) {
        let index = this.cartArr.indexOf(orderDetail);
        this.cartArr.splice(index, 1);
        this.notify();
    }

    clearOrderList() {
        this.cartArr = [];
        this.notify()
    }

    notify() {
        this._eventService.setCartNotification(this.cartArr.length);
    }

    showMessage(orderDetail: OrderDetailModel) {
        this._messageService.add({
            severity: 'success',
            summary: 'Success Message',
            detail: orderDetail.productEntityName + ' added to cart.'
        });
    }

    getCartItems() {
        return this.cartArr;
    }

    setCartItems(orderDetails: OrderDetailModel[]) {
        this.cartArr = orderDetails;
        this.saveCartDataInSessionStorage();
    }

    setShippingCost(shippingCost: number) {
        this.shippingCost = shippingCost;
    }

    getTotalAmount() {
        this.totalAmount = 0;
        let cartArr = this.getCartDataFromSession();
        for (let i = 0; i < cartArr.length; i++) {
            let eachCartItem: OrderDetailModel = cartArr[i];
            let eachItemTotal: number = eachCartItem.productEntityPrice * eachCartItem.quantity;
            this.totalAmount += eachItemTotal;
        }
        return this.totalAmount;
    }

    clearCart() {
        this.cartArr = [];
        this._sessionStorage.clearCart();
        this.notify();
    }


}
