import {Injectable} from "@angular/core";
import {FTBaseService} from "../../lib/services/ft-base.service";
import {ProductModel} from "../model/product.model";
import {HttpService} from "../../lib/services/http.service";


@Injectable()
export class HomeService extends FTBaseService {
    dataModel: ProductModel = new ProductModel();

    serviceApi: string = '/ekea/product';


    constructor(httpService: HttpService) {
        super(httpService);

    }

    getHotProductList() {
        return this.httpService.getRequest(this.serviceApi + "/hot-product-list");
    }

}
