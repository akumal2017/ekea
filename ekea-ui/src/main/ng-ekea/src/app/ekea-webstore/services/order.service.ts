import {Injectable} from "@angular/core";
import {FTBaseService} from "../../lib/services/ft-base.service";
import {HttpService} from "../../lib/services/http.service";
import {OrderModel} from "../model/order.model";


@Injectable()
export class OrderService extends FTBaseService {
    dataModel: OrderModel = new OrderModel();

    serviceApi: string = '/ekea/order';
    myOrders: string = '/my-orders';

    constructor(httpService: HttpService) {
        super(httpService);

    }

    getMyOrders() {
        return this.httpService.getRequest(this.serviceApi + this.myOrders);
    }

    updateStatus(orderId: string, orderStatus: string) {
        return this.httpService.getRequest(this.serviceApi + `/update-status/${orderId}/${orderStatus}`)
    }


}
