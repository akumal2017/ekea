import {Injectable} from "@angular/core";
import {FTBaseService} from "../../lib/services/ft-base.service";
import {ProductModel} from "../model/product.model";
import {HttpService} from "../../lib/services/http.service";
import {ApiConstant} from "../../lib/constants/api.constant";

import {share} from "rxjs/operators"; // lettable operator, tree-shakeable
import {Observable, Subject} from "rxjs";


@Injectable()
export class ProductService extends FTBaseService {
    dataModel: ProductModel = new ProductModel();

    serviceApi: string = '/ekea/product';

    url: string = '/ekea/upload';
    getImageUrl: string = '/ekea/display/';


    progress: number;
    progressObserver = new Subject();
    progress$ = new Subject();

    constructor(httpService: HttpService) {
        super(httpService);
        this.progress$ = Observable.create(observer => {
            this.progressObserver = observer
        }).pipe(share());

    }

    getProductListByCategoryId(categoryId: string, currentPage: number, pageSize: number) {
        return this.httpService.getRequest(this.serviceApi + `/${categoryId}/${currentPage}/${pageSize}`);
    }

    uploadRequest(uploadFile: File): Observable<any> {
        return Observable.create(observer => {
            let formData: FormData = new FormData(),
                xhr: XMLHttpRequest = new XMLHttpRequest();

            formData.append("uploadFile", uploadFile);
            // formData.append("names", names[i]);
            // formData.append("fileTypeName", fileTypeName[i]);


            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        // console.log("[Upload Service] Upload Response: " + xhr.response);
                        observer.next(JSON.parse(xhr.response));
                        observer.complete();

                    } else {
                        observer.error(xhr.response);
                    }
                }
            };

            xhr.upload.onprogress = (event) => {
                this.progress = Math.round(event.loaded / event.total * 100);

                this.progressObserver.next(this.progress);
                // this.getCancelBtnSubject().subscribe((status: boolean) => {
                //   if (status) {
                //     xhr.abort();
                //   }
                // });
            };
            // console.log("[Upload Service] Form Data: " + JSON.stringify(formData));
            xhr.open('POST', ApiConstant.BASE_API + this.url, true);
            // xhr.setRequestHeader("authorization", this.localStorageService.getToken());
            xhr.send(formData);
        });
    }

}
