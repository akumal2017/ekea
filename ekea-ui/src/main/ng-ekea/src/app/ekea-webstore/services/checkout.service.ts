import {Injectable} from "@angular/core";
import {FTBaseService} from "../../lib/services/ft-base.service";
import {HttpService} from "../../lib/services/http.service";
import {CartService} from "./cart.service";
import {OrderModel} from "../model/order.model";


@Injectable()
export class CheckoutService extends FTBaseService {
    dataModel: {};

    serviceApi: string = '/ekea/order';
    deliveryType: string;
    paymentMethod: string;
    orderUrl: string = "/make-order"

    constructor(httpService: HttpService,
                private _cartService: CartService
    ) {
        super(httpService);
        this.deliveryType = "HOME_DELIVERY";
        this.paymentMethod = "CASH_ON_DELIVERY";

    }

    setDeliveryType(deliveryType: string) {
        this.deliveryType = deliveryType;
    }

    setPaymentMethod(paymentMethod: string) {
        this.paymentMethod = paymentMethod;
    }

    makeOrder(finalOrder: OrderModel) {
        return this.httpService.postRequest(this.serviceApi + this.orderUrl, finalOrder);
    }


}
