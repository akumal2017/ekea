import {Injectable} from "@angular/core";
import {FTBaseService} from "../../lib/services/ft-base.service";
import {HttpService} from "../../lib/services/http.service";
import {OrderModel} from "../model/order.model";


@Injectable()
export class OrderDetailService extends FTBaseService {
    dataModel: OrderModel = new OrderModel();

    serviceApi: string = '/ekea/order';


    constructor(httpService: HttpService) {
        super(httpService);

    }

    getMyDetailedOrders(orderId: String) {
        return this.httpService.getRequest(this.serviceApi + `/my-orders/${orderId}`);
    }


}
