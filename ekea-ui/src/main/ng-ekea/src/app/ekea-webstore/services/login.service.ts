import {Injectable} from "@angular/core";
import {FTBaseService} from "../../lib/services/ft-base.service";
import {HttpService} from "../../lib/services/http.service";
import {SessionStorageService} from "../../lib/services/session-storage.service";
import {EventService} from "./event.service";


@Injectable()
export class LoginService extends FTBaseService {
    dataModel: {};

    serviceApi: string = '/ekea/user';
    authUrl: string = '/auth';
    changePwdUrl: string = '/chhangepassword';
    profileUrl: string = '/profile';

    locationUrl = "http://ip-api.com/json";

    constructor(httpService: HttpService,
                private _sessionStorageService: SessionStorageService,
                private  _eventService: EventService
    ) {
        super(httpService);

    }

    authenticate(data) {
        return this.httpService.postRequest(this.serviceApi + this.authUrl, data);
    }

    logout() {
        this._sessionStorageService.clearSession();
        let baseHref = window.location.origin;
        window.location.href = baseHref + "/webstore/home";
    }

    changePassword(data) {
        return this.httpService.postRequest(this.serviceApi + this.changePwdUrl, data);
    }

    getProfile() {
        return this.httpService.getRequest(this.serviceApi + this.profileUrl);
    }


    updateLoginStatus(loginStatus: boolean) {
        this._eventService.setLogInNotification(loginStatus);
    }


}
