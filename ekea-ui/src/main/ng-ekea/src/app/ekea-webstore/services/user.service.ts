import {Injectable} from "@angular/core";
import {FTBaseService} from "../../lib/services/ft-base.service";
import {HttpService} from "../../lib/services/http.service";
import {UserModel} from "../model/user.model";


@Injectable()
export class UserService extends FTBaseService {
    dataModel: UserModel = new UserModel();

    serviceApi: string = '/ekea/user';


    constructor(httpService: HttpService) {
        super(httpService);

    }


}
