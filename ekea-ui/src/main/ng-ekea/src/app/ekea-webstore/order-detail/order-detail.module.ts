import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OrderDetailComponent} from './order-detail.component';
import {OrderDetailRoutingModule} from "./order-detail-routing.module";

@NgModule({
    imports: [
        CommonModule,
        OrderDetailRoutingModule
    ],
    declarations: [OrderDetailComponent],
    exports: [OrderDetailComponent]
})
export class OrderDetailModule {
}
