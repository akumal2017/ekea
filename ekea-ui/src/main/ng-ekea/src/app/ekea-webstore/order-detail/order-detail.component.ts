import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {OrderDetailService} from "../services/order-detail.service";
import {OrderDetailModel} from "../model/order-detail.model";
import {ResponseModel} from "../../lib/model/response.model";
import {UserModel} from "../model/user.model";
import {LoginService} from "../services/login.service";
import {OrderService} from "../services/order.service";
import {OrderModel} from "../model/order.model";

@Component({
    selector: 'app-order-detail',
    templateUrl: './order-detail.component.html',
    styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {
    @Input("orderID")
    orderId: string;
    @Input("hideOnCms")
    hideOnCms: boolean = false;
    orderDetailList: OrderDetailModel[];
    user: UserModel;
    order: OrderModel;
    totalAmt: number;

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _orderDetailService: OrderDetailService,
        private _loginService: LoginService,
        private _orderService: OrderService
    ) {
        this.user = new UserModel();
        this.order = new OrderModel();
        this._activatedRoute.params.subscribe(params => {
            //console.log("Params: ", params.catego)
            if (params) {
                this.fetchOrderDetails(params.orderId);
                this.getOrder(params.orderId)
            } else {
                this.getOrder(this.orderId);
            }
        });
    }

    ngOnInit() {
        this.getUserProfile();
    }

    fetchOrderDetails(orderId: string) {
        this._orderDetailService.getMyDetailedOrders(orderId).then((res: ResponseModel) => {
            if (res.responseStatus) {
                this.orderDetailList = res.result;
                this.calculateTotal();
            } else {
                this.orderDetailList = [];
            }
        });
    }

    getUserProfile() {
        this._loginService.getProfile().then((res: ResponseModel) => {
            if (res.responseStatus) {
                this.user = res.result;
            }
        });
    }

    getOrder(orderId: string) {
        this._orderService.getByID(orderId).then((res: ResponseModel) => {
            if (res.responseStatus) {
                this.order = res.result;
            }
        });
    }

    calculateTotal() {
        this.totalAmt = 0;
        for (let i = 0; i < this.orderDetailList.length; i++) {
            let eachTotal = this.orderDetailList[i].quantity * this.orderDetailList[i].unitPrice;
            this.totalAmt += eachTotal;
        }
    }


}
