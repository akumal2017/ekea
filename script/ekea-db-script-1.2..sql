/*
SQLyog Community v13.0.1 (64 bit)
MySQL - 10.1.31-MariaDB : Database - ekea
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ekea` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ekea`;

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` varchar(255) NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `category` */

insert  into `category`(`id`,`version`,`description`,`type`) values 
('CAT10001FI',0,'A table is an item of furniture with a flat top and one or more legs, used as a surface for working at, eating from or on which to place things','Table'),
('CAT10002FI',0,'Bed','Bed'),
('CAT10003FI',0,'Chair','Chair'),
('CAT10004FI',0,'Wardrobe','Wardrobe');

/*Table structure for table `global_setting` */

DROP TABLE IF EXISTS `global_setting`;

CREATE TABLE `global_setting` (
  `id` varchar(255) NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `setting_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `global_setting` */

insert  into `global_setting`(`id`,`version`,`name`,`setting_value`) values 
('GLB10001FI',0,'ROOT_UPLOAD_LOCATION','../ekea-files'),
('GLB10002FI',0,'IMAGE_UPLOAD_LOCATION','/images');

/*Table structure for table `location` */

DROP TABLE IF EXISTS `location`;

CREATE TABLE `location` (
  `id` varchar(255) NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `location` */

insert  into `location`(`id`,`version`,`city`,`country`,`zip_code`) values 
('LOC10001FI',0,'Helsinki','Finland','00380'),
('LOC10002FI',0,'Stockholms','Sweden','11121'),
('LOC10003FI',0,'Saint Petersburg','Russia','33704');

/*Table structure for table `order_details` */

DROP TABLE IF EXISTS `order_details`;

CREATE TABLE `order_details` (
  `id` varchar(255) NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `unit_price` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_details_product_order` (`order_id`),
  KEY `fk_order_details_product` (`product_id`),
  CONSTRAINT `fk_order_details_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_order_details_product_order` FOREIGN KEY (`order_id`) REFERENCES `product_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `order_details` */

/*Table structure for table `payment` */

DROP TABLE IF EXISTS `payment`;

CREATE TABLE `payment` (
  `id` varchar(255) NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_payment_order` (`order_id`),
  KEY `fk_payment_user` (`user_id`),
  CONSTRAINT `fk_payment_order` FOREIGN KEY (`order_id`) REFERENCES `product_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_payment_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payment` */

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id` varchar(255) NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `img1` varchar(255) DEFAULT NULL,
  `img2` varchar(255) DEFAULT NULL,
  `img3` varchar(255) DEFAULT NULL,
  `img4` varchar(255) DEFAULT NULL,
  `material` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `category_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_cat` (`category_id`),
  CONSTRAINT `fk_product_cat` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product` */

insert  into `product`(`id`,`version`,`description`,`img1`,`img2`,`img3`,`img4`,`material`,`name`,`price`,`category_id`) values 
('PR10001FI',0,'Table top: Particleboard, Fibreboard, Acrylic paint for imitation printing, ABS plastic, Paper ',NULL,NULL,NULL,NULL,'Wood','LACK Coffee Table',250,'CAT10001FI'),
('PR10002FI',0,'On the planks of the table, there is a sense of genuine solid wood.',NULL,NULL,NULL,NULL,'Wood','MÖRBYLÅNGA Tea Table',300,'CAT10001FI'),
('PR10003FI',0,'The rustic coffee table is made of metal and full wood and has a separate shelf for magazines that you want to be close to.',NULL,NULL,NULL,NULL,'Wood','FJÄLLBO Coffee Table',100,'CAT10001FI'),
('PR10004FI',0,'Our Cavill upholstered bed frame is the perfect frame to save space. With a side opening ottoman and solid base ottoman storage you can store away anything from extra bedding to shoes and clothes whilst not having to worry about them touching the floor.\r\n',NULL,NULL,NULL,NULL,'Wood','Cavill Fabric Upholstered Ottoman Bed Frame',200,'CAT10002FI'),
('PR10005FI',0,'Upholstered in a high quality fabric and with scrolled head end and foot end, the contemporary Wilson Bed Frame will add class to your room.',NULL,NULL,NULL,NULL,'Wood','Wilson Upholstered Ottoman Bed Frame',300,'CAT10002FI'),
('PR10006FI',0,'With this versatile ottoman, you can select either a standard manual ottoman or upgrade to the electric controlled ottoman to make storing shoes, bags, blankets that little bit easier.\r\n\r\nThe easy to use remote control allows you to lift the ottoman base ',NULL,NULL,NULL,NULL,'Wood','Francis Upholstered Ottoman Bed Frame',400,'CAT10002FI');

/*Table structure for table `product_order` */

DROP TABLE IF EXISTS `product_order`;

CREATE TABLE `product_order` (
  `id` varchar(255) NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `user_Id` varchar(255) DEFAULT NULL,
  `order_status` varchar(255) DEFAULT NULL,
  `shipping_cost` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_order_user` (`user_Id`),
  CONSTRAINT `fk_product_order_user` FOREIGN KEY (`user_Id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product_order` */

/*Table structure for table `sequence_generator` */

DROP TABLE IF EXISTS `sequence_generator`;

CREATE TABLE `sequence_generator` (
  `id` varchar(255) NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `prefix` varchar(255) DEFAULT NULL,
  `sequence_name` varchar(255) DEFAULT NULL,
  `sequence_value` bigint(20) DEFAULT NULL,
  `suffix` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sequence_generator` */

insert  into `sequence_generator`(`id`,`version`,`prefix`,`sequence_name`,`sequence_value`,`suffix`) values 
('SEQ10002FI',0,'CAT','CategoryEntity',1005,'FI'),
('SEQ10003FI',0,'GBL','GlobalSettingEntity',1003,'FI'),
('SEQ10004FI',0,'LOC','LocationEntity',1004,'FI'),
('SEQ10005FI',8,'ODS','OrderDetails',1008,'FI'),
('SEQ10006FI',4,'ODR','OrderEntity',1004,'FI'),
('SEQ10007FI',4,'PAY','PaymentEntity',1004,'FI'),
('SEQ10008FI',0,'PR','ProductEntity',1000,'FI'),
('SEQ10009FI',0,'STO','StorageEntity',1007,'FI'),
('SQE10001FI',4,'USR','UserEntity',1002,'FI');

/*Table structure for table `storage` */

DROP TABLE IF EXISTS `storage`;

CREATE TABLE `storage` (
  `id` varchar(255) NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `location_id` varchar(255) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_storage_location` (`location_id`),
  KEY `fk_storage_product` (`product_id`),
  CONSTRAINT `fk_storage_location` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_storage_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `storage` */

insert  into `storage`(`id`,`version`,`quantity`,`location_id`,`product_id`) values 
('STO10001FI',0,56,'LOC10001FI','PR10001FI'),
('STO10002FI',0,67,'LOC10001FI','PR10002FI'),
('STO10003FI',0,55,'LOC10001FI','PR10003FI'),
('STO10004FI',0,87,'LOC10001FI','PR10004FI'),
('STO10005FI',0,97,'LOC10001FI','PR10005FI'),
('STO10006FI',0,40,'LOC10001FI','PR10006FI');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` varchar(255) NOT NULL,
  `version` bigint(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `admin` bit(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`version`,`address`,`city`,`country`,`email`,`admin`,`name`,`password`,`zip_code`,`mobile`,`phone`) values 
('USR1002FI',4,'Rusko','Lappeenranta','Finland','jon@gmail.com','\0','Jon','$2a$10$BFrnH8nHZ25oeKKFQKbQle9SGoS.zh11yJsNaphYP0gOvNwFp1eBK','53899','99999999999','+99323423');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
