package com.ekea.web.controller;

import com.ekea.core.constant.WebResourceConstant;
import com.ekea.core.controller.EkeaControllerBase;
import com.ekea.core.utils.impl.EkeaBeanMapperImpl;
import com.ekea.web.dto.request.PaymentRequestDto;
import com.ekea.web.dto.response.PaymentResponseDto;
import com.ekea.web.model.PaymentEntity;
import com.ekea.web.service.IPaymentService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@RestController
@RequestMapping(PaymentController.BASE_URL)
public class PaymentController extends EkeaControllerBase {
    public static final String BASE_URL = WebResourceConstant.EKEA.PAYMENT;

    public PaymentController(IPaymentService categoryService) {
        super(categoryService, new EkeaBeanMapperImpl(PaymentEntity.class, PaymentRequestDto.class), new EkeaBeanMapperImpl(PaymentEntity.class, PaymentResponseDto.class));
    }
}
