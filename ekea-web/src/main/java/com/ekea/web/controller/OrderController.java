package com.ekea.web.controller;

import com.ekea.core.constant.WebResourceConstant;
import com.ekea.core.controller.EkeaControllerBase;
import com.ekea.core.exception.EkeaException;
import com.ekea.core.model.EkeaResponseObj;
import com.ekea.core.model.EkeaTokenModel;
import com.ekea.core.utils.EkeaGlobalSettingUtils;
import com.ekea.core.utils.EkeaTokenUtils;
import com.ekea.core.utils.IEkeaBeanMapper;
import com.ekea.core.utils.impl.EkeaBeanMapperImpl;
import com.ekea.web.dto.request.OrderRequestDto;
import com.ekea.web.dto.response.OrderDetailResponseDto;
import com.ekea.web.dto.response.OrderResponseDto;
import com.ekea.web.enums.OrderStatus;
import com.ekea.web.model.OrderDetails;
import com.ekea.web.model.OrderEntity;
import com.ekea.web.model.PaymentEntity;
import com.ekea.web.model.UserEntity;
import com.ekea.web.service.IOrderDetailService;
import com.ekea.web.service.IOrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@RestController
@RequestMapping(OrderController.BASE_URL)
public class OrderController extends EkeaControllerBase {
    public static final String BASE_URL = WebResourceConstant.EKEA.ORDER;
    private IOrderService orderService;
    private IOrderDetailService orderDetailService;

    public OrderController(IOrderService orderService, IOrderDetailService orderDetailService) {
        super(orderService, new EkeaBeanMapperImpl(OrderEntity.class, OrderRequestDto.class), new EkeaBeanMapperImpl(OrderEntity.class, OrderResponseDto.class));
        this.orderService = orderService;
        this.orderDetailService = orderDetailService;
    }


    @PostMapping(WebResourceConstant.EKEA.MAKE_ORDER)
    public ResponseEntity<EkeaResponseObj> order(@RequestBody @Valid OrderRequestDto orderRequestDto) {
        OrderEntity entity = (OrderEntity) ekeaReqBeanMapper.mapToEntity(orderRequestDto);
        EkeaTokenModel ekeaTokenModel = EkeaTokenUtils.getEkeaTokenModel();
        UserEntity userEntity = new UserEntity();
        userEntity.setId(ekeaTokenModel.getUserId());
        entity.setUserEntity(userEntity);
        PaymentEntity paymentEntity = new PaymentEntity();
        paymentEntity.setAmount(orderRequestDto.getPaymentAmount());
        entity.setPaymentEntity(paymentEntity);
        entity.setOrderStatus(OrderStatus.BEING_PREPARED.getOrderStatus());
        entity.setCountry(EkeaGlobalSettingUtils.getGlobalSettingByKey(EkeaGlobalSettingUtils.COUNTRY));
        orderService.save(entity);
        // setCreateEntityProperties(entity);
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().result(entity.getId()).message("Your Order has been placed.").build(), HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.EKEA.MY_ORDERS)
    public ResponseEntity<EkeaResponseObj> myOrders() {

        List<OrderEntity> entities = this.orderService.getListByUserId(EkeaTokenUtils.getEkeaTokenModel().getUserId());
        if (entities.size() == 0) {
            throw new EkeaException("Sorry!! No Records Found");
        }
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().result(ekeaResBeanMapper.mapToDTO(entities)).message("Success").build(), HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.EKEA.MY_ORDER_DETAILS)
    public ResponseEntity<EkeaResponseObj> getOrderDetails(@PathVariable String orderId) {

        List<OrderDetails> entities = this.orderDetailService.getOrderDetailsByOrderId(orderId);
        if (entities.size() == 0) {
            throw new EkeaException("Sorry!! No Records Found");
        }
        IEkeaBeanMapper ekeaBeanMapper = new EkeaBeanMapperImpl(OrderDetails.class, OrderDetailResponseDto.class);
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().result(ekeaBeanMapper.mapToDTO(entities)).message("Success").build(), HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.EKEA.ORDER_BY_SITE)
    public ResponseEntity<EkeaResponseObj> getOrdersBySite(@PathVariable String countryName) {

        List<OrderEntity> entities = this.orderService.getOrdersByCountry(countryName);
        if (entities.size() == 0) {
            throw new EkeaException("Sorry!! No Records Found");
        }

        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().result(ekeaResBeanMapper.mapToDTO(entities)).message("Success").build(), HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.EKEA.UPDATE_BY_ORDER_STATUS)
    public ResponseEntity<EkeaResponseObj> getOrderDetails(@PathVariable String orderId, @PathVariable String orderStatus) {
        Boolean updateSuccess = this.orderService.updateStatus(orderId, orderStatus);
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().result(updateSuccess).message("Success").build(), HttpStatus.OK);
    }



}
