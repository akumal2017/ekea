package com.ekea.web.controller;

import com.ekea.core.constant.WebResourceConstant;
import com.ekea.core.controller.EkeaControllerBase;
import com.ekea.core.exception.EkeaException;
import com.ekea.core.model.EkeaResponseObj;
import com.ekea.core.model.PageModel;
import com.ekea.core.utils.IEkeaBeanMapper;
import com.ekea.core.utils.impl.EkeaBeanMapperImpl;
import com.ekea.web.dto.request.ProductRequestDto;
import com.ekea.web.dto.response.ProductResponseDto;
import com.ekea.web.model.ProductEntity;
import com.ekea.web.service.IProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@RestController
@RequestMapping(ProductController.BASE_URL)
public class ProductController extends EkeaControllerBase {
    public static final String BASE_URL = WebResourceConstant.EKEA.PRODUCT;
    private IProductService productService;
    public ProductController(IProductService productService) {
        super(productService, new EkeaBeanMapperImpl(ProductEntity.class, ProductRequestDto.class), new EkeaBeanMapperImpl(ProductEntity.class, ProductResponseDto.class));
        this.productService = productService;
    }

    @GetMapping(WebResourceConstant.EKEA.HOT_PRODUCT_LIST)
    public ResponseEntity<EkeaResponseObj> getHotProductList() {
        List<ProductEntity> productEntityList = this.productService.hotProductList();
        IEkeaBeanMapper<ProductEntity, ProductResponseDto> igqbBeanMapper =
                new EkeaBeanMapperImpl<>(ProductEntity.class, ProductResponseDto.class);
        if (productEntityList.size() == 0) {
            throw new EkeaException("Sorry!! No Records Found");
        }
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().result(igqbBeanMapper.mapToDTO(productEntityList)).message("Success").build(), HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.EKEA.PRODUCTS_BY_CATEGORY_ID_WITH_PAGE)
    public ResponseEntity<EkeaResponseObj> getProductsByCategoryIdWithPage(@PathVariable String categoryId, @PathVariable Integer currentPage, @PathVariable Integer pageSize) {
        List<ProductEntity> productEntityList = this.productService.getProductsByCategoryId(categoryId, currentPage, pageSize);
        IEkeaBeanMapper<ProductEntity, ProductResponseDto> igqbBeanMapper =
                new EkeaBeanMapperImpl<>(ProductEntity.class, ProductResponseDto.class);
        Long totalRecords = 0L;
        if (productEntityList.size() == 0) {
            throw new EkeaException("Sorry!! No Records Found");
        } else {
            totalRecords = this.productService.getCountByCategoryId(categoryId);
        }
        PageModel pageModel = new PageModel(currentPage, totalRecords, pageSize);
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().page(pageModel).result(igqbBeanMapper.mapToDTO(productEntityList)).message("Success").build(), HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.EKEA.PRODUCTS_BY_CATEGORY_ID)
    public ResponseEntity<EkeaResponseObj> getProductsByCategoryId(@PathVariable String categoryId) {
        List<ProductEntity> productEntityList = this.productService.getProductsByCategoryId(categoryId);
        IEkeaBeanMapper<ProductEntity, ProductResponseDto> igqbBeanMapper =
                new EkeaBeanMapperImpl<>(ProductEntity.class, ProductResponseDto.class);
        if (productEntityList.size() == 0) {
            throw new EkeaException("Sorry!! No Records Found");
        }
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().result(igqbBeanMapper.mapToDTO(productEntityList)).message("Success").build(), HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.GET_ALL_WITH_PAGE)
    public ResponseEntity<EkeaResponseObj> getProducts(@PathVariable Integer currentPage, @PathVariable Integer pageSize) {
        List<ProductEntity> productEntityList = this.productService.getList(currentPage, pageSize);
        IEkeaBeanMapper<ProductEntity, ProductResponseDto> igqbBeanMapper =
                new EkeaBeanMapperImpl<>(ProductEntity.class, ProductResponseDto.class);
        Long totalRecords = 0L;

        if (productEntityList.size() == 0) {
            throw new EkeaException("Sorry!! No Records Found");
        } else {
            totalRecords = this.productService.count();
        }
        PageModel pageModel = new PageModel(currentPage, totalRecords, pageSize);
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().page(pageModel).result(igqbBeanMapper.mapToDTO(productEntityList)).message("Success").build(), HttpStatus.OK);
    }


}
