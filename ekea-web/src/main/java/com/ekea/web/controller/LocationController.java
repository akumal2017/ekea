package com.ekea.web.controller;

import com.ekea.core.constant.WebResourceConstant;
import com.ekea.core.controller.EkeaControllerBase;
import com.ekea.core.utils.impl.EkeaBeanMapperImpl;
import com.ekea.web.dto.request.LocationRequestDto;
import com.ekea.web.dto.response.LocationResponseDto;
import com.ekea.web.model.LocationEntity;
import com.ekea.web.service.ILocationService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@RestController
@RequestMapping(LocationController.BASE_URL)
public class LocationController extends EkeaControllerBase {
    public static final String BASE_URL = WebResourceConstant.EKEA.LOCATION;

    public LocationController(ILocationService categoryService) {
        super(categoryService, new EkeaBeanMapperImpl(LocationEntity.class, LocationRequestDto.class), new EkeaBeanMapperImpl(LocationEntity.class, LocationResponseDto.class));
    }
}
