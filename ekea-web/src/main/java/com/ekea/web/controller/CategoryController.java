package com.ekea.web.controller;


import com.ekea.core.constant.WebResourceConstant;
import com.ekea.core.controller.EkeaControllerBase;
import com.ekea.core.utils.impl.EkeaBeanMapperImpl;
import com.ekea.web.dto.request.CategoryRequestDto;
import com.ekea.web.dto.response.CategoryResponseDto;
import com.ekea.web.model.CategoryEntity;
import com.ekea.web.service.ICategoryService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@RestController
@RequestMapping(CategoryController.BASE_URL)
public class CategoryController extends EkeaControllerBase {
    public static final String BASE_URL = WebResourceConstant.EKEA.CATEGORY;

    public CategoryController(ICategoryService categoryService) {
        super(categoryService, new EkeaBeanMapperImpl(CategoryEntity.class, CategoryRequestDto.class), new EkeaBeanMapperImpl(CategoryEntity.class, CategoryResponseDto.class));
    }
}
