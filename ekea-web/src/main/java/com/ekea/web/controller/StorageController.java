package com.ekea.web.controller;

import com.ekea.core.constant.WebResourceConstant;
import com.ekea.core.controller.EkeaControllerBase;
import com.ekea.core.utils.impl.EkeaBeanMapperImpl;
import com.ekea.web.dto.request.StorageRequestDto;
import com.ekea.web.dto.response.StorageResponseDto;
import com.ekea.web.model.StorageEntity;
import com.ekea.web.service.IStorageService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@RestController
@RequestMapping(StorageController.BASE_URL)
public class StorageController extends EkeaControllerBase {
    public static final String BASE_URL = WebResourceConstant.EKEA.STORAGE;

    public StorageController(IStorageService storageService) {
        super(storageService, new EkeaBeanMapperImpl(StorageEntity.class, StorageRequestDto.class), new EkeaBeanMapperImpl(StorageEntity.class, StorageResponseDto.class));
    }
}
