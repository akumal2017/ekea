package com.ekea.web.controller;

import com.ekea.core.constant.WebResourceConstant;
import com.ekea.core.model.EkeaResponseObj;
import com.ekea.core.model.FileInfoModel;
import com.ekea.core.utils.EkeaGlobalSettingUtils;
import com.ekea.core.utils.MultiPartFileUtils;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.util.UUID;

/**
 * Created by Anil Kumal on 12/01/2018.
 */
@RestController
@RequestMapping(ImageUploadController.BASE_URL)
public class ImageUploadController {
    public static final String BASE_URL = WebResourceConstant.EKEA.EKEA_BASE;

    @Autowired
    private HttpServletRequest httpServletRequest;

    public ImageUploadController(HttpServletRequest httpServletRequest) {
    }


    @PostMapping(WebResourceConstant.UPLOAD)
    public ResponseEntity<EkeaResponseObj> singleFileUpload(@RequestParam("uploadFile") MultipartFile uploadFile) {

        FileInfoModel fileInfoModel = new FileInfoModel.FileInfoBuilder()
                .folderName(EkeaGlobalSettingUtils.getGlobalSettingByKey(EkeaGlobalSettingUtils.IMAGE_UPLOAD_LOCATION))
                .rootLocation(EkeaGlobalSettingUtils.getGlobalSettingByKey(EkeaGlobalSettingUtils.ROOT_UPLOAD_LOCATION))
                .multipartFile(uploadFile)
                .build();
        String newFilename = this.getRandomName();
        MultiPartFileUtils.writeandRenameFile(fileInfoModel, newFilename);
        EkeaResponseObj fortunaResponseObj = new EkeaResponseObj.EkeaResponseObjBuilder().result(newFilename).message("success").build();
        return new ResponseEntity<>(fortunaResponseObj, HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.DISPLAY_FILE)
    public ResponseEntity<byte[]> displayFile(@PathVariable String fileName) throws FileNotFoundException {
        HttpHeaders httpHeaders = new HttpHeaders();
//        System.out.println("image = " + image);
        System.out.println("fileName = " + fileName);

        FileInfoModel fileInfoModel = new FileInfoModel.
                FileInfoBuilder().image(fileName)
                .folderName(EkeaGlobalSettingUtils.getGlobalSettingByKey(EkeaGlobalSettingUtils.IMAGE_UPLOAD_LOCATION))
                .rootLocation(EkeaGlobalSettingUtils.getGlobalSettingByKey(EkeaGlobalSettingUtils.ROOT_UPLOAD_LOCATION))
                .build();
        return new ResponseEntity<>(MultiPartFileUtils.readFile(fileInfoModel), httpHeaders, HttpStatus.OK);

    }

    private String getRandomName() {
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        return randomUUIDString;
    }


}
