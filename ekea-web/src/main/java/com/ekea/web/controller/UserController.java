package com.ekea.web.controller;

import com.ekea.core.constant.WebResourceConstant;
import com.ekea.core.controller.EkeaControllerBase;
import com.ekea.core.exception.EkeaException;
import com.ekea.core.model.EkeaResponseObj;
import com.ekea.core.model.EkeaTokenModel;
import com.ekea.core.utils.EkeaTokenUtils;
import com.ekea.core.utils.impl.EkeaBeanMapperImpl;
import com.ekea.web.dto.request.ChangePasswordRequestDto;
import com.ekea.web.dto.request.UserLoginRequestDto;
import com.ekea.web.dto.request.UserRequestDto;
import com.ekea.web.dto.response.UserResponseDto;
import com.ekea.web.model.UserEntity;
import com.ekea.web.security.IEkeaToken;
import com.ekea.web.service.IUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@RestController
@RequestMapping(UserController.BASE_URL)
public class UserController extends EkeaControllerBase {
    public static final String BASE_URL = WebResourceConstant.EKEA.USER;
    private IUserService userService;
    private IEkeaToken ekeaToken;

    public UserController(IUserService userService, IEkeaToken ekeaToken) {
        super(userService, new EkeaBeanMapperImpl(UserEntity.class, UserRequestDto.class), new EkeaBeanMapperImpl(UserEntity.class, UserResponseDto.class));
        this.userService = userService;
        this.ekeaToken = ekeaToken;
    }

    @PostMapping(WebResourceConstant.UserManagement.CHANGE_PASSWORD)
    public ResponseEntity<EkeaResponseObj> changePassword(@RequestBody @Valid ChangePasswordRequestDto changePasswordRequestDto) {
        if (changePasswordRequestDto.getNewPassword().equals(changePasswordRequestDto.getConfirmPassword())) {
            EkeaTokenModel ekeaTokenModel = EkeaTokenUtils.getEkeaTokenModel();
            this.userService.changePassword(changePasswordRequestDto.getOldPassword(), changePasswordRequestDto.getNewPassword(), ekeaTokenModel.getUserId());
        } else {
            throw new EkeaException("Confirmed Password Didn't match With New Password");
        }

        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().message("Password has been Changed Successfully.").build(), HttpStatus.OK);
    }

    @PostMapping(WebResourceConstant.UserManagement.UM_AUTHENTICATE)
    public ResponseEntity<EkeaResponseObj> authenticateUser(@RequestBody @Valid UserLoginRequestDto userLoginRequestDto) {
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail(userLoginRequestDto.getEmail());
        userEntity.setPassword(userLoginRequestDto.getPassword());
        UserEntity authenticUser = this.userService.authenticate(userEntity);

        if (authenticUser == null) {
            throw new EkeaException("Sorry!! Your email address or  password doesn't match");
        }


        String token = ekeaToken.generateToken(authenticUser);
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("token", token);
        responseMap.put("isAdmin", authenticUser.getIsAdmin());
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().result(responseMap).build(), HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.UserManagement.GET_PROFILE_DETAIL)
    public ResponseEntity<EkeaResponseObj> authenticateUser() {
        EkeaTokenModel ekeaTokenModel = EkeaTokenUtils.getEkeaTokenModel();
        if (ekeaTokenModel == null) {
            throw new EkeaException("Your session has been expired. Please sign in and try again");
        }
        UserEntity userEntity = this.userService.findOne(ekeaTokenModel.getUserId());
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().result(ekeaResBeanMapper.mapToDTO(userEntity)).message("Success").build(), HttpStatus.OK);
    }

}
