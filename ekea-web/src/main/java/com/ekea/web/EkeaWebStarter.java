package com.ekea.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * Created by anil on 6/2/17.
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class},
        scanBasePackages = {"com.ekea"}
)
public class EkeaWebStarter {
    public static void main(String[] args) {
        SpringApplication.run(EkeaWebStarter.class, args);
    }


}



