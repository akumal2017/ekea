package com.ekea.web.config;

import com.ekea.core.constant.WebResourceConstant;
import com.ekea.core.utils.EkeaGlobalSettingUtils;
import com.ekea.core.utils.EkeaStringUtils;
import com.ekea.core.utils.EkeaTokenUtils;
import com.ekea.web.security.IEkeaToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
public class AuthenticationHandlerInterceptor extends HandlerInterceptorAdapter {


    private static List<String> authorizationFreeuriList = new ArrayList<>();

    static {
        authorizationFreeuriList.add("/auth");
        authorizationFreeuriList.add("/product");
        authorizationFreeuriList.add("/product");
        authorizationFreeuriList.add("/product");
        authorizationFreeuriList.add("/product");
        authorizationFreeuriList.add("/category");
        authorizationFreeuriList.add("/user/create");
        authorizationFreeuriList.add("/upload");
        authorizationFreeuriList.add("/display");

    }

    @Autowired
    private IEkeaToken ekeaToken;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (request.getMethod().equalsIgnoreCase("OPTIONS")) {
            return true;
        }
        String ip = request.getHeader(WebResourceConstant.IP);
        String country = request.getHeader(WebResourceConstant.COUNTRY);
        String lat = request.getHeader(WebResourceConstant.LAT);
        String lon = request.getHeader(WebResourceConstant.LON);

        EkeaGlobalSettingUtils.put(EkeaGlobalSettingUtils.IP, ip);
        EkeaGlobalSettingUtils.put(EkeaGlobalSettingUtils.COUNTRY, country);
        EkeaGlobalSettingUtils.put(EkeaGlobalSettingUtils.LAT, lat);
        EkeaGlobalSettingUtils.put(EkeaGlobalSettingUtils.LAT, lon);
        String uri = request.getRequestURI();
//        System.out.println("uri = " + uri);
        String accessToken;
        String origin = request.getHeader("Origin");
        response.setHeader("Access-Control-Allow-Origin", origin);
        response.setHeader("Access-Control-Allow-Methods", "*");
        response.setHeader("Access-Control-Allow-Headers", "*");
        System.out.println("request Uri = " + uri);
        if (isAuthFreeUri(uri) && !uri.contains("/product/create")) {
            System.out.println("isAuthFreeUri() = returing true from authfree ");
            return true;
        }
        accessToken = request.getHeader(WebResourceConstant.AUTHORIZATION_HEADER);

        if (EkeaStringUtils.isNull(accessToken) && !isAuthFreeUri(uri)) {
            // throw new EkeaException("Unauthorized access!!");
        }

        if (EkeaStringUtils.isNotNull(accessToken)) {
            EkeaTokenUtils.setEkeaTokenModel(ekeaToken.parseToken(accessToken));
            System.out.println("EkeaTokenUtils.getEkeaTokenModel() = " + EkeaTokenUtils.getEkeaTokenModel().toString());
        }


        return true;
    }

    private boolean isAuthFreeUri(String uri) {
        if (EkeaStringUtils.isNull(uri)) return false;
        for (String authFreeUri : authorizationFreeuriList) {
            System.out.println("each-authFreeUri = " + authFreeUri);
            if (uri.contains(authFreeUri)) {
                System.out.println("check-authFreeUri = uri " + uri);
                System.out.println("check-authFreeUri = authFreeUri " + authFreeUri);
                return true;
            }
        }
        return false;
    }


}
