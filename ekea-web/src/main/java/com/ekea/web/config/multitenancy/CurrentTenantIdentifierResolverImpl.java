package com.ekea.web.config.multitenancy;

import com.ekea.core.utils.EkeaGlobalSettingUtils;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;

@Component
public class CurrentTenantIdentifierResolverImpl implements CurrentTenantIdentifierResolver {
    public static String DEFAULT_TENANTID = "FINLAND";

    @Override
    public String resolveCurrentTenantIdentifier() {
        String tenantId = EkeaGlobalSettingUtils.getGlobalSettingByKey(EkeaGlobalSettingUtils.COUNTRY);
        if (tenantId != null) {
            return tenantId;
        }
        return DEFAULT_TENANTID;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
}
