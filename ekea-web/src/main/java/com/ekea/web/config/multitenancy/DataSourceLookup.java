package com.ekea.web.config.multitenancy;

import com.ekea.core.constant.WebResourceConstant;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class DataSourceLookup {
    private Map<String, DataSource> dataSourceMap = new HashMap<>();
    private String url1 = "jdbc:mysql://ekea-fi.chqdujyjqjk4.us-east-2.rds.amazonaws.com/ekeafi?zeroDateTimeBehavior=convertToNull";
    private String url2 = "jdbc:mysql://ekea-sd.chqdujyjqjk4.us-east-2.rds.amazonaws.com/ekeasd?zeroDateTimeBehavior=convertToNull";
    private String username = "ekea";
    private String password = "ekeaproject";
    private String driverClassName = "com.mysql.jdbc.Driver";

    public DataSourceLookup() {
        System.out.println("Initializing Datasource ...........................................................");
        this.initDataSourceMap();
    }

    public DataSource getDataSourceByTenantId(String tenantId) {
        return this.dataSourceMap.get(tenantId);
    }

    private void initDataSourceMap() {


        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(this.driverClassName);
        hikariConfig.setJdbcUrl(this.url1);
        hikariConfig.setUsername(this.username);
        hikariConfig.setPassword(this.password);

        hikariConfig.setMaximumPoolSize(5);
        hikariConfig.setConnectionTestQuery("SELECT 1");
        hikariConfig.setPoolName("ekeafi-pool");

        hikariConfig.addDataSourceProperty("dataSource.cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSize", "250");
        hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSqlLimit", "2048");
        hikariConfig.addDataSourceProperty("dataSource.useServerPrepStmts", "true");

        HikariDataSource ekeafi = new HikariDataSource(hikariConfig);
        hikariConfig.setJdbcUrl(this.url2);
        hikariConfig.setPoolName("ekeasd-pool");
        HikariDataSource ekeasd = new HikariDataSource(hikariConfig);


        this.dataSourceMap.put(WebResourceConstant.TENANT.FINLAND, ekeafi);
        this.dataSourceMap.put(WebResourceConstant.TENANT.SWEDEN, ekeasd);
        System.out.println("Initializing Datasource = " + dataSourceMap);
    }
}
