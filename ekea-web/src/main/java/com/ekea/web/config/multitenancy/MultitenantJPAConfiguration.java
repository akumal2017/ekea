package com.ekea.web.config.multitenancy;

import com.ekea.core.constant.WebResourceConstant;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class MultitenantJPAConfiguration {
    private String url1 = "jdbc:mysql://ekea-fi.chqdujyjqjk4.us-east-2.rds.amazonaws.com/ekeafi?zeroDateTimeBehavior=convertToNull";
    private String url2 = "jdbc:mysql://ekea-sd.chqdujyjqjk4.us-east-2.rds.amazonaws.com/ekeasd?zeroDateTimeBehavior=convertToNull";
    private String url3 = "jdbc:mysql://ekea-ru.chqdujyjqjk4.us-east-2.rds.amazonaws.com/ekearu?zeroDateTimeBehavior=convertToNull";
    private String username = "ekea";
    private String password = "ekeaproject";
    private String driverClassName = "com.mysql.jdbc.Driver";

    @Bean(name = "dataSourcesEkea")
    public Map<String, DataSource> dataSourcesEkea() {
        Map<String, DataSource> result = new HashMap<>();
        result.put(WebResourceConstant.TENANT.FINLAND, ekeafiDatasource());
        result.put(WebResourceConstant.TENANT.SWEDEN, ekeasdDataSource());
        result.put(WebResourceConstant.TENANT.RUSSIA, ekeaRuDataSource());
        return result;
    }

    private DataSource ekeafiDatasource() {

        HikariConfig hikariConfig = this.getHikariConfig();
        hikariConfig.setDriverClassName(this.driverClassName);
        hikariConfig.setJdbcUrl(this.url1);
        HikariDataSource ekeafi = new HikariDataSource(hikariConfig);
        return ekeafi;
    }

    private DataSource ekeasdDataSource() {
        HikariConfig hikariConfig = this.getHikariConfig();
        hikariConfig.setDriverClassName(this.driverClassName);
        hikariConfig.setJdbcUrl(this.url2);
        HikariDataSource ekeafi = new HikariDataSource(hikariConfig);
        return ekeafi;

    }

    private DataSource ekeaRuDataSource() {
        HikariConfig hikariConfig = this.getHikariConfig();
        hikariConfig.setJdbcUrl(this.url3);
        HikariDataSource ekeafi = new HikariDataSource(hikariConfig);
        return ekeafi;

    }

    private HikariConfig getHikariConfig() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(this.driverClassName);
        hikariConfig.setUsername(this.username);
        hikariConfig.setPassword(this.password);
        hikariConfig.setMaximumPoolSize(5);
        hikariConfig.setConnectionTestQuery("SELECT 1");
        hikariConfig.addDataSourceProperty("dataSource.cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSize", "250");
        hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSqlLimit", "2048");
        hikariConfig.addDataSourceProperty("dataSource.useServerPrepStmts", "true");
        return hikariConfig;
    }
}
