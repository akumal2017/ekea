package com.ekea.web.config;

import org.hibernate.MultiTenancyStrategy;
import org.hibernate.cfg.Environment;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Configuration
@EnableJpaRepositories(basePackages = {"com.ekea"}, transactionManagerRef = "txManager")
@EnableTransactionManagement(proxyTargetClass = true)
//@EnableAutoConfiguration
public class AppConfig {

//    @Value("${spring.datasource.username}")
//    private String userName;
//    @Value("${spring.datasource.password}")
//    private String password;
//    @Value("${spring.datasource.url}")
//    private String url;
//    @Value("${spring.datasource.driverClassName}")
//    private String driverClassName;
//    @Value("${spring.jpa.database-platform}")
//    private String databasePlatform;

//    @Bean
//
//    @ConfigurationProperties(prefix = "spring.datasource")
//    public DataSource getDataSource() {
//
//        DataSourceBuilder factory = DataSourceBuilder.create()
//                .driverClassName(this.driverClassName)
//                .username(this.userName)
//                .password(this.password)
//                .url(this.url);
//        return factory.build();


//        PoolProperties poolProperties = new PoolProperties();
//        poolProperties.setUrl(this.url);
//        poolProperties.setUsername(this.userName);
//        poolProperties.setPassword(this.password);
//        poolProperties.setDriverClassName(this.driverClassName);
//
//        //Configuration for auto re-connect
//        poolProperties.setTestOnBorrow(true);
//        poolProperties.setTestWhileIdle(true);
//        poolProperties.setTimeBetweenEvictionRunsMillis(60000);
//        poolProperties.setValidationQuery("SELECT 1");
//        return new DataSource(poolProperties);
//    }

    @Bean("ekeasd")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource getSdDataSource() {
        String url = "jdbc:mysql://ekea-sd.chqdujyjqjk4.us-east-2.rds.amazonaws.com/ekeasd?zeroDateTimeBehavior=convertToNull";
        DataSourceBuilder factory = DataSourceBuilder.create()
                .driverClassName(this.driverClassName)
                .username(this.userName)
                .password(this.password)
                .url(this.url);
        return factory.build();

    }

    @Bean("ekearu")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource getRuDataSource() {
        String url = "jdbc:mysql://ekea-ru.chqdujyjqjk4.us-east-2.rds.amazonaws.com/ekearu?zeroDateTimeBehavior=convertToNull";
        DataSourceBuilder factory = DataSourceBuilder.create()
                .driverClassName(this.driverClassName)
                .username(this.userName)
                .password(this.password)
                .url(this.url);
        return factory.build();

    }

    //    @PersistenceContext
//    @Primary
//    @Bean
//    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
//
//        LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
//
//        Map<String, Object> props = new HashMap<>();
////        props.put("hibernate.ejb.interceptor", new DbInterceptor());
//        localContainerEntityManagerFactoryBean.setDataSource(getDataSource());
//        localContainerEntityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter());
//        localContainerEntityManagerFactoryBean.setPackagesToScan("com.EKEA");
//        localContainerEntityManagerFactoryBean.setJpaPropertyMap(props);
//        localContainerEntityManagerFactoryBean.setPersistenceUnitName("EkeaPersistenceUnit");
//        return localContainerEntityManagerFactoryBean;
//    }
    @Bean
    @Primary
    public synchronized LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(MultiTenantConnectionProvider multiTenantConnectionProvider,
                                                                                        CurrentTenantIdentifierResolver currentTenantIdentifierResolver) {

        Map<String, Object> hibernateProps = new LinkedHashMap<>();
        //hibernateProps.put("hibernate.transaction.factory_class",org.hibernate.transaction.JTATransactionFactory.class);
//    hibernateProps.put("hibernate.transaction.manager_lookup_class",com.atomikos.icatch.jta.hibernate3.TransactionManagerLookup.class);
        hibernateProps.put(Environment.MULTI_TENANT, MultiTenancyStrategy.DATABASE);
        hibernateProps.put(Environment.MULTI_TENANT_CONNECTION_PROVIDER, multiTenantConnectionProvider);
        hibernateProps.put(Environment.MULTI_TENANT_IDENTIFIER_RESOLVER, currentTenantIdentifierResolver);

        // No dataSource is set to resulting entityManagerFactoryBean
        LocalContainerEntityManagerFactoryBean result = new LocalContainerEntityManagerFactoryBean();
        result.setPackagesToScan("com.ekea");
        result.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        result.setJpaPropertyMap(hibernateProps);

        return result;
    }

//    @Bean ("entityManagerFactory")
//
//    public  EntityManagerFactory entityManagerFactory(LocalContainerEntityManagerFactoryBean entityManagerFactoryBean) {
//        return entityManagerFactoryBean.getObject();
//    }


    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(true);
        //set to true to generate tables from Entity
        hibernateJpaVendorAdapter.setGenerateDdl(true);
        hibernateJpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.MySQL5Dialect");
//        hibernateJpaVendorAdapter.setDatabasePlatform(this.databasePlatform);
        hibernateJpaVendorAdapter.setShowSql(true);
        return hibernateJpaVendorAdapter;
    }

    @Bean
    public PlatformTransactionManager txManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
        // return new JpaTransactionManager(entityManagerFactory);
    }


}
