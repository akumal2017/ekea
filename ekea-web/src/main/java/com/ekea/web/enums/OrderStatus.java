package com.ekea.web.enums;

public enum OrderStatus {
    BEING_PREPARED("Being Prepared"),

    RECEIVED("Received"),
    CANCLLED("Cancelled"),
    ON_HOLD("On Hold");

    private String orderStatus;

    OrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }


    public String getOrderStatus() {
        return this.orderStatus;
    }

}
