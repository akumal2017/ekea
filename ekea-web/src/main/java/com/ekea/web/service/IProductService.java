package com.ekea.web.service;


import com.ekea.core.service.ICrudService;
import com.ekea.web.model.ProductEntity;

import java.util.List;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface IProductService extends ICrudService<ProductEntity, String> {
    List<ProductEntity> hotProductList();

    List<ProductEntity> getProductsByCategoryId(String categoryId);

    List<ProductEntity> getProductsByCategoryId(String categoryId, Integer currentPage, Integer pageSize);

    Long getCountByCategoryId(String categoryId);

    List<ProductEntity> getList(Integer currentPage, Integer pageSize);
}
