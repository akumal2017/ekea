package com.ekea.web.service.impl;


import com.ekea.core.service.impl.CrudServiceImpl;
import com.ekea.web.model.GlobalSettingEntity;
import com.ekea.web.repository.IGlobalSettingRepository;
import com.ekea.web.service.IGlobalSettingService;
import org.springframework.stereotype.Service;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Service
public class GlobalSettingServiceImpl extends CrudServiceImpl<GlobalSettingEntity, String> implements IGlobalSettingService {

    public GlobalSettingServiceImpl(IGlobalSettingRepository storageRepository) {
        super(storageRepository, GlobalSettingEntity.class);

    }


}
