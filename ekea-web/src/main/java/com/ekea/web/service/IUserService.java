package com.ekea.web.service;


import com.ekea.core.service.ICrudService;
import com.ekea.web.model.UserEntity;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface IUserService extends ICrudService<UserEntity, String> {
    UserEntity authenticate(UserEntity userEntity);

    Boolean changePassword(String oldPassword, String newPassword, String userId);

    UserEntity getUserByEmail(String email);
}
