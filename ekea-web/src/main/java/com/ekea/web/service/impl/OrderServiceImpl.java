package com.ekea.web.service.impl;


import com.ekea.core.service.impl.CrudServiceImpl;
import com.ekea.web.domain.OrderDetailDomain;
import com.ekea.web.model.*;
import com.ekea.web.repository.IOrderRepository;
import com.ekea.web.service.*;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Service
public class OrderServiceImpl extends CrudServiceImpl<OrderEntity, String> implements IOrderService {
    private IOrderDetailService orderDetailService;
    private IProductService productService;
    private IPaymentService paymentService;
    private IStorageService storageService;
    private IOrderRepository orderRepository;

    public OrderServiceImpl(IOrderRepository orderRepository, IOrderDetailService orderDetailService, IProductService productService, IPaymentService paymentService, IStorageService storageService) {
        super(orderRepository, OrderEntity.class);
        this.orderDetailService = orderDetailService;
        this.productService = productService;
        this.paymentService = paymentService;
        this.storageService = storageService;
        this.orderRepository = orderRepository;

    }

    @Override
    public OrderEntity save(OrderEntity entity) {
        entity.setOrderDate(new Date());
        entity = super.save(entity);
        this.savePayment(entity);
        this.saveOrderDetails(entity);
        return entity;
    }

    private void saveOrderDetails(OrderEntity orderEntity) {
        for (OrderDetailDomain orderDetailDomain : orderEntity.getOrderDetailsList()) {
            OrderDetails orderDetails = new OrderDetails();
            OrderEntity order = new OrderEntity();
            order.setId(orderEntity.getId());
            ProductEntity productEntity = new ProductEntity();
            productEntity.setId(orderDetailDomain.getProductEntityId());
            orderDetails.setOrderEntity(orderEntity);
            orderDetails.setProductEntity(productEntity);
            orderDetails.setUnitPrice(this.productService.findOne(orderDetailDomain.getProductEntityId()).getPrice());
            orderDetails.setQuantity(orderDetailDomain.getQuantity());
            this.orderDetailService.save(orderDetails);
            this.updateProductQuantityOfStore(orderDetailDomain.getProductEntityId(), orderDetailDomain.getQuantity());
        }
    }

    private void savePayment(OrderEntity orderEntity) {
        PaymentEntity paymentEntity = new PaymentEntity();
        paymentEntity.setOrderEntity(orderEntity);
        paymentEntity.setUserEntity(orderEntity.getUserEntity());
        paymentEntity.setAmount(orderEntity.getPaymentEntity().getAmount());
        this.paymentService.save(paymentEntity);
    }

    private void updateProductQuantityOfStore(String productId, Integer quantity) {
        StorageEntity storageEntity = this.storageService.findByProductId(productId);
        Integer updatedQuantity = storageEntity.getQuantity() - quantity;
        storageEntity.setQuantity(updatedQuantity);
        this.storageService.update(storageEntity);

    }


    @Override
    public List<OrderEntity> getListByUserId(String userId) {
        return this.orderRepository.getListByUserId(userId);
    }

    @Override
    public Boolean updateStatus(String orderId, String orderStatus) {
        OrderEntity orderEntity = this.orderRepository.findOne(orderId);
        orderEntity.setOrderStatus(orderStatus);
        super.update(orderEntity);
        return true;
    }

    @Override
    public List<OrderEntity> getOrdersByCountry(String country) {
        return this.orderRepository.getOrdersByCountry(country);
    }


}


