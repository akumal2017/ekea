package com.ekea.web.service;


import com.ekea.core.service.ICrudService;
import com.ekea.web.model.PaymentEntity;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface IPaymentService extends ICrudService<PaymentEntity, String> {
}
