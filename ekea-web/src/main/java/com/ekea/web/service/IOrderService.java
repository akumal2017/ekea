package com.ekea.web.service;


import com.ekea.core.service.ICrudService;
import com.ekea.web.model.OrderEntity;

import java.util.List;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface IOrderService extends ICrudService<OrderEntity, String> {
    public List<OrderEntity> getListByUserId(String userId);

    public Boolean updateStatus(String orderId, String orderStatus);

    public List<OrderEntity> getOrdersByCountry(String country);
}
