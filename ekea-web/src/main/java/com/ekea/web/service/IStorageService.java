package com.ekea.web.service;


import com.ekea.core.service.ICrudService;
import com.ekea.web.model.StorageEntity;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface IStorageService extends ICrudService<StorageEntity, String> {
    StorageEntity findByProductId(String productId);

    StorageEntity findByLocationName(String country);
}
