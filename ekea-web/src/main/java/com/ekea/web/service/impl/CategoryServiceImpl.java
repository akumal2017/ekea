package com.ekea.web.service.impl;


import com.ekea.core.service.impl.CrudServiceImpl;
import com.ekea.web.model.CategoryEntity;
import com.ekea.web.repository.ICategoryRepository;
import com.ekea.web.service.ICategoryService;
import org.springframework.stereotype.Service;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Service
public class CategoryServiceImpl extends CrudServiceImpl<CategoryEntity, String> implements ICategoryService {

    public CategoryServiceImpl(ICategoryRepository categoryRepository) {
        super(categoryRepository, CategoryEntity.class);

    }


}
