package com.ekea.web.service;


import com.ekea.core.service.ICrudService;
import com.ekea.web.model.OrderDetails;

import java.util.List;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface IOrderDetailService extends ICrudService<OrderDetails, String> {
    public List<OrderDetails> getOrderDetailsByOrderId(String orderId);

}
