package com.ekea.web.service;


import com.ekea.core.service.ICrudService;
import com.ekea.web.model.CategoryEntity;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface ICategoryService extends ICrudService<CategoryEntity, String> {
}
