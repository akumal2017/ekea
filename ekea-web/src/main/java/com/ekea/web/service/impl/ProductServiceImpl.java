package com.ekea.web.service.impl;


import com.ekea.core.model.EkeaTokenModel;
import com.ekea.core.service.impl.CrudServiceImpl;
import com.ekea.core.utils.EkeaTokenUtils;
import com.ekea.web.model.LocationEntity;
import com.ekea.web.model.ProductEntity;
import com.ekea.web.model.StorageEntity;
import com.ekea.web.repository.IProductRepository;
import com.ekea.web.service.ILocationService;
import com.ekea.web.service.IProductService;
import com.ekea.web.service.IStorageService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Service
public class ProductServiceImpl extends CrudServiceImpl<ProductEntity, String> implements IProductService {
    private IProductRepository productRepository;
    private IStorageService storageService;
    private ILocationService locationService;

    public ProductServiceImpl(IProductRepository productRepository, IStorageService storageService, ILocationService locationService) {
        super(productRepository, ProductEntity.class);
        this.productRepository = productRepository;
        this.storageService = storageService;
        this.locationService = locationService;
    }

    @Override
    public ProductEntity save(ProductEntity entity) {
        entity.setId("");
        EkeaTokenModel ekeaTokenModel = EkeaTokenUtils.getEkeaTokenModel();
        LocationEntity locationEntity = this.locationService.getByCountryName(EkeaTokenUtils.getEkeaTokenModel().getOriginCountry());

        super.save(entity);
        StorageEntity storageEntity = new StorageEntity();
        storageEntity.setQuantity(20);
        storageEntity.setProductEntity(entity);
        storageEntity.setLocationEntity(locationEntity);
        this.storageService.save(storageEntity);
        return entity;
    }


    @Override
    public List<ProductEntity> hotProductList() {
        return this.productRepository.getHotProducts();
    }

    @Override
    public List<ProductEntity> getProductsByCategoryId(String categoryId) {
        return productRepository.getProductsByCategoryId(categoryId);
    }

    @Override
    public List<ProductEntity> getProductsByCategoryId(String categoryId, Integer currentPage, Integer pageSize) {
        return this.productRepository.getProductsByCategoryId(categoryId, currentPage, pageSize);
    }

    @Override
    public Long getCountByCategoryId(String categoryId) {
        return this.productRepository.getCountByCategoryId(categoryId);
    }

    @Override
    public List<ProductEntity> getList(Integer currentPage, Integer pageSize) {
        return this.productRepository.getList(currentPage, pageSize);
    }


}
