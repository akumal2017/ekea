package com.ekea.web.service.impl;


import com.ekea.core.service.impl.CrudServiceImpl;
import com.ekea.web.model.PaymentEntity;
import com.ekea.web.repository.IPaymentRepository;
import com.ekea.web.service.IPaymentService;
import org.springframework.stereotype.Service;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Service
public class PaymentServiceImpl extends CrudServiceImpl<PaymentEntity, String> implements IPaymentService {

    public PaymentServiceImpl(IPaymentRepository paymentRepository) {
        super(paymentRepository, PaymentEntity.class);

    }


}
