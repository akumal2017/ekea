package com.ekea.web.service;


import com.ekea.core.service.ICrudService;
import com.ekea.web.model.LocationEntity;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface ILocationService extends ICrudService<LocationEntity, String> {
    LocationEntity getByCountryName(String country);
}
