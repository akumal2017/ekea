package com.ekea.web.service.impl;


import com.ekea.core.service.impl.CrudServiceImpl;
import com.ekea.web.model.LocationEntity;
import com.ekea.web.repository.ILocationRepository;
import com.ekea.web.service.ILocationService;
import org.springframework.stereotype.Service;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Service
public class LocationServiceImpl extends CrudServiceImpl<LocationEntity, String> implements ILocationService {
    private ILocationRepository locationRepository;

    public LocationServiceImpl(ILocationRepository locationRepository) {
        super(locationRepository, LocationEntity.class);
        this.locationRepository = locationRepository;

    }


    @Override
    public LocationEntity getByCountryName(String country) {
        return this.locationRepository.getByCountryName(country);
    }
}
