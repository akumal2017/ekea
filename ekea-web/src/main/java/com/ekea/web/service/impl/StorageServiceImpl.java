package com.ekea.web.service.impl;


import com.ekea.core.service.impl.CrudServiceImpl;
import com.ekea.web.model.StorageEntity;
import com.ekea.web.repository.IStorageRepository;
import com.ekea.web.service.IStorageService;
import org.springframework.stereotype.Service;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Service
public class StorageServiceImpl extends CrudServiceImpl<StorageEntity, String> implements IStorageService {
    private IStorageRepository storageRepository;
    public StorageServiceImpl(IStorageRepository storageRepository) {
        super(storageRepository, StorageEntity.class);
        this.storageRepository = storageRepository;

    }


    @Override
    public StorageEntity findByProductId(String productId) {
        return this.storageRepository.findByProductId(productId);
    }

    @Override
    public StorageEntity findByLocationName(String country) {
        return storageRepository.findByLocationName(country);
    }
}
