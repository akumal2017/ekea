package com.ekea.web.service.impl;


import com.ekea.core.exception.EkeaException;
import com.ekea.core.model.EkeaTokenModel;
import com.ekea.core.service.impl.CrudServiceImpl;
import com.ekea.core.utils.EkeaTokenUtils;
import com.ekea.web.model.UserEntity;
import com.ekea.web.repository.IUserRepository;
import com.ekea.web.security.IEkeaPasswordEncoder;
import com.ekea.web.service.IUserService;
import org.springframework.stereotype.Service;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Service
public class UserServiceImpl extends CrudServiceImpl<UserEntity, String> implements IUserService {
    private IEkeaPasswordEncoder passwordEncoder;
    private IUserRepository userRepository;

    public UserServiceImpl(IUserRepository userRepository, IEkeaPasswordEncoder passwordEncoder) {
        super(userRepository, UserEntity.class);
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;

    }

    @Override
    public UserEntity save(UserEntity entity) {
        entity.setPassword(this.passwordEncoder.encrypt(entity.getPassword()));
        return super.save(entity);
    }

    @Override
    public UserEntity update(UserEntity entity) {
        EkeaTokenModel ekeaTokenModel = EkeaTokenUtils.getEkeaTokenModel();
        UserEntity existingUserEntity = this.userRepository.findOne(ekeaTokenModel.getUserId());
        existingUserEntity = this.map(entity, existingUserEntity);
        return super.update(existingUserEntity);
    }

    private UserEntity map(UserEntity sourceEntity, UserEntity destinationEntity) {
        destinationEntity.setName(sourceEntity.getName());
        destinationEntity.setAddress(sourceEntity.getAddress());
        destinationEntity.setCity(sourceEntity.getCity());
        destinationEntity.setZipCode(sourceEntity.getZipCode());
        destinationEntity.setCountry(sourceEntity.getCountry());
        destinationEntity.setMobile(sourceEntity.getMobile());
        destinationEntity.setPhone(sourceEntity.getPhone());
        return destinationEntity;
    }

    @Override
    public UserEntity authenticate(UserEntity userEntity) {
        UserEntity userToAuthenticate = this.userRepository.findByEmail(userEntity.getEmail());
        if (userToAuthenticate != null) {
            if (this.passwordEncoder.match(userEntity.getPassword(), userToAuthenticate.getPassword())) {
                return userToAuthenticate;
            }
        }
        return null;
    }

    @Override
    public Boolean changePassword(String oldPassword, String newPassword, String userId) {

        UserEntity umUserEntity = userRepository.findOne(userId);
        if (passwordEncoder.match(oldPassword, umUserEntity.getPassword())) {
            umUserEntity.setPassword(passwordEncoder.encrypt(newPassword));
            userRepository.update(umUserEntity);
            return true;
        }
        throw new EkeaException("Old Password Didn't match.");
    }

    @Override
    public UserEntity getUserByEmail(String email) {
        return this.userRepository.findByEmail(email);
    }
}
