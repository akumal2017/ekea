package com.ekea.web.service.impl;


import com.ekea.core.service.impl.CrudServiceImpl;
import com.ekea.web.model.OrderDetails;
import com.ekea.web.repository.IOrderDetailsRepository;
import com.ekea.web.service.IOrderDetailService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Service
public class OrderDetailServiceImpl extends CrudServiceImpl<OrderDetails, String> implements IOrderDetailService {
    private IOrderDetailsRepository orderDetailsRepository;

    public OrderDetailServiceImpl(IOrderDetailsRepository orderDetailRepository) {
        super(orderDetailRepository, OrderDetails.class);
        this.orderDetailsRepository = orderDetailRepository;

    }


    @Override
    public List<OrderDetails> getOrderDetailsByOrderId(String orderId) {
        return this.orderDetailsRepository.getOrderDetailsByOrderId(orderId);
    }


}
