package com.ekea.web.dto.request;


import com.ekea.core.model.ModelBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil Kumal on 12/02/2018.
 */
@Getter
@Setter
public class UserLoginRequestDto extends ModelBase {
    private String email;
    private String password;
}
