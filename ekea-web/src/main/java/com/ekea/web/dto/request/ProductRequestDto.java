package com.ekea.web.dto.request;

import com.ekea.core.model.EkeaRequestDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Getter
@Setter
public class ProductRequestDto extends EkeaRequestDtoBase {
    private String name;
    private String material;
    private String categoryEntityId;
    private String categoryEntityType;
    private String img1;
    private String img2;
    private String img3;
    private String img4;
    private Double price;
    private String description;
    private Integer quantity;

}
