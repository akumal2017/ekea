package com.ekea.web.dto.request;

import com.ekea.core.model.EkeaRequestDtoBase;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Getter
@Setter
public class PaymentRequestDto extends EkeaRequestDtoBase {

    private String orderEntityId;
    private Double amount;
    private String userEntityId;
    private String userEntityName;
    private String userEntityEmail;
    private Date orderDate;
    private String country;

}
