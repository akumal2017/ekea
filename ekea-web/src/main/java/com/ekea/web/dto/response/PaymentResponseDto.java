package com.ekea.web.dto.response;

import com.ekea.core.model.EkeaResponseDtoBase;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Getter
@Setter
public class PaymentResponseDto extends EkeaResponseDtoBase {

    private String orderEntityId;
    private Double amount;
    private String userEntityId;
    private String userEntityName;
    private String userEntityEmail;
    private Date orderDate;
    private String country;

}
