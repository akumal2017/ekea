package com.ekea.web.dto.response;

import com.ekea.core.model.EkeaResponseDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Getter
@Setter
public class OrderDetailResponseDto extends EkeaResponseDtoBase {

    private String productEntityId;
    private String productEntityName;
    private String orderEntityId;
    private String productEntityImg1;
    private Double productEntityPrice;
    private Integer quantity;
    private Double unitPrice;


}
