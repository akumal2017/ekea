package com.ekea.web.dto.response;

import com.ekea.core.model.EkeaResponseDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Getter
@Setter
public class LocationResponseDto extends EkeaResponseDtoBase {
    private String country;
    private String city;
    private String zipCode;

}
