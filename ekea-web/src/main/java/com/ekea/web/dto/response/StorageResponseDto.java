package com.ekea.web.dto.response;

import com.ekea.core.model.EkeaResponseDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Getter
@Setter
public class StorageResponseDto extends EkeaResponseDtoBase {
    private String locationEntityId;

    private String locationEntityCountry;

    private String locationEntityCity;

    private String locationEntityZipCode;

    private String productEntityId;
    private String productEntityName;

    private Integer quantity;

}
