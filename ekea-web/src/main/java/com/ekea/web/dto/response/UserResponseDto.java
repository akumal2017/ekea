package com.ekea.web.dto.response;

import com.ekea.core.model.EkeaResponseDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Getter
@Setter
public class UserResponseDto extends EkeaResponseDtoBase {
    private String name;
    private String address;
    private String country;
    private String zipCode;
    private String city;
    private String email;
    private String phone;
    private String mobile;
    private Boolean isAdmin;

}
