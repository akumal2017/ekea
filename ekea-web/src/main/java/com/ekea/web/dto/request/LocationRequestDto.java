package com.ekea.web.dto.request;

import com.ekea.core.model.EkeaRequestDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Getter
@Setter
public class LocationRequestDto extends EkeaRequestDtoBase {
    private String country;
    private String city;
    private String zipCode;

}
