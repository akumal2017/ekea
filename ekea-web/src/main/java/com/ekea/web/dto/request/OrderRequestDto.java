package com.ekea.web.dto.request;

import com.ekea.core.model.EkeaRequestDtoBase;
import com.ekea.web.domain.OrderDetailDomain;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Getter
@Setter
public class OrderRequestDto extends EkeaRequestDtoBase {

    private String userEntityId;
    private String userEntityName;
    private String userEntityEmail;
    private Date orderDate;
    private String country;
    List<OrderDetailDomain> orderDetailsList;
    private Double shippingCost;
    private Double paymentAmount;

}
