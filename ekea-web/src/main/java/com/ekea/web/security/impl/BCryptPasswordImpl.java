package com.ekea.web.security.impl;

import com.ekea.web.security.IEkeaPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Created by Anil on 5/14/18.
 */
@Component
public class BCryptPasswordImpl implements IEkeaPasswordEncoder {

    private PasswordEncoder passwordEncoder;

    public BCryptPasswordImpl(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public String encrypt(String password) {
        return passwordEncoder.encode(password);
    }


    @Override
    public Boolean match(String rawPassword, String encodedPassword) {
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }
}
