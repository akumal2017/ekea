package com.ekea.web.security.impl;


import com.ekea.core.exception.EkeaException;
import com.ekea.core.model.EkeaTokenModel;
import com.ekea.core.utils.EkeaDateUtils;
import com.ekea.web.model.UserEntity;
import com.ekea.web.security.IEkeaToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
@Component
public class EkeaTokenImpl implements IEkeaToken {

    public static final String SECRET = "EKEA-secret-code";

    /**
     * Generates a JWT token containing username as subject, and userId and other as additional claims. These properties are taken from the specified
     * User object
     *
     * @param umUserEntity the user for which the token will be generated
     * @return the JWT token
     */
    @Override
    public String generateToken(UserEntity umUserEntity) {
        Claims claims = Jwts.claims().setSubject(umUserEntity.getEmail());
        claims.put("userId", umUserEntity.getId());
        claims.put("email", umUserEntity.getEmail());
        claims.put("originCountry", umUserEntity.getCountry());

//        claims.put("companyId", umUserEntity.getUserCompanyId());
//        claims.put("branchId", umUserEntity.getUserBranchId());
//        try {
//            claims.put("roleName", umUserEntity.getUserRoleEntity().getUmRoleEntity().getName());
//            claims.put("roleId", umUserEntity.getUserRoleEntity().getUmRoleEntity().getId());
//            claims.put("roleLevel", umUserEntity.getUserRoleEntity().getUmRoleEntity().getLevel());
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new EkeaException("Unauthorized access, cause: " + e.getCause() + ", message: " + e.getMessage());
//        }
        String token = Jwts.builder().setClaims(claims)
                .setIssuedAt(EkeaDateUtils.localDateTimeIntoUtilDate(EkeaDateUtils.now()))
                .setExpiration(EkeaDateUtils.localDateTimeIntoUtilDate(EkeaDateUtils.addMinuteToDateTime(700)))
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();
        return token;
    }

    /**
     * Tries to parse specified String as a JWT token. If successful, returns EkeaTokenModel object
     * If unsuccessful (token is invalid or not containing all required user properties), simply returns null.
     *
     * @param token the JWT token to parse
     * @return the EkeaTokenModel object extracted from specified token or null if a token is invalid.
     */
    @Override
    public EkeaTokenModel parseToken(final String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token)
                .getBody();
        EkeaTokenModel tokenInfoModel = new EkeaTokenModel();
        tokenInfoModel.setEmail(claims.getSubject());
        try {
            tokenInfoModel.setUserId(claims.get("userId").toString());
            tokenInfoModel.setEmail(claims.get("email").toString());
            tokenInfoModel.setOriginCountry(claims.get("originCountry").toString());
//            tokenInfoModel.setBranchId(claims.get("branchId") != null ? Long.parseLong(claims.get("branchId").toString()) : null);
//            tokenInfoModel.setCompanyId(Long.parseLong(claims.get("companyId").toString()));
//            tokenInfoModel.setRoleName((String) claims.get("roleName"));
//            tokenInfoModel.setRoleId(Long.parseLong(claims.get("roleId").toString()));
//            tokenInfoModel.setRoleLevel(Integer.parseInt(claims.get("roleLevel").toString()));
        } catch (Exception e) {
            e.printStackTrace();
            throw new EkeaException("Unauthorized access, cause: " + e.getCause() + ", message: " + e.getMessage());
        }
        return tokenInfoModel;
    }
}
