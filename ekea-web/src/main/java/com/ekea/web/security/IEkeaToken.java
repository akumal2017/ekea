package com.ekea.web.security;


import com.ekea.core.model.EkeaTokenModel;
import com.ekea.web.model.UserEntity;

/**
 * Created by Anil Kumal on 01/12/2018.
 */
public interface IEkeaToken {
    /**
     * This method is used to generate token
     *
     * @param userEntity which contain system user information
     * @return token
     */
    String generateToken(UserEntity userEntity);

    /**
     * This method  is used to parse token
     *
     * @param token
     * @return TokenInfoModel object
     */
    EkeaTokenModel parseToken(final String token);
}
