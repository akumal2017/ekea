package com.ekea.web.repository;

import com.ekea.core.repository.ICrudRepository;
import com.ekea.web.model.CategoryEntity;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface ICategoryRepository extends ICrudRepository<CategoryEntity, String> {

}
