package com.ekea.web.repository.impl;

import com.ekea.core.repository.impl.CrudRepositoryImpl;
import com.ekea.web.model.PaymentEntity;
import com.ekea.web.repository.IPaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
@Repository
public class PaymentRepositoryImpl extends CrudRepositoryImpl<PaymentEntity, String> implements IPaymentRepository {
    private EntityManager entityManager;

    @Autowired
    public PaymentRepositoryImpl(EntityManager entityManager) {
        super(PaymentEntity.class, entityManager);
        this.entityManager = entityManager;
    }


}
