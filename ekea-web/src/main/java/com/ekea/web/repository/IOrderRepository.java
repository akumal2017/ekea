package com.ekea.web.repository;

import com.ekea.core.repository.ICrudRepository;
import com.ekea.web.model.OrderEntity;

import java.util.List;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface IOrderRepository extends ICrudRepository<OrderEntity, String> {
    List<OrderEntity> getListByUserId(String userId);

    List<OrderEntity> getOrdersByCountry(String country);

}
