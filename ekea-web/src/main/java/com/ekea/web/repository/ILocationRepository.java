package com.ekea.web.repository;

import com.ekea.core.repository.ICrudRepository;
import com.ekea.web.model.LocationEntity;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface ILocationRepository extends ICrudRepository<LocationEntity, String> {
    LocationEntity getByCountryName(String country);

}
