package com.ekea.web.repository.impl;

import com.ekea.core.repository.impl.CrudRepositoryImpl;
import com.ekea.web.model.ProductEntity;
import com.ekea.web.model.QProductEntity;
import com.ekea.web.repository.IProductRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
@Repository
public class ProductRepositoryImpl extends CrudRepositoryImpl<ProductEntity, String> implements IProductRepository {

    private EntityManager entityManager;

    @Autowired
    public ProductRepositoryImpl(EntityManager entityManager) {
        super(ProductEntity.class, entityManager);
        this.entityManager = entityManager;
    }


    @Override
    public List<ProductEntity> getHotProducts() {
        QProductEntity qProductEntity = QProductEntity.productEntity;
        System.out.println("entityManager............................ = " + entityManager);
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        List<ProductEntity> productEntityList = jpaQueryFactory
                .selectFrom(qProductEntity).orderBy(qProductEntity.id.desc()).limit(10).fetch();
        return productEntityList;
    }

    @Override
    public List<ProductEntity> getProductsByCategoryId(String categoryId) {
        QProductEntity qProductEntity = QProductEntity.productEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        List<ProductEntity> productEntityList = jpaQueryFactory
                .selectFrom(qProductEntity)
                .where(qProductEntity.categoryEntity.id.eq(categoryId))
                .orderBy(qProductEntity.id.desc())
                .fetch();
        return productEntityList;
    }

    @Override
    public List<ProductEntity> getProductsByCategoryId(String categoryId, Integer currentPage, Integer pageSize) {
        return entityManager.createQuery("SELECT t FROM " + tableName + " t where t.categoryEntity.id=:categoryId  ORDER BY t.id DESC")
                .setParameter("categoryId", categoryId)
                .setFirstResult(currentPage * pageSize)
                .setMaxResults(pageSize)
                .getResultList();
    }

    @Override
    public List<ProductEntity> getList(Integer currentPage, Integer pageSize) {
        return entityManager.createQuery("SELECT t FROM " + tableName + " t ORDER BY t.id DESC")
                .setFirstResult(currentPage * pageSize)
                .setMaxResults(pageSize)
                .getResultList();
        //return productEntityList;
    }

    @Override
    public Long getCountByCategoryId(String categoryId) {
        return (Long) entityManager.createQuery("SELECT COUNT(t) FROM " + tableName + " t where t.categoryEntity.id=:categoryId")
                .setParameter("categoryId", categoryId)
                .getSingleResult();
    }
}
