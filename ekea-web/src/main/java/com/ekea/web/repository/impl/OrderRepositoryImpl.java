package com.ekea.web.repository.impl;

import com.ekea.core.repository.impl.CrudRepositoryImpl;
import com.ekea.web.model.OrderEntity;
import com.ekea.web.model.QOrderEntity;
import com.ekea.web.repository.IOrderRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
@Repository
public class OrderRepositoryImpl extends CrudRepositoryImpl<OrderEntity, String> implements IOrderRepository {
    private EntityManager entityManager;

    @Autowired
    public OrderRepositoryImpl(EntityManager entityManager) {
        super(OrderEntity.class, entityManager);
        this.entityManager = entityManager;
    }


    @Override
    public List<OrderEntity> getListByUserId(String userId) {
        QOrderEntity qOrderEntity = QOrderEntity.orderEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        List<OrderEntity> orderEntityList = jpaQueryFactory
                .selectFrom(qOrderEntity)
                .where(qOrderEntity.userEntity.id.eq(userId))
                .orderBy(qOrderEntity.orderDate.desc())
                .fetch();
        return orderEntityList;
    }

    @Override
    public List<OrderEntity> getOrdersByCountry(String country) {
        QOrderEntity qOrderEntity = QOrderEntity.orderEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        List<OrderEntity> orderEntityList = jpaQueryFactory
                .selectFrom(qOrderEntity)
                .where(qOrderEntity.country.toLowerCase().eq(country.toLowerCase()))
                .orderBy(qOrderEntity.orderDate.desc())
                .fetch();
        return orderEntityList;
    }

    @Override
    public List<OrderEntity> findAll() {
        QOrderEntity qOrderEntity = QOrderEntity.orderEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        List<OrderEntity> orderEntityList = jpaQueryFactory
                .selectFrom(qOrderEntity)
                .orderBy(qOrderEntity.orderDate.desc())
                .fetch();
        return orderEntityList;
    }
}
