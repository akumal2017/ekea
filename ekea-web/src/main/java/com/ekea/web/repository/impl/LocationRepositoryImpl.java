package com.ekea.web.repository.impl;

import com.ekea.core.repository.impl.CrudRepositoryImpl;
import com.ekea.web.model.LocationEntity;
import com.ekea.web.model.QLocationEntity;
import com.ekea.web.repository.ILocationRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
@Repository
public class LocationRepositoryImpl extends CrudRepositoryImpl<LocationEntity, String> implements ILocationRepository {
    private EntityManager entityManager;

    @Autowired
    public LocationRepositoryImpl(EntityManager entityManager) {
        super(LocationEntity.class, entityManager);
        this.entityManager = entityManager;
    }


    @Override
    public LocationEntity getByCountryName(String country) {
        QLocationEntity qLocationEntity = QLocationEntity.locationEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        LocationEntity locationEntity = jpaQueryFactory
                .selectFrom(qLocationEntity)
                .where(qLocationEntity.country.eq(country))
                .fetchOne();
        return locationEntity;
    }

}
