package com.ekea.web.repository.impl;

import com.ekea.core.repository.impl.CrudRepositoryImpl;
import com.ekea.web.model.OrderDetails;
import com.ekea.web.model.QOrderDetails;
import com.ekea.web.repository.IOrderDetailsRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
@Repository
public class OrderDetailsRepositoryImpl extends CrudRepositoryImpl<OrderDetails, String> implements IOrderDetailsRepository {
    private EntityManager entityManager;

    public OrderDetailsRepositoryImpl(EntityManager entityManager) {
        super(OrderDetails.class, entityManager);
        this.entityManager = entityManager;
    }


    @Override
    public List<OrderDetails> getOrderDetailsByOrderId(String orderId) {
        QOrderDetails qOrderDetails = QOrderDetails.orderDetails;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        List<OrderDetails> orderDetailsEntityList = jpaQueryFactory
                .selectFrom(qOrderDetails)
                .where(qOrderDetails.orderEntity.id.eq(orderId))
                .fetch();
        return orderDetailsEntityList;
    }
}
