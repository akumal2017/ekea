package com.ekea.web.repository.impl;

import com.ekea.core.repository.impl.CrudRepositoryImpl;
import com.ekea.web.model.QStorageEntity;
import com.ekea.web.model.StorageEntity;
import com.ekea.web.repository.IStorageRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
@Repository
public class StorageRepositoryImpl extends CrudRepositoryImpl<StorageEntity, String> implements IStorageRepository {
    private EntityManager entityManager;

    @Autowired
    public StorageRepositoryImpl(EntityManager entityManager) {
        super(StorageEntity.class, entityManager);
        this.entityManager = entityManager;
    }


    @Override
    public StorageEntity findByProductId(String productId) {
        QStorageEntity qStorageEntity = QStorageEntity.storageEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        StorageEntity storageEntity = jpaQueryFactory
                .selectFrom(qStorageEntity)
                .where(qStorageEntity.productEntity.id.eq(productId))
                .fetchOne();
        return storageEntity;
    }

    @Override
    public StorageEntity findByLocationName(String country) {
        QStorageEntity qStorageEntity = QStorageEntity.storageEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        StorageEntity storageEntity = jpaQueryFactory
                .selectFrom(qStorageEntity)
                .where(qStorageEntity.locationEntity.country.eq(country))
                .fetchOne();
        return storageEntity;
    }
}
