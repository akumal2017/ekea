package com.ekea.web.repository;

import com.ekea.core.repository.ICrudRepository;
import com.ekea.web.model.OrderDetails;

import java.util.List;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface IOrderDetailsRepository extends ICrudRepository<OrderDetails, String> {
    List<OrderDetails> getOrderDetailsByOrderId(String orderId);


}
