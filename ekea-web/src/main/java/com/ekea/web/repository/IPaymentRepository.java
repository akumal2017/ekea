package com.ekea.web.repository;

import com.ekea.core.repository.ICrudRepository;
import com.ekea.web.model.PaymentEntity;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface IPaymentRepository extends ICrudRepository<PaymentEntity, String> {

}
