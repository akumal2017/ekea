package com.ekea.web.repository;

import com.ekea.core.repository.ICrudRepository;
import com.ekea.web.model.UserEntity;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface IUserRepository extends ICrudRepository<UserEntity, String> {
    UserEntity findByEmail(String email);

}
