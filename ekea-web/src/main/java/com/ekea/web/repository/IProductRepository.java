package com.ekea.web.repository;

import com.ekea.core.repository.ICrudRepository;
import com.ekea.web.model.ProductEntity;

import java.util.List;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface IProductRepository extends ICrudRepository<ProductEntity, String> {
    List<ProductEntity> getHotProducts();

    List<ProductEntity> getProductsByCategoryId(String categoryId);

    List<ProductEntity> getProductsByCategoryId(String categoryId, Integer currentPage, Integer pageSize);

    List<ProductEntity> getList(Integer currentPage, Integer pageSize);

    Long getCountByCategoryId(String categoryId);

}
