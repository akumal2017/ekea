package com.ekea.web.repository.impl;

import com.ekea.core.repository.impl.CrudRepositoryImpl;
import com.ekea.web.model.CategoryEntity;
import com.ekea.web.repository.ICategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
@Repository
public class CategoryRepositoryImpl extends CrudRepositoryImpl<CategoryEntity, String> implements ICategoryRepository {
    private EntityManager entityManager;

    @Autowired
    public CategoryRepositoryImpl(EntityManager entityManager) {
        super(CategoryEntity.class, entityManager);
        this.entityManager = entityManager;
    }


}
