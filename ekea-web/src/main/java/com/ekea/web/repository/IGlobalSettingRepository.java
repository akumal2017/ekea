package com.ekea.web.repository;

import com.ekea.core.repository.ICrudRepository;
import com.ekea.web.model.GlobalSettingEntity;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface IGlobalSettingRepository extends ICrudRepository<GlobalSettingEntity, String> {

}
