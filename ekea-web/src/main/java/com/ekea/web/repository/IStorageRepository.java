package com.ekea.web.repository;

import com.ekea.core.repository.ICrudRepository;
import com.ekea.web.model.StorageEntity;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface IStorageRepository extends ICrudRepository<StorageEntity, String> {
    StorageEntity findByProductId(String productId);

    StorageEntity findByLocationName(String country);
}
