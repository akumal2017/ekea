package com.ekea.web.repository.impl;

import com.ekea.core.repository.impl.CrudRepositoryImpl;
import com.ekea.web.model.GlobalSettingEntity;
import com.ekea.web.repository.IGlobalSettingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
@Repository
public class GlobalSettingRepositoryImpl extends CrudRepositoryImpl<GlobalSettingEntity, String> implements IGlobalSettingRepository {
    private EntityManager entityManager;

    @Autowired
    public GlobalSettingRepositoryImpl(EntityManager entityManager) {
        super(GlobalSettingEntity.class, entityManager);
        this.entityManager = entityManager;
    }


}
