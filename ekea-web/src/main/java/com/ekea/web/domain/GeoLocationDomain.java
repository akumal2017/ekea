package com.ekea.web.domain;

import com.ekea.core.model.ModelBase;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class GeoLocationDomain extends ModelBase {
    private String ip;
    private String country;
    private String lat;
    private String lon;
}
