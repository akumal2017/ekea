package com.ekea.web.domain;

import com.ekea.core.model.ModelBase;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDetailDomain extends ModelBase {
    private String productEntityId;
    private Integer quantity;
}
