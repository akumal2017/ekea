package com.ekea.web.model;

import com.ekea.core.model.EkeaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
/**
 * Created By Anil Kumal 20/11/2018
 */
@Table(name = "payment")
public class PaymentEntity extends EkeaEntityBase {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id")
    private OrderEntity orderEntity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @Column(name = "amount")
    private Double amount;

}
