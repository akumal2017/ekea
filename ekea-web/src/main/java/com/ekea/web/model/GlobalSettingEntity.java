package com.ekea.web.model;


import com.ekea.core.model.EkeaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created By Anil Kumal 20/11/2018
 */
@Getter
@Setter
@Entity
@Table(name = "global_setting")
public class GlobalSettingEntity extends EkeaEntityBase {
    @Column(name = "name")
    private String name;
    @Column(name = "setting_value")
    private String settingValue;

}
