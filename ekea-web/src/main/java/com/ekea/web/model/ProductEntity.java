package com.ekea.web.model;

import com.ekea.core.model.EkeaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
/**
 * Created By Anil Kumal 20/11/2018
 */
@Table(name = "product")
public class ProductEntity extends EkeaEntityBase {
    @Column(name = "name")
    private String name;

    @Column(name = "material")
    private String material;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id")
    private CategoryEntity categoryEntity;

    @Column(name = "img1")
    private String img1;

    @Column(name = "img2")
    private String img2;

    @Column(name = "img3")
    private String img3;

    @Column(name = "img4")
    private String img4;

    @Column(name = "price")
    private Double price;

    @Column(name = "description")
    private String description;


}
