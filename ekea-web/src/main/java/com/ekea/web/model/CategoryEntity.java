package com.ekea.web.model;

import com.ekea.core.model.EkeaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
/**
 * Created By Anil Kumal 20/11/2018
 */
@Table(name = "category")
public class CategoryEntity extends EkeaEntityBase {
    @Column(name = "type")
    private String type;

    @Column(name = "description")
    private String description;

}
