package com.ekea.web.model;

import com.ekea.core.model.EkeaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created By Anil Kumal 20/11/2018
 */
@Getter
@Setter
@Entity
@Table(name = "user")
public class UserEntity extends EkeaEntityBase {
    @Column(name = "name")
    private String name;

    @Column(name = "password")
    private String password;

    @Column(name = "address")
    private String address;

    @Column(name = "country")
    private String country;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "city")
    private String city;


    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "admin")
    private Boolean isAdmin = false;


}
