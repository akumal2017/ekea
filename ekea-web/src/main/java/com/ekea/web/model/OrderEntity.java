package com.ekea.web.model;

import com.ekea.core.model.EkeaEntityBase;
import com.ekea.web.domain.OrderDetailDomain;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
/**
 * Created By Anil Kumal 20/11/2018
 */
@Table(name = "product_order")
public class OrderEntity extends EkeaEntityBase {
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_Id")
    private UserEntity userEntity;

    @Column(name = "order_date")
    private Date orderDate = new Date();

    @Column(name = "country")
    private String country;

    @Column(name = "order_status")
    private String orderStatus;

    @Column(name = "shipping_cost")
    private Double shippingCost;

    @Transient
    private List<OrderDetailDomain> orderDetailsList;

    @Transient
    private PaymentEntity paymentEntity;

}
