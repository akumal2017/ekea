package com.ekea.core.dto.accessControl.responseDto;

import com.ekea.core.model.EkeaResponseDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by admin on 5/13/2018.
 */
@Getter
@Setter
public class UmRoleResponseDto extends EkeaResponseDtoBase {
    Integer level;
    String description;
}
