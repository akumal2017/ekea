package com.ekea.core.dto.accessControl.requestDto;


import com.ekea.core.model.EkeaRequestDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil Kumal on 11/29/2018.
 */
@Getter
@Setter
public class UmRoleRequestDto extends EkeaRequestDtoBase {
    Integer level;
    String description;
}
