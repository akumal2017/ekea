package com.ekea.core.dto.accessControl.requestDto;

import com.ekea.core.model.ModelBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by admin on 5/14/2018.
 */
@Getter
@Setter
public class UmChangePasswordRequestDto extends ModelBase {
    private String oldPassword;
    private String newPassword;
    private String confirmPassword;
}
