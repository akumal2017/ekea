package com.ekea.core.repository;


import com.ekea.core.model.SequenceEntity;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
public interface ISequenceRepository extends ICrudRepository<SequenceEntity, String> {
    SequenceEntity getByName(String name);
}
