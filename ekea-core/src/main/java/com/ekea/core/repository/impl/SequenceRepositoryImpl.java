package com.ekea.core.repository.impl;


import com.ekea.core.model.QSequenceEntity;
import com.ekea.core.model.SequenceEntity;
import com.ekea.core.repository.ISequenceRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/**
 * Created by Anil Kumal on 30/11/2018.
 */
@Repository
public class SequenceRepositoryImpl extends CrudRepositoryImpl<SequenceEntity, String> implements ISequenceRepository {
    private EntityManager entityManager;

    public SequenceRepositoryImpl(EntityManager entityManager) {
        super(SequenceEntity.class, entityManager);
        this.entityManager = entityManager;
    }


    @Override
    public SequenceEntity getByName(String name) {
        QSequenceEntity qsequenceEntity = QSequenceEntity.sequenceEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        SequenceEntity sequenceEntity = jpaQueryFactory
                .selectFrom(qsequenceEntity)
                .where(qsequenceEntity.sequenceName.toLowerCase().eq(name.toLowerCase()))
                .fetchOne();
        return sequenceEntity;
    }
}
