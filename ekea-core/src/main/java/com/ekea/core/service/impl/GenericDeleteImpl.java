package com.ekea.core.service.impl;


import com.ekea.core.model.ReferencedTableModel;
import com.ekea.core.service.IGenericDelete;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anil Kumal on 11/29/2018.
 */
@Component
public class GenericDeleteImpl implements IGenericDelete<Long> {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public boolean isDependencyExist(String tableName, Long id) {
        /*TODO Fix Me: By getting db name dynamically */
        String dbName = "EKEA";
        List<ReferencedTableModel> referencedTableModelList = fetchReferencedTables(dbName, tableName);
        for (ReferencedTableModel referenceTable : referencedTableModelList) {
            if (count(referenceTable.getTableName(), referenceTable.getColumnName(), id) > 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<ReferencedTableModel> fetchReferencedTables(String dbName, String tableName) {

        /**
         * Query Example:
         * SELECT
         * DISTINCT(TABLE_NAME),COLUMN_NAME
         * FROM
         * INFORMATION_SCHEMA.KEY_COLUMN_USAGE
         * WHERE
         * REFERENCED_TABLE_SCHEMA = 'nova_module' AND
         * REFERENCED_TABLE_NAME = 'system_setup_role';
         */
        List<Object[]> resultList = entityManager.createNativeQuery("SELECT DISTINCT(TABLE_NAME),COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE  where REFERENCED_TABLE_SCHEMA=:dbName AND REFERENCED_TABLE_NAME=:tableName")
                .setParameter("dbName", dbName)
                .setParameter("tableName", tableName)
                .getResultList();
        return rowMapper(resultList);

    }

    @Override
    public boolean deleteChildRecords(List<ReferencedTableModel> referencedTableModelList, Long aLong) {
        return false;
    }

    @Override
    public long count(String tableName, String columnName, Long value) {
        Number count = (Number) entityManager.createNativeQuery("SELECT COUNT(*) FROM " + tableName + " where " + columnName + "=:columnValue")
                .setParameter("columnValue", value)
                .getSingleResult();
        return count.longValue();
    }

    /**
     * Maps each tuple to {@link ReferencedTableModel}
     *
     * @param result
     * @return new object of ReferencedTableModel
     */
    private ReferencedTableModel rowMapper(Object[] result) {
        ReferencedTableModel referencedTableModel = new ReferencedTableModel();
        referencedTableModel.setTableName(result[0].toString());
        referencedTableModel.setColumnName(result[1].toString());
        return referencedTableModel;
    }

    /**
     * Maps result set to list of {@link ReferencedTableModel}
     *
     * @param resultList - result set of query
     * @return - list of {@link ReferencedTableModel}
     */
    private List<ReferencedTableModel> rowMapper(List<Object[]> resultList) {
        List<ReferencedTableModel> referencedTableModels = new ArrayList<>();
        for (Object[] result : resultList) {
            referencedTableModels.add(rowMapper(result));
        }
        return referencedTableModels;
    }
}
