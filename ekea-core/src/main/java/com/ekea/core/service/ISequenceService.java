package com.ekea.core.service;


import com.ekea.core.model.SequenceEntity;

/**
 * Created by Admin on 5/23/2018.
 */
public interface ISequenceService extends ICrudService<SequenceEntity, String> {
    SequenceEntity getByName(String name);
}
