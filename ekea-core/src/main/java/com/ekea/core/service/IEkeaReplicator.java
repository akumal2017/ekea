package com.ekea.core.service;

public interface IEkeaReplicator<T> {
    void replicateInsert(T t);

    void replicateUpdate(T t);

    void replicateDelete(T t);
}








