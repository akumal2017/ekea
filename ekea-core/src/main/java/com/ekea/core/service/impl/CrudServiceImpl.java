package com.ekea.core.service.impl;

import com.ekea.core.model.EkeaEntityBase;
import com.ekea.core.model.SequenceEntity;
import com.ekea.core.repository.ICrudRepository;
import com.ekea.core.service.ICrudService;
import com.ekea.core.service.ISequenceService;
import com.ekea.core.utils.EkeaGlobalSettingUtils;
import com.ekea.core.utils.EkeaObjectMapper;
import com.ekea.core.utils.EkeaStringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Anil Kumal on 11/29/2018.
 */
public class CrudServiceImpl<T, ID extends Serializable> implements ICrudService<T, ID> {

    protected ICrudRepository<T, ID> crudRepository;
    protected Class<T> classType;
    protected String sequenceKey;

    @Autowired
    private ISequenceService sequenceService;

    public CrudServiceImpl(ICrudRepository<T, ID> crudRepository, Class<T> classType) {
        this.crudRepository = crudRepository;
        this.classType = classType;
        this.sequenceKey = this.classType.getSimpleName();
    }


    @Override
    public <S extends T> S save(S entity) {
        SequenceEntity sequenceEntity = getSequenceEntity();
        Long codeSequence = sequenceEntity.getSequenceValue() + 1;
        if (isInstanceOfBaseEntity(entity)) {

            ((EkeaEntityBase) entity).setId(getNewCode(sequenceEntity, codeSequence));
        }
        crudRepository.save(entity);
        sequenceEntity.setSequenceValue(codeSequence);
        sequenceService.update(sequenceEntity);
        return entity;
    }

    @Override
    public <S extends T> S update(S entity) {
        return crudRepository.update(entity);
    }

    @Override
    public T findOne(ID id) {
        return crudRepository.findOne(id);
    }

    @Override
    public boolean exists(ID id) {
        return crudRepository.exists(id);
    }

    @Override
    public List<T> findAll() {
        return crudRepository.findAll();
    }


    @Override
    public List<T> findAll(ID id) {
        return crudRepository.findAll(id);
    }

    @Override
    public Long count() {
        return crudRepository.count();
    }


    @Override
    public boolean delete(ID id) {
        return crudRepository.delete(id);
    }


    @Override
    public boolean delete(T entity) {
        return crudRepository.delete(entity);
    }

    private Long getId(T entity) {
//        if (entity instanceof FortunaEntityBase) {
//            return ((EkeaEntityBase) entity).getId();
//        }
        return null;
    }

    private boolean isInstanceOfBaseEntity(T entity) {
        return (entity instanceof EkeaEntityBase);
    }


    @Override
    public Long count(String name) {
        return crudRepository.count(name);
    }

    protected String getNewCode(SequenceEntity sequenceEntity, Long codeSequence) {

        String preffix = EkeaStringUtils.isNotNull(sequenceEntity.getPrefix()) ? sequenceEntity.getPrefix() : "";
        String suffix = EkeaStringUtils.isNotNull(sequenceEntity.getSuffix()) ? sequenceEntity.getSuffix() : "";
        String newCode = preffix + codeSequence + suffix;
        return newCode.trim();
    }

    protected SequenceEntity getSequenceEntity() {
        SequenceEntity sequenceEntity = sequenceService.getByName(this.sequenceKey);
        return sequenceEntity;
    }


    @Override
    public void replicateInsert(T t) {
        String replicationObjs = EkeaGlobalSettingUtils.getGlobalSettingByKey(EkeaGlobalSettingUtils.REPLICATION_OBJECT_LIST);
        Map<String, String> replicationObjList = EkeaObjectMapper.mapStringToObject(replicationObjs);
        String currentTennant = EkeaGlobalSettingUtils.getGlobalSettingByKey(EkeaGlobalSettingUtils.COUNTRY);
        String nameOfReplicationObj = replicationObjList.get(this.classType.getSimpleName());
        if (nameOfReplicationObj.equalsIgnoreCase(this.classType.getSimpleName())) {
            for (String datasource : EkeaGlobalSettingUtils.getDataSourceName()) {
                if (!currentTennant.equalsIgnoreCase(datasource)) {
                    //switch to next tenant (Datasource), so operation can happen in next database
                    EkeaGlobalSettingUtils.put(EkeaGlobalSettingUtils.COUNTRY, datasource);
                    this.save(t);
                }
            }
        }

    }

    @Override
    public void replicateUpdate(T t) {
        String replicationObjs = EkeaGlobalSettingUtils.getGlobalSettingByKey(EkeaGlobalSettingUtils.REPLICATION_OBJECT_LIST);
        Map<String, String> replicationObjList = EkeaObjectMapper.mapStringToObject(replicationObjs);
        String currentTennant = EkeaGlobalSettingUtils.getGlobalSettingByKey(EkeaGlobalSettingUtils.COUNTRY);
        String nameOfReplicationObj = replicationObjList.get(this.classType.getSimpleName());
        if (nameOfReplicationObj.equalsIgnoreCase(this.classType.getSimpleName())) {
            for (String datasource : EkeaGlobalSettingUtils.getDataSourceName()) {
                if (!currentTennant.equalsIgnoreCase(datasource)) {
                    //switch to next tenant (Datasource), so operation can happen in next database
                    EkeaGlobalSettingUtils.put(EkeaGlobalSettingUtils.COUNTRY, datasource);
                    this.update(t);
                }
            }
        }
    }

    @Override
    public void replicateDelete(T t) {
        String replicationObjs = EkeaGlobalSettingUtils.getGlobalSettingByKey(EkeaGlobalSettingUtils.REPLICATION_OBJECT_LIST);
        Map<String, String> replicationObjList = EkeaObjectMapper.mapStringToObject(replicationObjs);
        String currentTennant = EkeaGlobalSettingUtils.getGlobalSettingByKey(EkeaGlobalSettingUtils.COUNTRY);
        String nameOfReplicationObj = replicationObjList.get(this.classType.getSimpleName());
        if (nameOfReplicationObj.equalsIgnoreCase(this.classType.getSimpleName())) {
            for (String datasource : EkeaGlobalSettingUtils.getDataSourceName()) {
                if (!currentTennant.equalsIgnoreCase(datasource)) {
                    //switch to next tenant (Datasource), so operation can happen in next database
                    EkeaGlobalSettingUtils.put(EkeaGlobalSettingUtils.COUNTRY, datasource);
                    this.delete(t);
                }
            }
        }
    }

    @Override
    public List<T> combineFromFragments() {
        String fragmentObjects = EkeaGlobalSettingUtils.getGlobalSettingByKey(EkeaGlobalSettingUtils.FRAGMENT_OBJECT_LIST);
        Map<String, String> fragmentObjMap = EkeaObjectMapper.mapStringToObject(fragmentObjects);
        String nameOfFragmentObj = fragmentObjMap.get(this.classType.getSimpleName());
        List<T> combinedList = new ArrayList<>();
        if (nameOfFragmentObj.equalsIgnoreCase(this.classType.getSimpleName())) {
            for (String datasource : EkeaGlobalSettingUtils.getDataSourceName()) {
                //switch to next tenant (Datasource), so that data will be fetched from different database
                EkeaGlobalSettingUtils.put(EkeaGlobalSettingUtils.COUNTRY, datasource);
                combinedList.addAll(this.findAll());
            }
        }
        return combinedList;
    }
}
