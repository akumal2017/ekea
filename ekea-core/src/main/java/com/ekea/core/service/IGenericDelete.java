package com.ekea.core.service;



import com.ekea.core.model.ReferencedTableModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Anil Kumal on 11/29/2018.
 */
public interface IGenericDelete<ID extends Serializable> {
    /**
     * checks if dependency exists in child tables on delete process
     *
     * @param tableName
     * @param id
     * @return
     */
    boolean isDependencyExist(String tableName, ID id);

    /**
     * fetches reference table list according to given table name
     *
     * @param dbName    - name of database where table exists
     * @param tableName - name of table
     * @return list of reference tables
     */
    List<ReferencedTableModel> fetchReferencedTables(String dbName, String tableName);

    /**
     * Delete all child records if parent record is deleted
     *
     * @param referencedTableModelList
     * @param id
     * @return true on successful delete, else false.
     */
    boolean deleteChildRecords(List<ReferencedTableModel> referencedTableModelList, ID id);

    /**
     * counts number of existing record in child table according to parent id.
     *
     * @param tableName  - child table name
     * @param columnName - reference column name
     * @param value      - value of parent id
     * @return number of existing records
     */
    long count(String tableName, String columnName, ID value);


}
