package com.ekea.core.controller;


import com.ekea.core.constant.WebResourceConstant;
import com.ekea.core.exception.EkeaException;
import com.ekea.core.model.EkeaResponseObj;
import com.ekea.core.service.ICrudService;
import com.ekea.core.utils.IEkeaBeanMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Anil Kumal on 11/29/2018.
 */

public abstract class EkeaControllerBase<Entity, Dto> {

    protected ICrudService iCrudService;
    protected IEkeaBeanMapper<Entity, Dto> ekeaReqBeanMapper;
    protected IEkeaBeanMapper<Entity, Dto> ekeaResBeanMapper;


    public EkeaControllerBase(ICrudService iCrudService, IEkeaBeanMapper ekeaReqBeanMapper, IEkeaBeanMapper ekeaResBeanMapper) {
        this.iCrudService = iCrudService;
        this.ekeaReqBeanMapper = ekeaReqBeanMapper;
        this.ekeaResBeanMapper = ekeaResBeanMapper;
    }

    @PostMapping(WebResourceConstant.CREATE)
    public ResponseEntity<EkeaResponseObj> create(@RequestBody @Valid Dto dto) {
        Entity entity = ekeaReqBeanMapper.mapToEntity(dto);
        iCrudService.save(entity);
        this.iCrudService.replicateInsert(entity);
        // setCreateEntityProperties(entity);
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().message("Record has been created.").build(), HttpStatus.OK);
    }

    @PutMapping(WebResourceConstant.UPDATE)
    public ResponseEntity<EkeaResponseObj> update(@RequestBody @Valid Dto dto) {
        Entity entity = ekeaReqBeanMapper.mapToEntity(dto);
        //  setUpdateEntityProperties(entity);
        iCrudService.update(entity);
        this.iCrudService.replicateUpdate(entity);
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().message("Record has been updated.").build(), HttpStatus.OK);
    }

    @DeleteMapping(WebResourceConstant.DELETE)
    public ResponseEntity<EkeaResponseObj> delete(@PathVariable String id) {
        Entity entity = (Entity) this.iCrudService.findOne(id);
        iCrudService.delete(id);
        this.iCrudService.replicateDelete(entity);
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().result(id).message("Record with id: " + id + " deleted.").build(), HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.GET)
    public ResponseEntity<EkeaResponseObj> get(@PathVariable String id) {
        Entity entity = (Entity) iCrudService.findOne(id);
        if (entity == null) {
            throw new EkeaException("Sorry!! No Records Found");
        }
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().result(ekeaResBeanMapper.mapToDTO(entity)).message("Success").build(), HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.GET_ALL)
    public ResponseEntity<EkeaResponseObj> getAll() {
        List<Entity> entities = iCrudService.findAll();
        entities.addAll(this.iCrudService.combineFromFragments());
        if (entities.size() == 0) {
            throw new EkeaException("Sorry!! No Records Found");
        }
        return new ResponseEntity<>(new EkeaResponseObj.EkeaResponseObjBuilder().result(ekeaResBeanMapper.mapToDTO(entities)).message("Success").build(), HttpStatus.OK);
    }

    public ResponseEntity<EkeaResponseObj> getAll(Integer currentPage, Integer pageSize) {
        return null;
    }


}
