package com.ekea.core.controller;

import com.ekea.core.constant.WebResourceConstant;
import com.ekea.core.model.EkeaResponseObj;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anil Kumal on 11/29/2018.
 */
public abstract class EkeaRestTemplateControllerBase<Dto> {
    protected String serviceURI;
    private RestTemplate restTemplate;

    public EkeaRestTemplateControllerBase(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @PostMapping(WebResourceConstant.CREATE)
    public ResponseEntity<EkeaResponseObj> create(@RequestBody @Valid Dto dto) {
//        return restTemplate.postForEntity(this.getServiceURI() +WebResourceConstant.CREATE, dto, EkeaResponseObj.class);
        HttpEntity<Dto> requestHttpEntity = new HttpEntity<>(dto);
        ResponseEntity<EkeaResponseObj> ekeaResponse = restTemplate.exchange(this.getServiceURI() + WebResourceConstant.CREATE, HttpMethod.POST, requestHttpEntity, EkeaResponseObj.class);
        return ekeaResponse;
    }

    @PutMapping(WebResourceConstant.UPDATE)
    public ResponseEntity<EkeaResponseObj> update(@RequestBody @Valid Dto dto) {
//       return restTemplate.postForEntity(this.getServiceURI()+WebResourceConstant.UPDATE,dto,EkeaResponseObj.class);
        HttpEntity<Dto> requestHttpEntity = new HttpEntity<>(dto);
        ResponseEntity<EkeaResponseObj> ekeaResponse = restTemplate.exchange(this.getServiceURI() + WebResourceConstant.UPDATE, HttpMethod.PUT, requestHttpEntity, EkeaResponseObj.class);
        return ekeaResponse;
    }

    @DeleteMapping(WebResourceConstant.DELETE)
    public ResponseEntity<EkeaResponseObj> delete(@PathVariable Long id) {
        Map<String, Long> pathVariableMap = new HashMap<>();
        pathVariableMap.put("id", id);
        ResponseEntity<EkeaResponseObj> ekeaResponse = restTemplate.exchange(this.getServiceURI() + WebResourceConstant.DELETE, HttpMethod.DELETE, null, EkeaResponseObj.class, pathVariableMap);
        return ekeaResponse;
    }

    @GetMapping(WebResourceConstant.GET)
    public ResponseEntity<EkeaResponseObj> get(@PathVariable Long id) {
        EkeaResponseObj ekeaResponse = restTemplate.getForObject(this.getServiceURI() + WebResourceConstant.GET, EkeaResponseObj.class, id);
        return new ResponseEntity<>(ekeaResponse, HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.GET_ALL)
    public ResponseEntity<EkeaResponseObj> getAll() {
        EkeaResponseObj ekeaResponse = restTemplate.getForObject(this.getServiceURI() + WebResourceConstant.GET_ALL, EkeaResponseObj.class);
        return new ResponseEntity<>(ekeaResponse, HttpStatus.OK);
    }

    protected abstract String getServiceURI();
}
