package com.ekea.core.utils;

import java.util.List;

/**
 * Created by Anil Kumal on 11/29/2018.
 */
public interface IEkeaBeanMapper<Entity, DTO> {
    Entity mapToEntity(DTO viewModel);

    DTO mapToDTO(Entity entity);

    List<Entity> mapToEntity(List<DTO> dtoList);

    List<DTO> mapToDTO(List<Entity> entityList);
}
