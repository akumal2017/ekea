package com.ekea.core.utils;



import com.ekea.core.model.EkeaTokenModel;
import org.springframework.stereotype.Component;

/**
 * Created by Anil Kumal on 11/29/2018.
 */
@Component
public class EkeaTokenUtils {

    private static EkeaTokenModel ekeaTokenModel;

    public static EkeaTokenModel getEkeaTokenModel() {
        return ekeaTokenModel;
    }

    public static void setEkeaTokenModel(final EkeaTokenModel ekeaTokenModel) {
        EkeaTokenUtils.ekeaTokenModel = ekeaTokenModel;
    }
}
