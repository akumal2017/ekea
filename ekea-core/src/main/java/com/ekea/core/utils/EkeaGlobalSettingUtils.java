package com.ekea.core.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Anil Kumal on 11/29/2018.
 */
public class EkeaGlobalSettingUtils {

    public static final String MAX_LOGIN_ATTEMPT = "MAX_LOGIN_ATTEMPT";
    public static final String IMAGE_UPLOAD_LOCATION = "IMAGE_UPLOAD_LOCATION";
    public static final String ROOT_UPLOAD_LOCATION = "ROOT_UPLOAD_LOCATION";
    public static final String REPLICATION_OBJECT_LIST = "REPLICATION_OBJECT_LIST";
    public static final String FRAGMENT_OBJECT_LIST = "FRAGMENT_OBJECT_LIST";
    public static final String IP = "IP";
    public static final String COUNTRY = "COUNTRY";
    public static final String LAT = "LAT";
    public static final String LON = "LON";

    private static Map<String, String> globalSettingMap = new HashMap<>();

    public static void setGlobalSettingMap(Map<String, String> systemResource) {
        globalSettingMap = systemResource;
    }

    public static String getGlobalSettingByKey(String key) {
        return globalSettingMap.get(key);
    }

    public static void put(String key, String value) {
        globalSettingMap.put(key, value);
    }

    public static List<String> getDataSourceName() {
        List<String> countryList = new ArrayList<>();
        countryList.add("FINLAND");
        countryList.add("SWEDEN");
        countryList.add("RUSSIA");
        return countryList;
    }
}
