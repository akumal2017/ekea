package com.ekea.core.constant;

/**
 * Created by Anil Kumal on 11/28/2018.
 */
public class WebResourceConstant {


    /**
     * Common api end points
     */
    public static final String BASE_API = "/api";
    public static final String CREATE = "/create";
    public static final String UPDATE = "/update";
    public static final String DELETE = "/{id}";
    public static final String GET_ALL = "/list";
    public static final String GET_ALL_WITH_PAGE = "/list/{currentPage}/{pageSize}";
    public static final String GET = "/{id}";

    //For file
    public static final String FILE = "/file";

    public static final String DISPLAY_FILE = "/display/{fileName}";
    public static final String FILE_DOWNLOAD = "/download";
    public static final String SEARCH = "/search/{currentPage}/{pageSize}";
    public static final String UPLOAD = "/upload";




    public static final String LOGGER = "/logger/{status}";
    //header Constants
    public static final String FORM_HEADER = "form";
    public static final String APPLICATION_HEADER = "application";
    public static final String AUTHORIZATION_HEADER = "authorization";
    public static final String IP = "ip";
    public static final String COUNTRY = "country";
    public static final String LAT = "lat";
    public static final String LON = "lon";

    /**
     * Module wise interface for api end points
     */
    public interface UserManagement {
        String UM_AUTHENTICATE = "/auth";
        String CHANGE_PASSWORD = "/chhangepassword";
        String GET_PROFILE_DETAIL = "/profile";


    }

    public interface EKEA {
        String EKEA_BASE = BASE_API + "/ekea";
        String HOME = EKEA_BASE + "/home";
        String ORDER_PRODUCT = EKEA_BASE + "/order-product";
        String CATEGORY = EKEA_BASE + "/category";
        String LOCATION = EKEA_BASE + "/location";
        String ORDER = EKEA_BASE + "/order";
        String MAKE_ORDER = "/make-order";
        String MY_ORDERS = "/my-orders";
        String MY_ORDER_DETAILS = "/my-orders/{orderId}";
        String PAYMENT = EKEA_BASE + "/payment";
        String PRODUCT = EKEA_BASE + "/product";
        String PRODUCTS_BY_CATEGORY_ID = "/category/{categoryId}";
        String PRODUCTS_BY_CATEGORY_ID_WITH_PAGE = "/{categoryId}/{currentPage}/{pageSize}";
        String HOT_PRODUCT_LIST = "/hot-product-list";
        String STORAGE = EKEA_BASE + "/storage";
        String USER = EKEA_BASE + "/user";

        String ORDER_BY_SITE = "/list/country/{countryName}";

        String UPDATE_BY_ORDER_STATUS = "/update-status/{orderId}/{orderStatus}";


    }

    public interface TENANT {
        String FINLAND = "FINLAND";
        String SWEDEN = "SWEDEN";
        String RUSSIA = "RUSSIA";
    }


}
