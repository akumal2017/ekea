package com.ekea.core.constant;

/**
 * Created by Anil Kumal on 11/28/2018.
 */
public final class ServiceURIConstant extends WebResourceConstant {
    public static final String ROOT_HOST = "http://localhost:8080";
    public static final String ROOT_GATEWAY_API = "/gymhouse";

}
