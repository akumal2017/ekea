package com.ekea.core.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * Created by admin on 5/10/2018.
 */
@Getter
@Setter
@MappedSuperclass
public class EkeaEntityBase extends ModelBase {
    @Id
    @Column(name = "id")
    private String id;

    @Version
    @Column(name = "version")
    private Long version = 0L;


}
