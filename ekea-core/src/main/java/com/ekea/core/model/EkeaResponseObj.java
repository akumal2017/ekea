package com.ekea.core.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil Kumal on 11/28/2018.
 */
@Getter
@Setter
public class EkeaResponseObj extends ModelBase {
    private Object result;
    private String message;
    private Boolean responseStatus;
    private Object page;

    public EkeaResponseObj() {
    }

    private EkeaResponseObj(EkeaResponseObjBuilder ekeaResponseObjBuilder) {
        this.result = ekeaResponseObjBuilder.result;
        this.message = ekeaResponseObjBuilder.message;
        this.responseStatus = ekeaResponseObjBuilder.responseStatus;
        this.page = ekeaResponseObjBuilder.page;
    }

    public static class EkeaResponseObjBuilder {
        private Object result;
        private String message;
        private Boolean responseStatus = true;
        private Object page;

        public EkeaResponseObjBuilder() {
        }

        public EkeaResponseObjBuilder result(Object result) {
            this.result = result;
            return this;
        }

        public EkeaResponseObjBuilder message(String message) {
            this.message = message;
            return this;
        }

        public EkeaResponseObjBuilder responseStatus(Boolean responseStatus) {
            this.responseStatus = responseStatus;
            return this;
        }

        public EkeaResponseObjBuilder page(Object page) {
            this.page = page;
            return this;
        }

        public EkeaResponseObj build() {
            return new EkeaResponseObj(this);
        }

    }


}
