package com.ekea.core.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil Kumal on 11/28/2018.
 */
@Getter
@Setter
public class EkeaResponseDtoBase extends EkeaDtoBase {
    private String id;


}
