package com.ekea.core.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil Kumal on 11/28/2018.
 */
@Getter
@Setter
public class ReferencedTableModel extends ModelBase {
    String tableName;
    String columnName;
}
