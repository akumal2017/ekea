package com.ekea.core.model;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import java.io.Serializable;

/**
 * Created by Anil Kumal on 11/28/2018.
 * <p>
 * Base Class for every class related to domain object, entity object response object, DTO etc.
 */
public abstract class ModelBase implements Serializable {

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
