package com.ekea.core.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil on 5/14/18.
 */
@Getter
@Setter
public class EkeaTokenModel extends ModelBase {

    private String userId;

    private String email;

    private String userLocation;

    private String userIp;

    private String originCountry;

}
