package com.ekea.core.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil Kumal on 11/29/18.
 */
@Getter
@Setter
public class EkeaException extends RuntimeException {
    private Object data;

    public EkeaException(String message) {
        this(message, null);
    }

    public EkeaException(String message, Object data) {
        super(message);
        this.data = data;
    }
}
