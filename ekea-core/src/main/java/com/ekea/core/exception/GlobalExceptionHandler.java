package com.ekea.core.exception;


import com.ekea.core.model.EkeaResponseObj;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created by Anil Kumal on 11/29/18.
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * Handles the general error case. print the exception
     *
     * @param e the exception not handled by other exception handler methods
     * @return {@link EkeaResponseObj} which contains the error message and response status with JSON format
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<EkeaResponseObj> handleGenericException(Exception e) {
        e.printStackTrace();
        EkeaResponseObj ekeaResponseObj = new EkeaResponseObj.EkeaResponseObjBuilder()
                .message("cause: " + e.getCause() + ", message: " + e.getMessage())
                .responseStatus(false)
                .build();
        return new ResponseEntity<>(ekeaResponseObj, HttpStatus.OK);
    }


    /**
     * Handles the customize {@link ExpiredJwtException} expired jwt exception error case . print the exception
     *
     * @param runtimeException the exception not handled by other exception handler methods
     * @return {@link EkeaResponseObj} which contains the error message and response status with JSON format
     */

    @ExceptionHandler(ExpiredJwtException.class)
    public ResponseEntity<EkeaResponseObj> handleExpiredJwtException(RuntimeException runtimeException) {
        runtimeException.printStackTrace();
        EkeaResponseObj ekeaResponseObj = new EkeaResponseObj.EkeaResponseObjBuilder()
                .message("cause: " + runtimeException.getCause() + ", message: " + runtimeException.getMessage())
                .responseStatus(false)
                .build();
        return new ResponseEntity<>(ekeaResponseObj, HttpStatus.OK);
    }

    /**
     * Handles the method argument not valid exception. print the exception
     *
     * @param e the exception not handled by other exception handler methods
     * @return {@link EkeaResponseObj} which contains the error message and response status with JSON format
     */

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<EkeaResponseObj> methodArgumentNotValidException(Exception e) {
        e.printStackTrace();
        EkeaResponseObj ekeaResponseObj = new EkeaResponseObj.EkeaResponseObjBuilder()
                .message("cause: " + e.getCause() + ", message: " + e.getMessage())
                .responseStatus(false)
                .build();
        return new ResponseEntity<>(ekeaResponseObj, HttpStatus.OK);

    }

    /**
     * Handles the Data database level DataIntegrityViolationException. print the exception
     *
     * @param e the exception not handled by other exception handler methods
     * @return {@link EkeaResponseObj} which contains the error message and response status with JSON format
     */

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<EkeaResponseObj> constraintViolationException(Exception e) {
        EkeaResponseObj ekeaResponseObj = new EkeaResponseObj.EkeaResponseObjBuilder()
                .message("cause: " + e.getCause() + ", message: " + e.getMessage())
                .responseStatus(false)
                .build();
        return new ResponseEntity<>(ekeaResponseObj, HttpStatus.OK);
    }

    /**
     * Handles the Data database level TransientPropertyValueException. print the exception
     *
     * @param e the exception not handled by other exception handler methods
     * @return {@link EkeaResponseObj} which contains the error message and response status with JSON format
     */

    @ExceptionHandler(InvalidDataAccessApiUsageException.class)//TO DO
    public ResponseEntity<EkeaResponseObj> transientPropertyValueException(Exception e) {
        EkeaResponseObj ekeaResponseObj = new EkeaResponseObj.EkeaResponseObjBuilder()
                .message("cause: " + e.getCause() + ", message: " + e.getMessage())
                .responseStatus(false)
                .build();
        return new ResponseEntity<>(ekeaResponseObj, HttpStatus.OK);
    }

    /**
     * Handles the Data database level ObjectOptimisticLockingFailureException. print the exception
     *
     * @param e the exception not handled by other exception handler methods
     * @return {@link EkeaResponseObj} which contains the error message and response status with JSON format
     */

    @ExceptionHandler(ObjectOptimisticLockingFailureException.class)
    public ResponseEntity<EkeaResponseObj> objectOptimisticLockingFailureException(Exception e) {
        EkeaResponseObj ekeaResponseObj = new EkeaResponseObj.EkeaResponseObjBuilder()
                .message("cause: " + e.getCause() + ", message: " + e.getMessage())
                .responseStatus(false)
                .build();
        return new ResponseEntity<>(ekeaResponseObj, HttpStatus.OK);
    }

    /**
     * Handles the Data database level SignatureException. print the exception
     *
     * @param e the exception not handled by other exception handler methods
     * @return {@link EkeaResponseObj} which contains the error message and response status with JSON format
     */
    @ExceptionHandler(SignatureException.class)
    public ResponseEntity<EkeaResponseObj> signatureException(Exception e) {
        EkeaResponseObj ekeaResponseObj = new EkeaResponseObj.EkeaResponseObjBuilder()
                .message("cause: " + e.getCause() + ", message: Sorry!! Someone has already updated the record")
                .responseStatus(false)
                .build();
        return new ResponseEntity<>(ekeaResponseObj, HttpStatus.OK);
    }


    /**
     * Handles custom exception
     *
     * @param e the exception not handled by other exception handler methods
     * @return {@link EkeaResponseObj} which contains the error message and response status with JSON format
     */
    @ExceptionHandler(EkeaException.class)
    public ResponseEntity<EkeaResponseObj> dracException(RuntimeException e) {
        e.printStackTrace();
        Object data = null;
        if (e instanceof EkeaException) {
            data = ((EkeaException) e).getData();
        }
        EkeaResponseObj ekeaResponseObj = new EkeaResponseObj.EkeaResponseObjBuilder()
                .message("cause: " + e.getCause() + ", message: " + e.getMessage())
                .responseStatus(false)
                .result(data)
                .build();
        return new ResponseEntity<>(ekeaResponseObj, HttpStatus.OK);
    }
}
